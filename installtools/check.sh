#!/bin/sh
# check.sh $(srcdir)

srcdir="$1"
cd "$srcdir" || exit 1

# argparse new in 2.7, 3.2
# u'' in Python 3 since 3.3
# Some rare platforms are missing or have broken multiprocessing
v_ck="import argparse, multiprocessing; u''"

interpreter_list=$(mktemp)
if ! [ -f "$interpreter_list" ]; then
    echo "mktemp failed" >&2
    exit 1
fi

try ()
{
    interpreter="$1"
    if "$interpreter" -c "$v_ck" 2>/dev/null ; then
        echo found
        echo "$interpreter" >>"$interpreter_list"
        if [ "$DEBUG" = 1 ]; then
            echo "XXX $interpreter" >&2
        fi
        if ! "$interpreter" -c "import test; test.pre_release()" >&2; then
            echo fail
            echo "Failed on $interpreter" >&2
        fi
    fi
}

main ()
{
    echo "$PATH" | tr : '\n' | while IFS=: read directory;
    do
        for implementation in python pypy jython ipy ironpython graalpy \
                              rustpython
        do
            try "${directory}/${implementation}"
            try "${directory}/${implementation}2"
            try "${directory}/${implementation}2.7"
            try "${directory}/${implementation}3"
            minor=3
            while [ "$minor" -le 30 ]; do
                try "${directory}/${implementation}3.${minor}${suffix}"
                try "${directory}/${implementation}3.${minor}${suffix}m"
                try "${directory}/${implementation}3.${minor}${suffix}t"
                for micro in "${directory}/${implementation}3.${minor}."*; do
                    try "${micro}"
                done
                minor=$((minor+1))
            done
        done
    done
}

results="$(main)"

if [ -z "$results" ]; then
    echo "No python interpreters found" >&2
    rm "$interpreter_list"
    exit 42
fi

if [ -n "$(echo "$results" | grep fail)" ]; then
    echo "Headless test failed" >&2
    rm "$interpreter_list"
    exit 23
else
    echo "Tested OK on all interpreters found:" >&2
    cat "$interpreter_list" >&2
    rm "$interpreter_list"
fi
