#!/usr/bin/python

'''This module provides the engine of Anonymine

Copyright (c) Oskar Skog, 2016-2024
Released under the FreeBSD (2-Clause) license.

The engine of Anonymine
=======================

    The class `game_engine` contains the field, initializes it and
    manipulates it during a game.
    The documentation for `game_engine` will describe the coordinate
    system for the different field types, methods used to manipulate
    it, and the actual gluing of engine and interface.

'''

import errno
import getpass
import locale
import math
import multiprocessing
import os
import platform
# I think `queue` is only for the exception raised
try:
    import queue
except ImportError:
    import Queue as queue
import random
import signal
import sys
import threading
import time

import anonymine_solver as solver
import anonymine_fields as fields

# Define sys_platform as sys.platform.getshadow() or sys.platform
# Jython on Windows isn't identified as Windows.
try:
    sys_platform = sys.platform.getshadow()
    os_name = os.name.getshadow()
except AttributeError:
    sys_platform = sys.platform
    os_name = os.name


try:
    multiprocessing.set_start_method('fork')
    # https://bugs.python.org/issue33725
    # On macOS, 'spawn' is the default for Python >= 3.8
    # 'spawn' and 'forkserver' both require pickling which doesn't work.
    # There is a reason why the default is 'spawn' on macOS, but it
    # shouldn't apply for single threaded programs. Anonymine 0.6.6~0.6.7
    # has been confirmed to run only a single thread on macOS 10.12 Sierra,
    # 10.14 Mojave and 11 Big Sur.
except:
    pass


# See bugs in doc/INSTALL.SerenityOS
# Python 3.2 has multiprocessing.queues.SimpleQueue
# Python 3.3 has multiprocessing.SimpleQueue
# Python 3.3 is lowest supported version of Python 3
use_SimpleQueue = (sys_platform == 'serenityos')
use_SimpleQueue = False     # Queue is currently working
use_SimpleQueue = int(os.getenv('USE_SIMPLEQUEUE', use_SimpleQueue))
if sys.version_info[0] < 3 and use_SimpleQueue:
   sys.stderr.write("WARNING: SimpleQueue not supported by Python 2.7\n")
   use_SimpleQueue = False


# https://github.com/python/cpython/issues/126688
# The fix will probably be included in 3.13.1 / 3.14.0a2
if platform.python_implementation() == 'CPython':
    affected_versions = [
        (3, 13, 0, 'final', 0),
        (3, 14, 0, 'alpha', 1),
    ]
    if sys.version_info in affected_versions:
        # Whitelist all known systems where pthread_self returns the same
        # value after fork
        whitelist = [
            'cygwin',   'darwin',       'freebsd14',    'gnu0',     'haiku1',
            'linux',    'midnightbsd3', 'netbsd10',     'openbsd7',
        ]
        # ... and all systems where Python is known to be patched
        whitelist += ['serenityos',]
        # 'sunos5' is too vague, Solaris 9 is suspected to be affected
        # by this bug
        solaris11 = False
        if sys.platform == 'sunos5':
            if platform.uname().release == '5.11':
                solaris11 = True
        whitelisted = (sys.platform in whitelist) or solaris11
        if 'fork' in dir(os) and not whitelisted:
            del os.fork
            sys.stderr.write(
                'NOTICE: Performance degraded because of potential Python bug\n'
                'See https://github.com/python/cpython/issues/126688\n'
            )



class security_alert(Exception):
    pass


class cfg_error(Exception):
    pass


def _repr_lenlimit(thing):
    '''`repr(thing)` but length-limited. For use in `load_cfg`.'''
    s = repr(thing)
    maxlen = 2*250      # MUST be even
    if len(s) >= maxlen:
        s = '{}\n... {} more characters ...\n{}'.format(
                s[:maxlen>>1], len(s) - maxlen, s[-(maxlen>>1):]
        )
    return s


def _assert_file_is_safe(file_obj):
    '''
    Make sure this user and trusted users are the only ones that could
    have created/edited the file that is about to be read and `eval`ed.

    Raises `security_alert` if the file cannot be trusted.

    Usage:
        f = open(filepath)
        _assert_file_is_safe(f)
        data = eval(f.read())

    Trusted users:
        root
        Group 0 (wheel)
        Cygwin & Python 3 only: Anyone in group 544 (Administrators)
        Cygwin & Python 2 only: Any file in /etc/
    '''
    stat = os.fstat(file_obj.fileno())

    if os_name == 'posix':
        # IronPython 2.7.12 on Mono on Linux has broken os.stat and no os.getuid
        if platform.python_implementation() == 'IronPython':
            if sys.version_info[0] == 2:
                return
        # Check if the file owner is an administrator on Windows
        is_admin = False
        if sys_platform == 'cygwin':
            if sys.version_info[:2] >= (3,5):
                # os.getgrouplist availability: Unix, added in Python 3.3
                # Segfaults on ReactOS which has Python 3.4
                import pwd
                owner = pwd.getpwuid(stat.st_uid)
                owner_groups = os.getgrouplist(owner.pw_name, owner.pw_gid)
                is_admin = 544 in owner_groups
            else:
                # Python 2.7 should only be used on NT 5.x
                # "CYGWIN_NT-5.1" -> 5
                win_major = int(platform.uname()[0].split('-')[1].split('.')[0])
                if win_major <= 5:
                    is_admin = file_obj.name.startswith('/etc/')
        # Permission checks
        if not (stat.st_uid == os.getuid() or stat.st_uid == 0 or is_admin):
            raise security_alert('File owner not trusted')
        if stat.st_gid != 0 and stat.st_mode & 0o020:
            raise security_alert('Non-root group has write permission')
        if stat.st_mode & 0o002:
            raise security_alert('Others have write permission')
        # Check that it is a normal file
        if stat.st_mode | 0o7777 != 0o107777:
            raise security_alert('Not a normal file')

    if sys_platform.startswith('win'):
        # Stop shenanigans like /etc/anonymine/*cfg on Windows
        # Only allow proper relative or absolute paths
        normalized = file_obj.name.replace('/', '\\')
        absolute = os.path.abspath(file_obj.name)
        relative = os.path.relpath(file_obj.name)
        if not normalized in (absolute, relative):
            raise security_alert('Please use a sane absolute or relative path')


def _raise_cfg_error(cfgname, path, orig_exception, message=""):
    raise cfg_error(
        "There was an error loading {} configuration file\n"
        "File: {}\n"
        "Message: {}\n"
        "Original exception: {}"
        .format(cfgname, path, message, _repr_lenlimit(orig_exception))
    )


def load_cfg(cfgfile_path, cfgname, imports):
    '''
    return eval(open(cfgfile).read())
    
    `cfgfile_path` is the path to the configuration file
    `cfgname` is only an identifying name to be used in exceptions
    `imports` is an iterable of strings for modules that need to be imported,
    example: ['curses']
    
    Can raise cfg_error if things go wrong.
    Can follow "symlink" extracted from zip on Windows.
    '''
    for module in imports:
        # `exec` has changed in Python 3.13, or rather `locals` has different
        # semantics.
        exec('import ' + module, globals())

    try:
        cfgfile = open(cfgfile_path, 'rb')
    except Exception as err:
        _raise_cfg_error(cfgname, cfgfile_path, err, "Could not open file")

    # Look at stat to determine if the file is safe to `eval`
    try:
        _assert_file_is_safe(cfgfile)
    except security_alert as err:
        _raise_cfg_error(cfgname, cfgfile_path, err,
                         "Refusing to load file due to security concerns")

    try:
        content = cfgfile.read()
        cfgfile.close()
    except Exception as err:
        _raise_cfg_error(cfgname, cfgfile_path, err, "Failed to read file")

    # 'windows-beta/cursescfg' may be a symlink to '../cursescfg'
    symlink = None
    # Cygwin symlink, only with CYGWIN=winsymlinks:sys
    if content.startswith(b'!<symlink>'):
        symlink = content[10:].decode('utf-16').lstrip(u'\ufeff').rstrip(u'\0')
    # Gitlab zip
    elif content in (b'../cursescfg', b'..\\cursescfg'):
        symlink = content.decode('ascii')

    if symlink is not None:
        symlink_target = os.path.join(os.path.split(cfgfile_path)[0], symlink)
        # Convert to proper relative path to not trigger _assert_file_is_safe
        symlink_target = os.path.relpath(symlink_target)
        return load_cfg(symlink_target, cfgname, imports)

    try:
        return eval(content.decode('utf-8'))
    except Exception as err:
        _raise_cfg_error(cfgname, cfgfile_path, err, "Error in file content")


### End cfg file loading



class hiscores():
    '''
    Manage highscores.
    
    The `play_game` method of `game_engine` will return a
    pre-configured `hiscores` object to the interface (its caller).
    
    The `add_entry` method will need to be called to add the recently
    played game to the list.  `add_entry` will also set the `position`
    attribute (1-indexed), it is None if the user didn't make it.
    
    File format
    -----------
    
        Each line represents an item in a list, the list items also
        specify in which list they are.
        
        The file does not support comments.
    
        line = <paramstring> ":" <time_taken> ":" <time> ":" <user> ":" <nick>
        
        `paramstring` identifies in which list the item is.
        
        `time_taken` is the time in seconds of how long it took to
        play.  Or if the `paramstring` is for a lost game:
        `time_taken` is "{mines_left},{time}".
        
        `time` is the Unix time when the `hiscores` instance was
        created.  (The time when the game was won.)

        `user` is the user name (login name) of the player.
        
        `nick` = "[" <extension-list> "]" space <nickname>
        `nick` was originally just a player chosen nickname.  `nick` is
        the only field that MAY contain a colon.  Other fields MUST NOT
        contain colons.

        `extensions-list` is a comma separated list of extensions
        Extensions are either value-less extensions or of the form
        <name>:<value>
        Current and past extensions:
            cheater     Value-less
            seed-*      Removed in 0.6.42, considered as different
                        value-less extensions
        
        Even if either `user` or `nick` is disabled in the
        configuration, their fields must still appear in the
        highscores file.  Disabling them only disables them from
        being reported by the `display` method and makes the
        `add_entry` method set their fields to an empty string.
    
    
    paramstring
    -----------
    
        The syntax that will be used for paramstrings should be
        documented somewhere and somehow.
        
        "{mines}@{width}x{height}-{gametype}" + ng*"+losable"
        
        <mines>"@"<width>"x"<height>"-"<gametype>["+losable"]
        
        And for a lost game, "lost/" is prepended to the paramstring, ie:
        
        "lost/"<mines>"@"<width>"x"<height>"-"<gametype>["+losable"]
    
    
    Unicode
    -------
    
        The highscores file is UTF-8 encoded.  The stings returned by
        the methods in this class are a combination of ASCII only `str`
        types on both Python versions, and Unicode strings (`unicode`
        or `str`).   Notice that the callback for `add_entry` MUST
        return an `str` instance on both Python versions.
        
    '''

    def __init__(self, cfg, paramstring, time_taken, mines_left=0,
                 immortal_cheat=False):
        '''
        Create a `hiscores` object for the played game.
        This object is created by `game_engine.play_game` after
        the game has been won.
        
        If `time_taken` is None, the `add_entry` method is neutralized
        for both won and lost games.
        
        `paramstring` selects the sublist (game settings/parameters).
        
        `cfg` is a dictionary that MUST contain the following keys:
            - 'file'            string; Path to the highscores file
            - 'maxsize'         int; Maximum allowed filesize (bytes)
            - 'nick-maxlen'     int; Maximum allowed length of nickname
                                (unicode code points)
            - 'entries'         int; Entries per sublist (paramstring)
            - 'show-cheater'    bool
            - 'save-user'       bool; Store user/login names
            - 'show-user'       bool; Display user/login names
            - 'use-nick'        bool; List and display nicknames
        
        `mines_left` (integer) is only used if the game was lost.
        
        If the game was lost, either set `time_taken` to None
        (to prevent someone from entering the winners' highscores),
        or prepend "lost/" to `paramstring`.  The latter option
        will add a record the losers' highscores.
        '''
        self.paramstring = paramstring
        if time_taken is None:
            self.time_taken = None
        else:
            self.time_taken = str(time_taken)
        self.win_time = str(time.time())
        self.mines_left = mines_left
        self.cheater = immortal_cheat
        
        self.hiscorefile = cfg['file']
        self.n_entries = cfg['entries']
        self.maxsize = cfg['maxsize']
        # Show cheaters
        if 'show-cheater' in cfg:
            self.show_cheater = cfg['show-cheater']
        else:
            self.show_cheater = True
        # Save/show login names
        if 'use-user' in cfg:
            self.save_user = self.show_user = cfg['use-user']
        if 'save-user' in cfg:
            self.save_user = user = cfg['save-user']
        if 'show-user' in cfg:
            self.show_user = user = cfg['show-user']
        self.use_nick = cfg['use-nick']
        self.nick_maxlen = cfg['nick-maxlen']

        # The caption that will be displayed by the callback sent to
        # `display`.  This caption can be changed by any method before
        # ending up on the screen.
        self.display_caption = 'Higscores for these settings'
        self.hiscores = None
        self.position = None

        self.overwrite_prevention = False


    def _load(self):
        '''Load `self.hiscores` from `self.hiscorefile`.'''
        def line_to_entry(line):
            parts = line.split(':', 4)
            # Sanity checking
            if len(parts) != 5:
                raise ValueError('Not enough fields in ' +repr(line))
            # <mines_left>,<time_taken>  or  <time_taken>
            list(map(float, parts[1].split(',')))
            # unix timestamp
            float(parts[2])

            # Automatically update format
            nickname, extensions = self._read_nick_field(parts[4])
            parts[4] = self._write_nick_field(nickname, extensions)

            return tuple(parts)

        try:
            # Read
            f = open(self.hiscorefile, 'rb')
            file_bytes = f.read(self.maxsize + 1)
            if len(file_bytes) > self.maxsize:
                raise security_alert('Highscores file too large')
            # Decode
            try:
                filecontent = file_bytes.decode('utf-8')
                lines = list(filter(None, filecontent.split('\n')))
                self.hiscores = list(map(line_to_entry, lines))
            except Exception as err:
                raise ValueError("Highscores file corrupted: {}".format(
                    repr(err)[:200]
                ))
        except Exception as err:
            self.display_caption = "IOError on read: {}".format(err)
            self.hiscores = []
            self.overwrite_prevention = True
    

    def _store(self):
        '''Store `self.hiscores` to `self.hiscorefile`.'''
        if self.overwrite_prevention:
            self.display_caption = (
                'Not overwriting highscores file due to previous error'
            )
            return

        content = ''
        for entry in self.hiscores:
            content += ':'.join(entry) + '\n'
        # Encode as UTF-8
        content = content.encode('utf-8', errors='xmlcharrefreplace')
        # Check size in bytes
        if len(content) <= self.maxsize:
            try:
                f = open(self.hiscorefile, 'wb')
            except IOError as err:
                self.display_caption = 'IO error on write: ' + err.strerror
                return
            f.write(content)
            f.close()
        else:
            self.display_caption = "New highscore's filesize too large"
    

    def _sort(self, sublist, game_lost):
        '''
        Sort a sublist.
        
        Function needed due to loser's highscores [0.3.1]
        '''
        if not game_lost:
            sublist.sort(key=lambda entry: float(entry[1]))
        else:
            times = {}
            for record in sublist:
                mines_left = int(record[1].split(',')[0])
                t = float(record[1].split(',')[1])
                if mines_left not in times:
                    times[mines_left] = t
                if times[mines_left] > t:
                    times[mines_left] = t
            def rank(record):
                mines_left = int(record[1].split(',')[0])
                t = float(record[1].split(',')[1])
                return t/times[mines_left] * mines_left
            sublist.sort(key=rank)
    

    def _read_nick_field(self, nickname_field):
        '''
        obj._read_nick_field(nickname_field) -> nickname, extensions

        nickname is a str
        extensions is a dict

        Known keys in extenstions:
            'cheater':      Value is None
            'seed-'*:       Value is None
        '''
        # Detect old format entries
        if nickname_field.startswith('[Pretending to be immortal]'):
            nickname = '[IMMORTAL]' + nickname_field.split(']', 1)[1]
            return nickname, {}
        if nickname_field.startswith('[IMMORTAL] '):
            return nickname_field.split('] ', 1)[1], {'cheater': None,}
        if not nickname_field.startswith('[') or '] ' not in nickname_field:
            return nickname_field, {}
        #
        extensions_subfield, nickname = nickname_field.split('] ', 1)
        try:
            extensions_list = extensions_subfield[1:].split(',')
            extensions = {}
            for item in extensions_list:
                if item == '':
                    continue
                if ':' in item:
                    key, value = item.split(':', 1)
                else:
                    key = item
                    value = None
                extensions[key] = value
            return nickname, extensions
        except Exception:
            raise
            # Probably old format
            return nickname_field, {}


    def _write_nick_field(self, nickname, extensions):
        '''
        Inverse function to self._read_nick_field
        '''
        extensions_list = []
        for key in extensions:
            if extensions[key] is not None:
                extensions_list.append('{}:{}'.format(key, extensions[key]))
            else:
                extensions_list.append(key)
        extensions_str = '[{}]'.format(','.join(extensions_list))
        if nickname is not None:
            return u'{} {}'.format(extensions_str, nickname)
        else:
            return extensions_str


    def add_entry(self, inputfunction):
        '''Call this method to add yourself to the hiscores list.
        
        `inputfunction` is a callback to the interface.
        string = inputfunction(titlebar, prompt)
        
        string MUST be of the `str` type on both Python versions.
        '''
        def load_split_add(self_reference, new_entry):
            '''
            Load self_reference.hiscores and separates the sublist
            from it.  `new_entry` will be appended to the sublist.
            
            Returns the sorted and tail truncated sublist.
            self_reference.hiscores does not contain the sublist.
            '''
            self_reference._load()
            sublist = list(filter(
                lambda entry: entry[0] == self_reference.paramstring,
                self_reference.hiscores
            ))
            self.hiscores = list(filter(
                lambda entry: entry[0] != self_reference.paramstring,
                self_reference.hiscores
            ))
            # Add entry.
            sublist.append(new_entry)
            self._sort(sublist, self.paramstring.startswith('lost/'))
            sublist = sublist[:self_reference.n_entries]
            return sublist

        # Display only mode:
        if self.time_taken is None:
            return
        # Get login name
        if self.save_user:
            try:
                user = getpass.getuser()
            except:
                user = '(unknown)'
        else:
            user = ''
        user = user.replace('\\', '\\\\').replace(':', '\\x3a')
        # Prepare the new entry
        if self.paramstring.startswith('lost/'):
            time_taken = "{},{}".format(self.mines_left, self.time_taken)
        else:
            time_taken = self.time_taken
        new_entry = [
            self.paramstring,
            time_taken,
            self.win_time,
            user,
            '',
        ]
        assert '\n' not in ''.join(new_entry)
        # Get the nickname only if the player actually made it to the list.
        # The entry will actually be added twice, but only the latter will
        # be stored.
        try:
            position = load_split_add(self, new_entry).index(new_entry)
            # self.position will be set later
        except ValueError:
            position = None
            # If position is None, self.position won't be set later.
            self.position = None
            self.display_caption = "You didn't make it to the top {}".format(
                self.n_entries
            )
        if position is not None:
            if self.use_nick:
                title = 'You made it to #{}'.format(position + 1)
                while True:
                    nick = inputfunction(title, 'Nickname')
                    if sys.version_info[0] == 2:
                        # Don't try decoding using other charsets,
                        # it'll just blow up on output instead.
                        try:
                            nick = nick.decode(
                                locale.getpreferredencoding(), errors='ignore'
                            )
                        except LookupError:
                            # IronPython 2.7 on Windows raised this for
                            # whatever reason
                            nick = nick.decode('ascii', errors='ignore')
                    if len(nick) > self.nick_maxlen:
                        title = 'No more than {} characters allowed'.format(
                            self.nick_maxlen
                        )
                    else:
                        break
            else:
                nick = None
            if self.cheater:
                extensions = {'cheater': None}
            else:
                extensions = []
            new_entry[4] = self._write_nick_field(nick, extensions)


            # Load the list again (inputfunction may take a very long time)
            # and add the nickname to the entry.
            sublist = load_split_add(self, new_entry)
            # Write back
            self.hiscores.extend(sublist)
            self._store()

            # Position message.
            if new_entry in sublist:
                position = sublist.index(new_entry)
                self.position = position + 1
                self.display_caption = 'You made it to #{}'.format(
                    self.position
                )
            else:
                # Race condition
                self.position = None
                self.display_caption = "Nearly made it!"
        # END if position is not None

        # dispay_caption is different for losers, why?
        if self.paramstring.startswith('lost/'):
            self.display_caption = "Losers' highscores"
            if self.position is not None:
                self.display_caption += ", you're #{}".format(self.position)
    

    def display(self):
        '''
        self.display -> caption, headers, rows
        
        `caption` is a string.
        `headers` is a tuple of strings.
        `rows` is a list of tuples of strings.
        
        Explaining with pseudoHTML:
        <h1>caption</h1>
        <table>
            <tr><th>headers[0]</th>...<th>headers[c]</tr>
            <tr><td>rows[0][0]</td>...<td>rows[0][c]</tr>
            ...
            <tr><td>rows[r][0]</td>...<td>rows[r][c]</tr>
        </table>
        <!-- There are r-1 rows and c-1 columns -->
        '''
        
        def format_deltatime(t):
            def tfmt(format, t):
                return time.strftime(format, time.gmtime(t))
            if 1000*t <= 3559999:
                t = math.ceil(t * 1000.0) / 1000.0
                subsec = int(math.ceil(t * 1000.0) % 1000)
                ds = str(int(subsec/100))
                cs = str(int(subsec/10%10))
                ms = str(int(subsec%10))
                return tfmt('%M:%S.', t) + ds + cs + ms
            elif t <= 86399:
                t = math.ceil(t)
                return tfmt('%H:%M:%S', t)
            elif t <= 863940:
                t = 60 * math.ceil(t / 60.0)
                return "{0}d {1}".format(int(t//86400), tfmt('%H:%M', t))
            else:
                return "A long time"
        
        def format_wontime(t):
            # Does not need to be precise, anything less than a week is good.
            if time.time() - t < 6*86400:
                s = time.strftime('%a %H:%M', time.localtime(t))
                if sys.version_info[0] == 2:
                    try:
                        encoding = locale.getpreferredencoding()
                    except LookupError:
                        encoding = 'ascii'
                    s = s.decode(encoding, errors='xmlcharrefreplace')
                return s
            elif time.time() - t < 0:
                return '(Future)'
            else:
                return time.strftime('%Y-%m-%d', time.localtime(t))
        
        game_lost = self.paramstring.startswith('lost/')
        
        self._load()
        # Use only the relevant sublist.
        sublist = list(filter(
            lambda entry: entry[0] == self.paramstring,
            self.hiscores
        ))
        
        self._sort(sublist, game_lost)
        
        headers = ['Rank']
        if game_lost:
            headers.append('Mines left')
        headers.extend(['Time taken', 'When'])
        if self.show_cheater:
            # The body will be either '' or 'Cheater', so the
            # header is intentionally blank.
            headers.append('')
        if self.show_user:
            headers.append('Login name')
        if self.use_nick:
            headers.append('Nickname')
        
        rows = []
        for index, entry in enumerate(sublist):
            position_col = '#' + str(index + 1)
            if index+1 == self.position:
                position_col += ' <--'
            if game_lost:
                row = [
                    position_col,
                    str(int(entry[1].split(',')[0])),
                    format_deltatime(float(entry[1].split(',')[1])),
                    format_wontime(float(entry[2])),
            ]
            else:
                row = [
                    position_col,
                    format_deltatime(float(entry[1])),
                    format_wontime(float(entry[2])),
                ]
            nick, extensions = self._read_nick_field(entry[4])
            cheater = 'cheater' in extensions
            if self.show_cheater:
                if cheater:
                    row.append('Cheater')
                else:
                    row.append('')
            if self.show_user:
                row.append(entry[3])
            if self.use_nick:
                row.append(nick)
            rows.append(tuple(row))
        
        return self.display_caption, headers, rows



class game_engine():
    r'''
    This class creates game engine objects.
    
    This doc-string describes how the engine and the interface
    interacts.
    
    The engine:
        * Creates and initializes a field with the specified game
            parameters.
        * Has a play loop that will use the interface to do all IO.
        * Contains and manipulates the field object.
    
    The interface:
        * Makes a representation of the field for the player.
        * Contains IO methods that are used by the play loop.
        * Uses various important methods of the engine.
    
    
    (field-) Coordinates (non-hexagonal fields)
    ===========================================
    
        Each coordinate is a tuple of (x, y).
        Where x and y are integers and
            0 <= x < width
            0 <= y < height
        
        (0, 0)  (1, 0)  (2, 0)  (3, 0)  (4, 0)
        (0, 1)  (1, 1)  (2, 1)  (3, 1)  (4, 1)
        (0, 2)  (1, 2)  (2, 2)  (3, 2)  (4, 2)
        (0, 3)  (1, 3)  (2, 3)  (3, 3)  (4, 3)
        (0, 4)  (1, 4)  (2, 4)  (3, 4)  (4, 4)
        
        The Moore neighbours are
            (x-1, y-1)  (x, y-1)  (x+1, y-1)
            (x-1, y)              (x+1, y)
            (x-1, y+1)  (x, y+1)  (x+1, y+1)
        And the Neumann neighbours are:
                        (x, y-1)
            (x-1, y)              (x+1, y)
                        (x, y+1)
    
    
    (field-) Coordinates (hexagonal fields)
    =======================================
    
        Each coordinate is a tuple of (x, y).
        Where x and y are integers and
            0 <= x < width
            0 <= y < height
        
        What makes this different from square fields is that odd lines
        are indented (a half step) on the screen so that it looks like.
        
         / \ / \ / \ / \ / \ / \ / \
        |0,0|1,0|2,0|3,0|4,0|5,0|6,0|
         \ / \ / \ / \ / \ / \ / \ / \
          |0,1|1,1|2,1|3,1|4,1|5,1|6,1|
         / \ / \ / \ / \ / \ / \ / \ /
        |0,2|1,2|2,2|3,2|4,2|5,2|6,2|
         \ / \ / \ / \ / \ / \ / \ / \
          |0,3|1,3|2,3|3,3|4,3|5,3|6,3|
           \ / \ / \ / \ / \ / \ / \ /
        
        The neighbourhoods are similar to Moore neighbourhoods, but
        these have no "corners" on the right for even rows, and no
        "corners" on the left on odd rows
        
        The hexagonal neighbours are:
            (x - 1 + y%2,  y - 1)       (x + y%2,  y - 1)
            (x - 1,  y)                 (x + 1,  y)
            (x - 1 + y%2,  y + 1)       (x + y%2,  y + 1)
    
    
    Important parts of the engine object
    ====================================
    
        `engine.game_status` is a string that has the value:
            'pre-game'  when the field hasn't been initialized
                        (every cell is free).
            'play-game' while the game has been initialized but not
                        won or lost.
            (INTERNAL): 'game-won'
            (INTERNAL): 'game-lost'
        
        `engine.field` is the actual field object.  The interface will
            need to use the `get` method of the field.  A modifying
            method (of the field object) can safely be used AFTER
            initialization.
        
        `engine.flag(coordinate)` is a simple wrapper that flags free
            cells and unflags flagged cells.
        
        `engine.reveal(coordinate)` is a simple wrapper that reveals
            cells after initialization, OR initializes the field.
            (Let the player choose the starting point by playing.)
        
        `engine.init_field(startpoint)` is the method that will place
            the mines and reveals the starting point, from which the
            game CAN be won.
        
        `engine.immortal_cheat` this attribute can be changed externally.
            It's False by default, but when set to True it becomes
            impossible to mis-click.  The cheat function is actually
            inside the `field` instance.
    
    
    Required methods of the interface object
    ========================================
    
        `interface.input(engine)`
            Receive input from the user and manipulate the field.
        
        `interface.output(engine)`
            "Show" the user a representation of the field.
            To do this, you are probably going to need to know how
            the coordinate system used by the field works.
        
        `interface.anykey_cont()`
            "Press any key to continue..."
            Let the user see the last screen before returning from
            `engine.play_game`.
            This method is actually not required.
    '''

    def __init__(self, cfgfile, **parameters):
        '''
        `cfgfile` is the path to the "enginecfg" configuration file.
        
        Recognised keyword arguments are:
            width=              # int >= 4
            height=             # int >= 4
            mines=              # int; Only integers are allowed here.
            gametype=           # str; 'moore', 'hex' or 'neumann'
            guessless=          # bool; Must be possible to solve without
                                #       guessing?
        '''
        # Define some constants.
        self.gametypes = ('moore', 'hex', 'neumann')
        
        # Handle parameters.
        default = {
            'width':     10,
            'height':    10,
            'mines':     10,
            'gametype':  'moore',
            'guessless': True,
        }
        for key in default:
            if key not in parameters:
                parameters[key] = default[key]
        assert parameters['gametype'] in ('neumann', 'hex', 'moore')
        
        self.cfg = load_cfg(cfgfile, 'enginecfg', [])
        if 'options' not in self.cfg['init-field']:
            self.cfg['init-field']['options'] = {}
        # Prevent DoS:
        area = parameters['width'] * parameters['height']
        try:
            if area > self.cfg['init-field']['sec-maxarea']:
                raise security_alert('Area too large, aborting')
        except KeyError as e:
            raise cfg_error("'sec-maxarea' missing in enginecfg\n"
                            "Original exception: {}".format(repr(e)))
        
        # Begin initialization.
        self.immortal_cheat = False   # Set this to True and you cannot lose!
        self.immortal_cheat_ever_used = False
        # The immortal flag is sent to the fields instances where incorrect
        # reveals and flags are prevented.
        self.dimensions = (parameters['width'], parameters['height'])
        self.gametype = parameters['gametype']
        self.n_mines = parameters['mines']
        self.guessless = parameters['guessless']

        if self.gametype == 'hex':
            self.field = fields.hexagonal_field(
                parameters['width'],
                parameters['height'],
                True    # Flagcount
            )
        else:
            self.field = fields.generic_field(
                [parameters['width'], parameters['height']],
                self.gametype == 'moore',
                True    # Flagcount
            )
        
        self.game_status = 'pre-game' # play-game game-won game-lost
        
        self.solver = solver.solver()
        self.solver.field = self.field

        # rule9bf
        if 'rule9bf-max' in self.cfg['init-field']['options']:
            rule9bf_max = self.cfg['init-field']['options']['rule9bf-max']
        else:
            rule9bf_max = 10
        self.solver.rule9bf_max = rule9bf_max

        if 'rule9bf-action' in self.cfg['init-field']['options']:
            rule9bf_action = self.cfg['init-field']['options']['rule9bf-action']
        else:
            rule9bf_action = 'deny'
        if rule9bf_action == 'allow':
            def action(arg):
                return False
        elif rule9bf_action == 'deny':
            def action(arg):
                return True
        elif rule9bf_action == 'trace':
            def action(arg):
                import os
                print('{}: {} cells to brute-force'.format(os.getpid(), arg))
                return True
        else:
            raise cfg_error(
                'Unknown value for "rule9b-action": "{}"'.format(rule9b_action)
            )
        self.solver.rule9bf_action = action
    

    def init_field2(self, startpoint):
        '''(Internal use.)  Uses enginecfg.
        
        Uses multiple processes to test random fields to find a
        solvable one.  When a process finds a solvable field, it will
        store the coordinates of the mines in a tempfile which will
        then be read by the master process.
        
        enginecfg['init-field']
            'force-procs'   int: Number of slaves (forced), default is automatic
            'procs'         int: Number of slaves (fallback)
            'maxtime'       float: Start over after having tried one field
                            for this long.
        '''

        # TODO: child_inner is really long and should be moved out from
        # init_field2, but it may reference variables in init_field2.

        def child(myqueue):
            try:
                child_inner(myqueue)
            except KeyboardInterrupt:
                pass

        def child_inner(myqueue, start_times=None):
            # We do not want anyting weird to happen when the parent
            # terminates the remaining children:
            def SIGTERM_handler(*args):
                sys.exit(0)
            signal.signal(signal.SIGTERM, SIGTERM_handler)
            # Example: see issue 14 in Gitlab

            # Drop priority
            if 'nice' in dir(os):
                if 'nice' in self.cfg['init-field']['options']:
                    nice = self.cfg['init-field']['options']['nice']
                else:
                    nice = 3
                os.nice(nice)

            debug = False
            if 'debug' in self.cfg['init-field']['options']:
                if self.cfg['init-field']['options']['debug']:
                    debug = True
            def debug_print(msg):
                if debug:
                    print(msg)
            if debug and start_times is None:
                start_times = os.times()

            rng = random.Random()
            all_cells = set(self.field.all_cells())
            # Initialize
            init_safe = set(self.field.get_neighbours(startpoint)+[startpoint])
            known_safe = init_safe
            known_mines = set()

            # Keep track of progress to revert if stuck
            progtracker = []
            # If stuck_check_len last elements are identical, then we are
            # probable stuck and need to revert to the state just before
            # all of the stuck states.
            if 'stuck-check' in self.cfg['init-field']['options']:
                stuck_check = self.cfg['init-field']['options']['stuck-check']
                if not isinstance(stuck_check, int):
                    raise cfg_error('stuck-check is not an integer')
                if stuck_check < 0:
                    raise cfg_error('stuck-check is negative')
                stuck_check_len = stuck_check + 1
            else:
                stuck_check_len = 2

            while True:
                # New random locations for unfound mines
                unknown = all_cells - known_safe - known_mines
                n_new_mines = self.n_mines - len(known_mines)
                # Use `sorted` instead of `list` to be consistent between
                # CPython and PyPy.
                new_mines = rng.sample(sorted(unknown), n_new_mines)
                # Recreate mine field
                all_mines = known_mines.union(new_mines)
                self.field.clear()
                self.field.fill(all_mines)
                # Bypass slow solver
                for cell in known_safe:
                    self.field.reveal(cell)
                for cell in known_mines:
                    self.field.flag(cell)

                # Try to solve remaining cells
                debug_print('Step')
                if self.solver.solve()[0]:
                    break

                # Find known mines and known safe cells
                known_mines = set()
                known_safe = set()
                for cell in all_cells:
                    if self.field.get(cell) == 'F':
                        known_mines.add(cell)
                    elif isinstance(self.field.get(cell), int):
                        known_safe.add(cell)

                progtracker.append((known_safe, known_mines))
                # Detect if stuck and revert to previous stage
                # If stuck_check_len last elements are identical, then we
                # are probably stuck and need to revert to the state just
                # before all of the stuck states.
                if len(progtracker) >= stuck_check_len:
                    tmp = progtracker[-stuck_check_len:]
                    if all(x == tmp[0] for x in tmp):
                        # Remove current and previous attempts
                        del progtracker[-stuck_check_len:]
                        if progtracker:
                            debug_print('Revert')
                            known_safe = progtracker[-1][0].copy()
                            known_mines = progtracker[-1][1].copy()
                            progtracker = progtracker[:-1]
                        else:
                            debug_print('Start over')
                            known_safe = init_safe.copy()
                            known_mines = set()
            # end while True

            # Verify solution, on rare occasions it fails
            self.field.clear()
            self.field.fill(all_mines)
            self.field.reveal(startpoint)
            if not self.solver.solve()[0]:
                debug_print("False solution, recursing")
                child_inner(myqueue, start_times)
                return

            if debug:
                stop_times = os.times()
                print(
                    '\r\n[solve-time] ' +
                    'user: {:6.2f}, sys: {:6.2}, real: {:6.2f}'.format(
                        stop_times[0] - start_times[0],
                        stop_times[1] - start_times[1],
                        stop_times[4] - start_times[4],
                    )
                )
            myqueue.put(all_mines)
            # os._exit(0) doesn't work here


        # FUNCTION STARTS HERE.
        # Clean up after whatever may have called us.
        self.field.clear()
        
        # Determine the proper amount of slaves:
        if 'force-procs' in self.cfg['init-field']['options']:
            nprocs = self.cfg['init-field']['options']['force-procs']
        else:
            try:
                nprocs = multiprocessing.cpu_count()
            except:
                nprocs = 4
            if 'max-procs' in self.cfg['init-field']['options']:
                max_procs = self.cfg['init-field']['options']['max-procs']
                nprocs = min(max_procs, nprocs)

        # multiprocessing gremlins
        me = os.getpid()

        # Create slaves
        if use_SimpleQueue:
            myqueue = multiprocessing.SimpleQueue()
        else:
            myqueue = multiprocessing.Queue()
        pool = []
        try:
            # This code can't be pickled, so platforms that lack fork can't
            # utilize multiprocessing.
            for i in range(nprocs):
                newproc = multiprocessing.Process(target=child, args=(myqueue,))
                newproc.start()
                pool.append(newproc)
        except Exception:
            # Effectively does this, but with timeout:
            #       child(myqueue)
            #       self.field.clear()
            # Normally timeout is handled at queue.get
            def alarm():
                # Timoeut is completely dysfunctional on Windows.
                # ~version 0.6.42 CTRL_C_EVENT doesn't work either
                os.kill(os.getpid(), signal.SIGINT)
            maxtime = self.cfg['init-field']['sec-maxtime']
            timer = threading.Timer(maxtime, alarm)
            try:
                timer.start()
                assert threading.active_count() > 1, "Timer failed to start"
                child(myqueue)
                timer.cancel()
                time.sleep(0.2)     # Try to catch slightly delayed SIGINT
            except KeyboardInterrupt:
                raise security_alert('Initialization took too long, aborted')
            # MUST clear the field since child didn't run in a separate process:
            self.field.clear()
        
        if os.getpid() != me:
            # multiprocessing gremlins
            os._exit(0)

        # Get data from first child, then kill the others
        try:

            # Basically do `mines = myqueue.get(True, timeout)`:
            # multiprocessing.Queue().get() raises queue.Empty if SIGWINCH is
            # caught on Python 2.7, so we need to check if it was a timeout
            # or a window size change.
            timeout = self.cfg['init-field']['sec-maxtime']
            start_time = time.time()
            while True:
                try:
                    if use_SimpleQueue:
                        mines = myqueue.get()
                    else:
                        mines = myqueue.get(True, timeout)
                    break
                except queue.Empty:
                    if (time.time()-start_time) > timeout:
                        raise

        except queue.Empty:
            raise security_alert('Initialization took too long, aborted')
        finally:
            for process in pool:
                process.terminate()
            # Both Process and Queue object take up open files.
            # FIXME: PyPy 2.7 still leaks file descriptors.
            # Process.close new in Python 3.7
            # PyPy 3.5 doesn't have Queue.close (assuming it also needs 3.7)
            if sys.version_info[0] == 3 and sys.version_info[1] >= 7:
                for process in pool:
                    process.join()
                    process.close()
                try:
                    myqueue.close()
                except AttributeError:
                    # SimpleQueue.close introduced in Python 3.9
                    pass

        # Fill the field with the mines.
        self.field.fill(mines)
        self.field.reveal(startpoint)
    

    def init_field(self, startpoint):
        '''Place the mines and reveal the starting point.
        
        Internal details:
            It will wrap in `init_field2` in guessless mode.
            It will place the mines by itself when not in guessless
            mode.
        '''

        self.saved_startpoint = startpoint

        if self.guessless:
            # Wrap in the best version.
            try:
                self.init_field2(startpoint)
            except KeyError as e:
                raise cfg_error("A KeyError exception occured, this is most"
                                " likely due to a mistake in enginecfg\n"
                                "Exception: {}".format(repr(e)))
        else:
            safe = self.field.get_neighbours(startpoint) + [startpoint]
            cells = list(filter(
                lambda x: x not in safe,
                self.field.all_cells()
            ))
            rng = random.Random()
            mines = rng.sample(cells, self.n_mines)
            self.field.clear()
            self.field.fill(mines)
            self.field.reveal(startpoint)
            
        def win(field, engine):
            engine.game_status = 'game-won'
            field.set_callback('win', None, None)
            field.set_callback('lose', None, None)
        def lose(field, engine):
            engine.game_status = 'game-lost'
            field.set_callback('win', None, None)
            field.set_callback('lose', None, None)
        self.field.set_callback('win', win, self)
        self.field.set_callback('lose', lose, self)
    

    def flag(self, coordinate):
        '''Automatic flag/unflag at `coordinate`.
        '''
        if self.field.get(coordinate) == 'F':
            self.field.unflag(coordinate)
        else:
            self.field.flag(coordinate)
    

    def reveal(self, coordinate):
        '''Automatic initialization/reveal at `coordinate`.
        '''
        if self.game_status == 'pre-game':
            # # Let the player choose the starting point.
            self.init_field(coordinate)
            self.game_status = 'play-game'
        elif self.game_status == 'play-game':
            self.field.reveal(coordinate)
        else:
            assert False, "game_status not in ('play-game', 'in-game')"
    

    def play_game(self, interface):
        '''bool_won, hiscores_obj = play_game(interface)
        
        Attach the interface to the engine and play the game.
        
        See the doc-string for the class as a whole for (more)
        information.

        To abort the game, `interface.input` (or `interface.output`)
        must raise an exception that will be caught by our caller.
        '''

        self.field.clear()

        # Enter the main loop.
        start_time = None

        while self.game_status in ('pre-game', 'play-game'):
            interface.output(self)
            interface.input(self)
            # Start timer
            if start_time is None and self.game_status == 'play-game':
                interface.anykey_cont()
                start_time = time.time()
            # Immortality cheat
            self.immortal_cheat_ever_used |= self.immortal_cheat
            try:
                self.field.immortal_cheat = self.immortal_cheat
            except AttributeError:
                pass
        # Won? Time?
        game_won = self.game_status == 'game-won'
        time_taken = time.time() - start_time

        # Show everything.
        if self.game_status == 'game-lost':
            # This takes a long time in some really weird configurations.
            # anonymine -m 1 -s 100x100
            for cell in self.field.all_cells():
                self.field.reveal(cell)
        interface.output(self)
        
        # Create a proper paramstring for the hiscores object.
        paramstring = '{}{}@{}x{}-{}'.format(
            {True: "", False: "lost/"}[game_won],
            self.n_mines,
            self.dimensions[0],
            self.dimensions[1],
            self.gametype
        )
        if not self.guessless:
            paramstring += '+losable'
        mines_left = 0
        if not game_won:
            # Count the remaining mines. Flags != mines.
            for cell in self.field.all_cells():
                if self.field.get(cell) == 'X':
                    mines_left += 1
            # Somehow missed more than 20% of all mines??
            # Probably cheating to get good time with very few flags remaining.
            fail = float(mines_left - self.field.flags_left)/self.n_mines
            if fail > .20:
                mines_left = self.n_mines * 42
        hs = hiscores(self.cfg['hiscores'], paramstring, time_taken, mines_left,
                      immortal_cheat=self.immortal_cheat_ever_used)
        
        interface.anykey_cont()         # ANY key
        return game_won, hs
