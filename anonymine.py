#!/usr/bin/python

'''A minesweeper that can be solved without guessing

Copyright (c) Oskar Skog, 2016-2024
Released under the FreeBSD (2-Clause) license.

A minesweeper that can be solved without guessing
=================================================

    This script contains a curses based interface class, a command line
    setup function, a line mode setup function and glue.

    The engine is in a separate module (anonymine_engine).
    
    The game support three different gametypes:
        Moore neighbourhoods: each cell has eight neighbours.
        Hexagonal cells: each cell has six neighbours.
        Von Neumann neighbourhoods: each cell has four neighbours.
'''

ANONYMINE_VERSION = (
    'Anonymine version MAKEFILE_GAME_VERSION\n'
    'Copyright (c) Oskar Skog, 2016-2024\n'
    'Released under the Simplified BSD license (2 clause).\n'
)

import argparse
import base64           # Only for cheatcodes
import curses
import errno
import hashlib          # Only for cheatcodes
import locale
import os
import platform
import signal
import subprocess
import sys
import time
import traceback        # Only for unexpected crashes

import anonymine_engine as game_engine

# Force InterruptedError and errno.EINTR to be defined
try:
    InterruptedError
except NameError:
    class InterruptedError(Exception):
        pass
try:
    errno.EINTR
except AttributeError:
    errno.EINTR = 'impossible-code'

# We'll need to convert between bytes and integers. This is of course
# different on Python 2 and Python 3
if sys.version_info[0] == 2:
    mychr = chr
    myord = ord
else:
    mychr = lambda x: bytes([x])
    def myord(x):
        if isinstance(x, int):
            return x
        else:
            return int(x[0])

# Jython hides the underlying OS
sys_platform = game_engine.sys_platform

# curses.curs_set can fail on some terminals
def try_curs_set(new_cursor):
    try:
        return curses.curs_set(new_cursor)
    except Exception:
        return None


# ### GLOBAL VARIABLES ###
allow_curses_getch_error = False    # Allow the first getch after a ^C to fail


# ### CONSTANTS ###

default_difficulties = {
        # Name      Mines, width, height
    'moore': [
        ('Easy',    31,    18, 17),
        ('Medium',  50,    21, 16),
        ('Default', 80,    20, 20),
        ('Hard',   128,    24, 18),
        ('Ultra',  205,    27, 19),
    ],
    'hex': [
        ('Easy',    31,    18, 17),
        ('Medium',  50,    21, 16),
        ('Default', 80,    20, 20),
        ('Hard',   128,    25, 19),
        ('Ultra',  205,    25, 24),
    ],
    'neumann': [
        ('Easy',    31,    18, 17),
        ('Medium',  50,    21, 16),
        ('Default', 80,    20, 20),
        ('Hard',   128,    27, 21),
        ('Ultra',  205,    38, 21),
    ],
}


# ### CHEAT CODES ###
# After seeing Decino's video on Doom's cheat codes I thought I could make
# something that's harder to crack.  There are still intentional weaknesses
# though.  Also these are completely useless.
#     The cheat function is used by the input method in the curses_game class
# hence it's written above it, also the cheat codes list is here.

cheatcodes = [
    (11, 'ec5287c45f0e70ec22d52e8bcbeeb640',    # List available cheats
        'U8mycngjZGVd3J6kPt+WDBFP9XaIVziwvsrzQ4cWHY93pL1wPwrS7vCRH7l6+OXRYh4'
        '/aA62O6nJ4i6QkD54E/a3ZGu6kIGCnUdnkoTHUi5WLBMYQGOZ'),
    (40, '807d18d25a79525d0e9c12c4955a23df',    # Thor mode 1
        '0CmFT1rCjDz1bR0Fob2CBBOPknYnFYpEHJ7CPAptfZTPMtgEf5q/8oYEEIuDE9Rxgl/'
        'jK3vioSOruUA1k5htWbw='),
    ( 5, '1247f4c2ee260d35f2489e2e0eb9125d',    # Thor mode 2
        '6QHZkzzNoQfBMg7GmYWi+C7K16vDgczE1dh2uQ8USELBkBrR8F6KwSNmYRJ8hD7rUPU'
        'TqoDo0ydXNhhI5IuXiqgjMtRlzTsquPOx'),
    ( 7, 'b9e2eb85b3af6e9a3f7877fa352e76ed',    # Thor mode 3
        'X7KGT8q/AGJ5yaK4aTzPkHXe7t/J7iQYKcNJTLL1BwYDkyP6XWzAZ29ig5/iHQDRUzQ'
        'msH6rZm03kR4nD/9CJvYv7HX2e4VCz/o4Dv2MHa+0vd3FBvbxalJPAZI2EidLd47f'),
    ( 8, '3b49536add82a13e0969e2fa12f8b958',    # Thor mode 4
        'MP5vzrwJH95Okn5QS7NUXsKrmpeuc+sG8rhs2Y1IDJdH3t6X9A2yT5/mJRHh0vLVC0j'
        'R2pCtRlc9V9kll+EuHTxMo7f9hopoaWFq8K+Uv0NqfU5FfZ6ZArb6Vn9W6qQ='),
    ( 8, '1fbbcba39b8fe0651b0deef30fcce810',    # Thor mode 5
        'qcicvwc2hD6UymDSwvbQ9n+xUpDvcgFq0Ydpub6UGPFY185dLe14eVCxTuXwUxjofWn'
        'n+fHT9+qmycKRIgw/5vpehaL4AP5ogtqgYg6sHA=='),
    (14, '6ee8bfe524178f3cd5ca00644c4e7247',    # Thor mode 6A
        '8GkRzduCBcCoxmVtpHA3IaCvEzWXzLOT9sw4fb9nf7C6fPAaOz55kJkLLN7g1ms1RDV'
        'Vrqu8gwKp07FotXYfGYPiSCXmJDUwLtqYm5Ot5B0/enrQGUk39W7V4KkShEEKZEKBTD'
        'CyTiU='),
    (16, 'a5a733790eddbda9a23f028e68afd37a',    # Thor mode 6B
        'YqjQSg46Rt+gZDt7CBCSAQjDTHJxLj1WTdSgyaovk+KMT+X61igxiHOwxyPn+QWEGYX'
        'RBTaAeYS2JI/pWwXEo39s3T37bidtdLsrnqAKHhpo43M1kMTlrXkdqPOg/KbSxlAYN2'
        'tjXig='),
    ( 9, 'be7cf0b2e9bc604400b5485c7703f53e',    # For Asciinema, 'letmetype'
        'zcQibK+3AolvVROKiwiS2mdmfMhlndCWKMEpwYPDcjcEQBL2F6/4LFv84V1xf8IcZwJ'
        'tVnditzV43CsFiqCc'),
    ( 8, '3f63978d1c5a543b71db5b4bd8958345',    # 4x lower case 'a' with umlaut
        '4vsQPE1+XjfAEBYGB8mzWfiE4hwe9bYD2goK'),
    (17, '9e58a2f773e12b20267b6485978a8821',    # Cheat code that goes *Ping*
        'bzNQtNwMMw=='),
]

# This is an ugly kludge. It should probably be a method in the `curses_game`.
def cheat(bytes_buf, display_thingy):
    '''cheat_code_detected, immortal_cheat_toggle = cheat(buffer, display)

    `buffer` is a bytes object of keyboard input.  Expecting UTF-8.'''
    def md5(s):
        'Return MD5 of bytestring `s` as hexadecimal string.'
        return hashlib.md5(s).hexdigest()

    def safestr(s):
        '''
        Assert that the string `s` does not contain any character that
        is not safe to print to a terminal.

        Returns `s`.  Raises AssertionError if `s` is unsafe.
        '''
        safe_chars = [7]                    # Character that goes *Ping*
        safe_chars.extend(range(32, 128))   # Space ... ~
        safe_chars.extend(range(160, 256))  # NBSP ... y with diaeresis
        if not all([ord(ch) in safe_chars for ch in s]):
            raise AssertionError("Unsafe string")
        return s
    
    # Detect if a correct cheat code has been entered, and which:
    for n_bytes, cheatcode_hash, big_ass_bytes in cheatcodes:
        used_cheatcode = bytes_buf[-n_bytes:]
        if md5(used_cheatcode) == cheatcode_hash:
            encrypted_command = base64.b64decode(big_ass_bytes.encode('ascii'))
            break
    else:
        return (False, False)   # not cheat code, not toggle god mode
    
    # The used cheatcode will be used as a key in a (probably very)
    # dodgy stream cipher.
    decrypted_command = b''
    for index, char in enumerate(encrypted_command):
        # Maximum message length is 256 since the index will be used as a
        # byte value.
        tmp_hash = md5(used_cheatcode + mychr(index))
        key = int(tmp_hash[-2:], 16)
        decrypted_command += mychr(myord(char) ^ key)
    command_parts = decrypted_command.split(b'$')

    # Unencrypted form:
    #       b'<message>$<command>'
    #       b'<message>$<command>$<arg>$<arg>'
    # All strings are UTF-8 encoded and anything that will be on the screen
    # should go through `safestr` because who knows what they might contain.
    #
    # Commands:             Arguments           Use
    #       b'immortal'     -                   To be known as a CHEATER
    #       b'text-input'   -                   Annotating an Asciinema
    #       b'crash'        integer, message2   Punishment
    #       b'no-op', *     -                   Less harsh punishment
    message = safestr(command_parts[0].decode('utf-8'))
    command = command_parts[1]
    try:
        command_args = command_parts[2:]
    except IndexError:
        pass
    
    # Always print a message
    display_thingy.cheatcode_message = message

    if command == b'immortal':
        return (True, True)     # cheat code, toggle god mode
    elif command == b'text-input':
        # We won't see the cheat code message normally until we are finished.
        # We must instead use the `message` method which normally doesn't work.
        # Let's clear the cheat code message while at it to remove clutter.
        display_thingy.cheatcode_message = ''
        display_thingy.message(message)
        # Leave curses temporarily
        # BUG: Looks ugly on Windows and broken on NetBSD.
        # TODO: Do this in curses instead?
        try_curs_set(2)
        curses.reset_shell_mode()
        # Let there be input
        sys.stdout.write('\r')
        sys.stdout.flush()
        sys.stdin.readline()
        # Return to curses
        curses.reset_prog_mode()
        display_thingy._init_mouse()
        try_curs_set(0)
    elif command == b'crash':
        # Is there a nicer way of displaying the last message before the
        # crash?
        display_thingy.message(message)
        time.sleep(5)
        # Raise a weird exception, maybe even an IndexError or ValueError!
        ways_to_crash = [
            SystemExit, KeyboardInterrupt, AssertionError, OSError
        ]
        exception = ways_to_crash[int(command_args[0])]
        message2 = safestr(command_args[1].decode('utf-8'))
        raise exception(message2)
    else:
        # No-op command
        pass

    return (True, False)    # cheat code, not toggle god mode


# ### CLASS DEFINITIONS ###

class curses_game():
    '''Class for interface object for `engine.play_game(interface)`.
    
    This is a large part of the curses mode interface for Anonymine.
    The "engine" is in the module `anonymine_engine` and could be used
    by a different interface.
    
    The engine is responsible for creating the "field", initialization
    of the field (filling it with mines) and the main loop of a game.
    
    anonymine_engine.game_engine(**params).play_game(interface)
    
    `interface` is an object that needs to provide these methods:
        input(self, engine)
        output(self, engine)
        anykey_cont(self)
    
    It is recommended that you read the documentation for the engine
    as well.
    
    
    Coordinates
    ===========
    
        The "cursor" marks the selected cell on the field. It is a
        "field coordinate".
        
        A "virtual coordinate" is a coordinate on an imaginary screen.
        A virtual coordinate doesn't need to be in the "visible area".
        A virtual coordinate can easily be generated from a field
        coordinate.
        
        The visible area are those virtual coordinates that exist on
        the screen.  The visible area can be moved with
        `self.window_start`.
        
        A "real coordinate" is a coordinate on the screen and can be
        sent to the methods of `self.window`.
        
        real[0] = virtual[0] - window_start[0]
        real[1] = virtual[1] - window_start[1]
        
        The screen is `self.window`, not the real one.
    
    
    Externally used methods
    =======================
    
        interface = curses_game(cfgfile, gametype)
            `cfgfile` is the path to the cursescfg configuration file
                (the configuration file with the key bindings and
                textics.)
            `gametype` is either 'moore', 'hex' or 'neumann'.
            
            NOTICE: `curses_game.__init__` will enter curses mode.
            WARNING: `curses_game.__init__` MAY raise an exception
                while in curses mode if something is wrong with the
                configuration or if bad parameters are given.
        
        interface.leave()
            Leave curses mode.
        
        interface.input(engine)
            (This method is called by `engine.play_game`.)
            Take command from user and act on `engine.field`.
        
        interface.output(engine)
            (This method is called by `engine.play_game`.)
            Print the field (`engine.field`) to the screen.
            Prints the flags left text, invokes `self.print_square` or
            `self.print_hex`, and finally, `self.window.refresh`.
        
        interface.anykey_cont()
            (This method is called by `engine.play_game`.)
            Pause until input from user.
            "Press any key to continue..."
    
    
    Attributes
    ==========
    
        interface.window = curses.initscr()
            The screen.
        
        interface.cursor = (0, 0)
            The *field* coordinate.
        
        interface.window_start = [0, 0]
            Needed for translating virtual coordinates into real
            coordinates.
    
    
    Internally used methods
    =======================
    
        char, attributes = self.curses_output_cfg(key)
            "Parse" the configuration (cursescfg) and pick the most
            appropriate mode for the property `key`.  The color pair
            is properly attached to `attributes`.
        
        self.message(msg)
            Print `msg` at the bottom of the screen while in curses
            mode.  Invokes `self.window.refresh`.
            Used for printing the initialization message and by
            `self.anykey_cont`.
        
        self.travel(engine, direction)
            Modify `self.cursor` to select a new cell in the
            specified direction.
        
        self.print_char(x, y, cfg, char=None)
            Print a character at the virtual coordinate (x, y) using
            the textics property `cfg`.  `char` will override the
            character specified in the textics directive.
        
        self.move_visible_area(virtual_x,virtual_y,x_border,y_border)
            Modify `self.window_start` so (virtual_x, virtual_y) is
            visible on the screen, and not too close to any edge.
        
        self.print_square(field)
            Print Moore and Neumann fields and the "cursor" to the
            screen.  Does not invoke `self.window.refresh` and does
            not print the flags left text.
        
        self.print_hex(field)
            Print hexagonal fields and the "cursor" to the screen.
            Does not invoke `self.window.refresh` and does not print
            the flags left text.
    
    
    Constants
    =========
    
        self.travel_diffs
            A dictionary of dictionaries to translate a direction
            into DELTA-x and DELTA-y.  Used by `self.travel`.
        
        self.direction_keys
            A dictionary of lists representing the valid directions
            for a certain gametype.  This allows square and hex
            directions to share the same keys.
            Used by `self.input`.
        
        self.specials
            The method `get` of the field object may return one of
            these value or a normal number.  (Normal numbers will be
            printed using their digit as character and the textics
            directive 'number'.)
            This dictionary maps the special return values into
            textics properties.
    '''
    
    def __init__(self, cfgfile, gametype):
        '''Create interface object and enter curses mode.
        
        `cfgfile` is the path to the cursescfg file.
        `gametype` must be 'moore', 'hex' or 'neumann'.
        
        WARNING: This does not leave curses mode on exceptions!
        '''
        
        # Constants
        self.travel_diffs = {
            'square': {
                'up':           (0, -1),
                'right':        (1,  0),
                'down':         (0,  1),
                'left':        (-1,  0),
                'NE':           (1, -1),
                'SE':           (1,  1),
                'SW':          (-1,  1),
                'NW':          (-1, -1),
            },
            'hex-even': {
                'hex0':         (0, -1),         # 5 0
                'hex1':         (1,  0),        # 4   1
                'hex2':         (0,  1),         # 3 2
                'hex3':        (-1,  1),
                'hex4':        (-1,  0),        # x - 1 and x on even
                'hex5':        (-1, -1),        # rows.
            },
            'hex-odd': {
                'hex0':         (1, -1),
                'hex1':         (1,  0),        # x and x + 1 on odd
                'hex2':         (1,  1),        # rows.
                'hex3':         (0,  1),
                'hex4':        (-1,  0),
                'hex5':         (0, -1),
            }
        }
        self.direction_keys = {
            'hex': ['hex0', 'hex1', 'hex2', 'hex3', 'hex4', 'hex5'],
            'square': ['up', 'NE', 'right', 'SE', 'down', 'SW', 'left', 'NW'],
        }
        self.specials = {
            0:          'zero',
            None:       'free',
            'F':        'flag',
            'X':        'mine',
        }
        # Initialize...
        self.gametype = gametype
        self.window_start = [0, 0]      # Item assignment
        self.cursor = (0, 0)
        self.reveal_select = None       # Inhibit click if field scrolled
        self.flag_select = None         # Inhibit click if field scrolled
        self.attention_mode = False
        self.cheatcode_input = b''
        self.cheatcode_message = u''
        self.last_cheatcode_msg = 0     # Time
        self.immortal = False
        # Initialize curses.
        self.window = curses.initscr()
        curses.cbreak()
        curses.noecho()
        curses.meta(1)
        self.window.keypad(True)
        self.old_cursor = try_curs_set(0)
        
        # Check that we have a reasonable size on the window.
        def toosmall():
            self.leave()
            sys.stdout.flush()
            output(sys.stderr,'\nSCREEN TOO SMALL\n')
            sys.stderr.flush()
            sys.exit(1)
        height, width = self.window.getmaxyx()
        if self.gametype == 'hex' and (width < 10 or height < 8):
            toosmall()
        if self.gametype != 'hex' and (width < 7 or height < 4):
            toosmall()

        # 0.6.14: Changing the window size causes window.getch() to return
        # curses.ERR on OpenBSD
        self.last_size = (height, width)
        self.curses_input_errors = 0
        
        # CONFIGURATION
        self.cfg = game_engine.load_cfg(cfgfile, 'cursescfg', ['curses'])
        
        # New optional part "options"
        default_options = {
            'autorefresh':      False,
            'disable-mouse':    False,
        }
        if 'options' not in self.cfg:
            self.cfg['options'] = {}
        options = self.cfg['options']
        for option in default_options:
            if option not in options or options[option] == 'auto':
                options[option] = default_options[option]
            elif options[option] == 'on':
                options[option] = True
            elif options[option] == 'off':
                options[option] = False
            else:
                raise game_engine.cfg_error(
                    'Invalid value "{}" for "{}" in cursescfg '
                    '"options"'.format(options[option], option)
                )

        # Catch KeyError while reading required attributes and convert
        # to game_engine.cfg_error if any is missing.
        try:
            # Apply ord() automatically to the keys in 'curses-input':
            # This is done in-place.
            for key in self.cfg['curses-input']:
                for index in range(len(self.cfg['curses-input'][key])):
                    value = self.cfg['curses-input'][key][index]
                    if isinstance(value, str):
                        self.cfg['curses-input'][key][index] = ord(value)
                        # Append upper-case version of keys
                        if value.islower():
                            upper = value.upper()
                            self.cfg['curses-input'][key].append(ord(upper))
            # Initialize the color pairs.
            self.color_pairs = []
            if curses.has_colors():
                self.use_color = True
                # TODO: Check that enough pairs are available.
                curses.start_color()
                for key in self.cfg['curses-output']:
                    value = self.cfg['curses-output'][key]
                    ch, foreground, background, attr = value
                    # Only add new pairs.
                    if (foreground, background) not in self.color_pairs:
                        self.color_pairs.append((foreground, background))
                        curses.init_pair(
                            len(self.color_pairs),
                            eval('curses.COLOR_' + foreground),
                            eval('curses.COLOR_' + background)
                        )
                # Workaround for NetBSD bug (PDcurses maybe?):
                if curses.color_pair(1) == curses.A_STANDOUT:
                    curses.color_pair = lambda x: 131072 * x
                    # Python 2.7, NetBSD 10.0:      affected
                    # Python 3.8-3.12, NetBSD 10.0: not affected
            else:
                self.use_color = False
                # Make sure invisible is not supported on ANY monochrome
                # terminal.  This is vestigial code from 0.6.37, see
                # doc/latest-tested in the 0.6.37 branch in git.
                curses.A_INVIS = 0
        except KeyError as e:
            raise game_engine.cfg_error(
                'Invalid cursescfg configuration file\n'
                'Original exception: {}'.format(repr(e))
            )

        # Finish initializing
        self._init_mouse()
        curses.def_prog_mode()


    def _init_mouse(self):
        '''This function exists because mosue gets disabled after
        a new minefield was generated.'''
        if self.cfg['options']['disable-mouse']:
            return
        try:
            mask = curses.REPORT_MOUSE_POSITION|curses.ALL_MOUSE_EVENTS
            curses.mousemask(mask)
            curses.mouseinterval(self.cfg['curses-mouse-input']['interval'])
        except AttributeError:
            pass
        except KeyError:
            raise game_engine.cfg_error(
                "'curses-mouse-input' missing or incomplete in cursescfg "
                "configuration file"
            )


    def leave(self):
        '''Leave curses mode.'''
        # Erase the screen, otherwise the old content will be re-drawn
        # on new game start.
        self.window.erase()
        # Leave curses mode
        curses.nocbreak()
        curses.echo()
        self.window.keypad(False)
        try_curs_set(self.old_cursor)
        curses.endwin()
        curses.echo()           # Somehow fixes echo issue on NetBSD


    def curses_output_cfg(self, key):
        '''Retrieve textics directive from cursescfg.
        
        char, attributes = self.curses_output_cfg(key)
        
        `key` is the property in cursescfg ('curses-output').
        `char` is a character and needs to be converted before passed
            to a curses function.
        `attributes` is an integer to be passed directly to a curses
            function.  Color is or'ed in.
        
        Retrieve a textics directive from the configuration file
        (cursescfg).  This function is responsible to choose the
        key with the correct property and best available mode.
        
        Raises KeyError if the entry can't be found.
        
        gametype        Best available mode     2nd best        worst
        'moore':        ':moore'                ':square'       ''
        'hex':          ':hex'                  ''
        'neumann'       ':neumann'              ':square'       ''
        
        This function will automatically convert the directive line
        into two directly useful parts:
            `char`:             The character to be printed or `None`.
            `attributes`:       The attributes to be used (curses).
                                The color pair is also or'ed in.
        
        See also: the configuration file.
        '''
        cfg = self.cfg['curses-output']
        # Choose gametype specific entries if available
        if self.gametype == 'neumann':
            if key + ':neumann' in cfg:
                key += ':neumann'
            elif key + ':square' in cfg:
                key += ':square'
        elif self.gametype == 'hex':
            if key + ':hex' in cfg:
                key += ':hex'
        elif self.gametype == 'moore':
            if key + ':moore' in cfg:
                key += ':moore'
            elif key + ':square' in cfg:
                key += ':square'
        if key not in cfg:
            raise game_engine.cfg_error("Required key '{}' is not in"
                                        "'curses-output' in cursescfg"
                                        " configuration file".format(key))
        # Translate the key into (char, attributes)
        char, foreground, background, attributes = cfg[key]
        if self.use_color:
            attributes |= curses.color_pair(
                self.color_pairs.index((foreground, background)) + 1
            )
        return char, attributes


    def message(self, msg):
        '''Print `msg` at the bottom of the screen while in curses mode.
        
        Invokes `self.window.refresh`.
        Used for printing the initialization message and by
        `self.anykey_cont`.
        '''
        self._message(msg)
        self.window.refresh()
    

    def _message(self, msg):
        '''Internal use. It's used by `message` which refreshes the window
        and by `output` which needs to know how many lines to reserve.
        
        Print `msg` at the bottom of the screen while in curses mode.
        
        Return value: number of lines.
        '''
        if sys.version_info[0] == 2:
            msg = msg.encode(sys.stdout.encoding, errors='xmlcharrefreplace')
        height, width = self.window.getmaxyx()
        text_width = width - 4  # Pretty margin on the left.
        _, clear_attributes = self.curses_output_cfg('background')
        _, msg_attributes = self.curses_output_cfg('text')
        lines = len(msg)//text_width + 1
        if lines <= height:
            for line in range(lines):
                y, x = height - lines + line, 3
                # Clear line area
                self.window.addstr(y, x, ' '*text_width, clear_attributes)
                # Write new message
                line_text = msg[line*text_width:(line+1)*text_width]
                self.window.addstr(y, x, line_text, msg_attributes)
        else:
            # A screen this small? Seriously?
            # 0 lines would be correct, but `output` may assume it's at least
            # one line.
            lines = 1
        return lines
    

    def anykey_cont(self):
        '''Press any key to continue...
        
        Wait for input from the user, discard the input.
        
        (This method is called by `engine.play_game`.)
        '''
        self.message('Press the "any" key to continue...')
        self.window.getch()


    def output(self, engine):
        '''This method is called by `engine.play_game`.
        
        It erases the window, prints the flags left message if it would
        fit on the screen, invokes the appropriate field printer and
        refreshes the screen. (In that order.)
        '''
        # Set the appropriate background.
        char, attributes = self.curses_output_cfg('background')
        self.window.bkgdset(32, attributes)     # 32 instead of `char`.
        # NOTE: window.bkgdset causes a nasty issue when the background
        # character is not ' ' and color is unavailable.
        
        # Print the screen.
        self.window.erase()
        # Screen could resized at any time.
        self.height, self.width = self.window.getmaxyx()
        
        chunks = []
        if self.cheatcode_message:
            chunks.append(self.cheatcode_message)
        if engine.game_status == 'pre-game':
            chunks.append('Choose your starting point.')
        if engine.game_status == 'play-game':
            chunks.append("Flags left: {0}".format(engine.field.flags_left))
        
        msg = ' ||| '.join(chunks)
        # (Keeping the following [*] outside the loop magically solves a
        # resizing bug that traces back to an `addch` in `self.print_char`.)
        # Lie to the field printer functions to preserve the text.
        # * NOTE: Code has changed, this used to duplicate some functionality
        # found in message, the common part has been moved out to _message.
        self.height -= self._message(msg)
        
        # Print the field.
        if self.gametype == 'hex':
            self.print_hex(engine.field)
        else:
            self.print_square(engine.field)
        # Remember that self.height has already been decremented by one.
        self.window.move(self.height, 0)
        self.window.refresh()


    def input(self, engine):
        '''This method is called by `engine.play_game`.
        
        It receives a character from the user and interprets it.
        Invokes `self.travel` for the steering of the cursor.
        
        It doesn't do any output except for printing the field
        initialization message, and forcing the entire screen to be
        redrawn on unrecognised input (to de-fuck-up the screen).
        '''
        if self.gametype == 'hex':
            direction_keys = self.direction_keys['hex']
        else:
            direction_keys = self.direction_keys['square']
        look_for = ['reveal','flag','toggle-attention','quit']+direction_keys

        # Receive input from player:
        ch = self.window.getch()
        # If KeyboardInterrupt was generated last time window.getch was
        # called, it will return curses.ERR.  Allow it to fail once after
        # a KeyboardInterrupt has been raised.
        # 0.6.14: Changing the window resolution also causes this on OpenBSD.
        # Checking if the window size has changed usually works, but sometimes
        # doesn't.  Maybe it changed and then changed back?
        # Allow:
        #   - One error after ^C
        #   - Any number of errors if the window size has changed
        #   - An arbitrary number of errors otherwise.
        global allow_curses_getch_error
        if ch == curses.ERR and not allow_curses_getch_error:
            current_size = self.window.getmaxyx()
            if self.last_size == current_size:
                self.curses_input_errors += 1
                if self.curses_input_errors > 10:
                    raise SystemExit("curses input error (Terminal hang-up?)")
            else:
                self.last_size = current_size
        else:
            self.curses_input_errors = 0
        allow_curses_getch_error = False

        # Deal with cheat codes:
        self.cheatcode_input = self.cheatcode_input[-64:] + mychr(ch%256)
        cheat_detected, toggle_immortal = cheat(self.cheatcode_input, self)
        if toggle_immortal:
            if self.immortal:
                self.cheatcode_message = "God mode deactivated!"
            self.immortal = not self.immortal
            engine.immortal_cheat = self.immortal
        if cheat_detected:
            self.last_cheatcode_msg = time.time()
        if time.time() - self.last_cheatcode_msg > 5:
            self.cheatcode_message = ""

        # Interpret input from player:
        command = None
        if ch == curses.KEY_MOUSE:
            _, x, y, _, buttons = curses.getmouse()
            if self.gametype == 'hex':
                valid_coord = self.mouse_travel_hex(
                    x, y, engine.field
                )
            else:
                valid_coord = self.mouse_travel_square(
                    x, y, engine.field, (engine.game_status == 'pre-game')
                )
            if valid_coord:
                # Unsafe actions first, safe actions last
                mouse_commands = (
                    'reveal', 'reveal-confirm', 'reveal-select',
                    'flag', 'flag-confirm', 'flag-select',
                )
                # Last valid action will take effect
                for test_command in mouse_commands:
                    try:
                        masks = self.cfg['curses-mouse-input'][test_command]
                    except KeyError:
                        continue
                    for mask in masks:
                        if buttons & mask:
                            command = test_command
                # Reveal/flag, but only if the field hasn't moved
                if command == 'reveal-select':
                    self.reveal_select = self.cursor
                    command = None
                if command == 'reveal-confirm':
                    if self.reveal_select == self.cursor:
                        command = 'reveal'
                if command == 'flag-select':
                    self.flag_select = self.cursor
                    command = None
                if command == 'flag-confirm':
                    if self.flag_select == self.cursor:
                        command = 'flag'
        else:
            # Keyboard input:
            try:
                for key in look_for:
                    if ch in self.cfg['curses-input'][key]:
                        command = key
            except KeyError:
                raise game_engine.cfg_error(
                    "Required key '{}' is not in 'curses-input' in cursescfg"
                    " configuration file".format(key)
                )

        # Act:
        if command == 'flag':
            engine.flag(self.cursor)
        elif command == 'reveal':
            pre_game = engine.game_status == 'pre-game'
            if pre_game:
                self.message('Initializing field...   This may take a while.')
            engine.reveal(self.cursor)
        elif command in direction_keys:
            self.travel(engine.field, command)
        elif command == 'toggle-attention':
            self.attention_mode = not self.attention_mode
        elif command == 'quit':
            # Originally needed on Windows because windows-curses doesn't
            # raise KeyboardInterrupt on ctrl+C.  'Q' was later added as a
            # universal quit key.
            raise KeyboardInterrupt
        elif ch != curses.KEY_MOUSE and not self.cfg['options']['autorefresh']:
            # Command to refresh the screen
            #   Don't do it on random mouse events
            #   And don't do it twice if autorefresh=True
            self.window.redrawwin()

        # Some platforms need to always refresh the screen.
        if self.cfg['options']['autorefresh']:
            self.window.redrawwin()
    

    def travel(self, field, direction):
        '''Move the cursor in the specified direction.
        
        It will not move past an edge (or in an otherwise impossible
        direction).  This is why the `field` argument is required.
        
        Valid directions when self.gametype == 'moore':
            'up', 'NE', 'right', 'SE', 'down', 'SW', 'left', 'NW'
        Valid directions when self.gametype == 'hex':
            'hex0', 'hex1', 'hex2', 'hex3', 'hex4', 'hex5'
        Valid directions when self.gametype == 'neumann':
            'up', 'right', 'down', 'left'
        
        The hexagonal directions are:
             5 0
            4   1
             3 2
        '''
        x, y = self.cursor
        # Find the appropriate dictionary of direction to DELTA-x and DELTA-y.
        if self.gametype != 'hex':
            key = 'square'
        elif y % 2:
            key = 'hex-odd'
        else:
            key = 'hex-even'
        
        # Move in the specified direction.
        x_diff, y_diff = self.travel_diffs[key][direction]
        new = x + x_diff, y + y_diff
        # Do nothing if it is impossible to move in the specified direction.
        x, y = new
        if x >= 0 and x < field.dimensions[0]:
            if y >= 0 and y < field.dimensions[1]:
                self.cursor = new
    

    def move_visible_area(self, virtual_x, virtual_y, x_border, y_border):
        '''Move the area that will be printed by `self.print_char`.
        
        Move the visible area (as printed by `self.print_char`) by
        modifying `self.window_start`, which is used for translating
        virtual coordinates (a step between field coordinates and
        screen coordinates.)
        
        `virtual_x` and `virtual_y` is the virtual coordinate.
        `x_border` is the minimal allowed border between the virtual
        coordinate and the left or the right side of the screen.
        `y_border` is the minimal allowed border between the virtual
        coordinate and the top or the bottom of the screen.
        '''
        real_x = virtual_x - self.window_start[0]
        real_y = virtual_y - self.window_start[1]
        if real_x + x_border > self.width - 1:
            self.window_start[0] = virtual_x - self.width + x_border + 1
        if real_x - x_border < 0:
            self.window_start[0] = virtual_x - x_border
        if real_y + y_border > self.height - 1:
            self.window_start[1] = virtual_y - self.height + y_border + 1
        if real_y - y_border < 0:
            self.window_start[1] = virtual_y - y_border
    

    def print_char(self, x, y, cfg, char=None):
        '''Print a character at a virtual coordinate with the right attributes.
        
        Print a character at the virtual coordinate (`x`, `y`)
        using the textics directive `cfg`.
        
        `char` is used to override the default character of the
        textics directive.
        '''
        real_x = x - self.window_start[0]
        real_y = y - self.window_start[1]
        # Verify that the coordinate is printable.
        if 0 <= real_x < self.width:
            if 0 <= real_y < self.height:
                cfg_char, attributes = self.curses_output_cfg(cfg)
                if char is None:
                    char = cfg_char
                self.window.addstr(real_y, real_x, char, attributes)
    

    def print_digit(self, x, y, digit):
        '''Print a digit at a virtual coordinate.
        
        Introduced in 0.4.9 to allow digits to have different colors.
        '''
        try:
            self.print_char(x, y, str(digit))
        except KeyError:
            self.print_char(x, y, 'number', str(digit))
    

    def print_cell(self, x, y, field, cell):
        '''
        `x` and `y` is the virtual coordinate for the single character
        to be printed.
        
        `cell` is the cell from the field.
        
        Introduced in 0.4.15 to reduce code duplication and apply the
        attention mode to numbers with too many mines around them.
        '''
        value = field.get(cell)
        if value not in self.specials:
            if self.attention_mode:
                flags = 0
                for neighour in field.get_neighbours(cell):
                    if field.get(neighour) == 'F':
                        flags += 1
                if flags > value:
                    self.print_char(x, y, 'attention', str(value))
                    return
            self.print_digit(x, y, value)
        else:
            if value is None and self.attention_mode:
                self.print_char(x, y, 'attention')
            else:
                self.print_char(x, y, self.specials[value])
    

    def print_square(self, field):
        '''Helper function for `self.output` for non-hexagonal gametypes.
        
        Print a non-hexagonal field in the area
        0 to self.width-1 by 0 to self.height-2.
        
        Also prints the "cursor".
        It does not print the flags left text.
        
        It will invoke `self.move_visible_area` to keep the "cursor" on
        the screen.  It will use `self.print_char` to print characters
        on the screen.
           _______
          | X X X |
          | X(*)X |
          | X X X |
           -------
        '''
        # Move the visible area.
        # Compute the virtual locations on the screen and real locations.
        # Adjust the virtual coordinate of the visible area.
        #
        # Border = 1 cell.
        x, y = self.cursor
        self.move_visible_area(2*x+1, y, 3, 1)
        
        # Print all cells in a field.
        for cell in field.all_cells():
            x, y = cell
            # Print blank grid .
            self.print_char(2*x, y, 'grid', ' ')
            self.print_char(2*x+2, y, 'grid', ' ')
            # Print the actual cell.
            self.print_cell(2*x+1, y, field, cell)
        # Print the "cursor".
        x, y = self.cursor
        self.print_char(2*x, y, 'cursor-l')
        self.print_char(2*x+2, y, 'cursor-r')
    

    def mouse_travel_square(self, x, y, field, pre_game=False):
        '''
        TODO: What does this return?
        '''
        def autoaim(x, y, field, pre_game):
            '''return x = (x-1) // 2   # But with autoaim'''
            # NOTE: This depends on the layout.  If the layout is changed,
            #       this will break.
            # Right on the target:
            if x % 2:
                return (x-1) // 2
            # Between two positions:
            left_x = x//2 - 1
            right_x = x//2
            try:
                left = field.get([left_x, y])
            except IndexError:
                return right_x      # Left edge
            try:
                right = field.get([right_x, y])
            except IndexError:
                return left_x       # Right edge
            # Safe to guess:
            if pre_game:
                return left_x
            # Unsafe to guess:
            if left is None and right is None:
                return False
            # Only one blank:
            if left is None:
                return left_x
            elif right is None:
                return right_x
            # Same but for flags:   (None of the cells are blank.)
            if left == 'F' and right == 'F':
                return False
            elif left == 'F':
                # Number on the right, remove flag on the left
                return left_x
            elif right == 'F':
                # Number on the left, remove flag on the right
                return right_x            
            # Neither are blank nor flags:
            return False
                
        # == Main function ==
        x += self.window_start[0]
        y += self.window_start[1]
        x = autoaim(x, y, field, pre_game)
        if x is False:
            return False
        # Travel
        if 0 <= x < field.dimensions[0] and 0 <= y < field.dimensions[1]:
            self.cursor = (x, y)
            return True
        else:
            return False


    def print_hex(self, field):
        r'''Helper function for `self.output` for the hexagonal gametype.
        
        Print a hexagonal field in the area
        0 to self.width-1 by 0 to self.height-2.
        
        Also prints the "cursor".
        It does not print the flags left text.
        
        It will invoke `self.move_visible_area` to keep the "cursor" on
        the screen.  It will use `self.print_char` to print characters
        on the screen.
        
            0000000000111111111122222222223
            0123456789012345678901234567890
        00   / \ / \ / \ / \ / \ / \ / \
        01  | X | X | X | X | X | X | X |
        02   \ / \ / \ / \ / \ / \ / \ / \
        03    | X | X | X | X | X | X | X |
        04   / \ / \ / \ / \ / \ / \ / \ /
        05  | X | X | X |(X)| X | X | X |
        06   \ / \ / \ / \ / \ / \ / \ / \
        07    | X | X | X | X | X | X | X |
        08     \ / \ / \ / \ / \ / \ / \ /
        '''
        # Define functions that translates field coordinates into
        # virtual screen coordinates.
        def fx(x, y): return 2 * (2*x + 1 + (y % 2))
        
        def fy(x, y): return 2*y + 1
        
        # Move the visible area.
        #
        # Compute the virtual locations on the screen and real locations.
        # Adjust the virtual coordinate of the visible area.
        # Border = 1 cell.
        x, y = self.cursor
        self.move_visible_area(fx(x, y), fy(x, y), 6, 3)
        
        # Draw the grid in 4 layers:
        #   grid4: field.all_cells()
        #   grid3: neighbours2
        #   grid2: neighbors
        #   grid: self.cursor
        neighbours = field.get_neighbours(self.cursor)
        neighbours2 = set()
        for cell in neighbours:
            neighbours2.update(field.get_neighbours(cell))

        # Print all cells in a field.
        ordered_cells = (
            field.all_cells()
            + list(neighbours2)
            + neighbours
            + [self.cursor]
        )
        for cell in ordered_cells:
            x = 2 * (2*cell[0] + 1 + (cell[1] % 2))
            y = 2*cell[1] + 1
            
            # Print blank grid.
            if cell == self.cursor:
                grid = 'grid'
            elif cell in neighbours:
                grid = 'grid2'
            elif cell in neighbours2:
                grid = 'grid3'
            else:
                grid = 'grid4'
            # Roof:
            self.print_char(x - 1, y - 1, grid, '/')
            self.print_char(x, y - 1, grid, ' ')
            self.print_char(x + 1, y - 1, grid, '\\')
            # Left wall:
            self.print_char(x - 2, y, grid, '|')
            self.print_char(x - 1, y, grid, ' ')
            # Right wall:
            self.print_char(x + 2, y, grid, '|')
            self.print_char(x + 1, y, grid, ' ')
            # Floor:
            self.print_char(x - 1, y + 1, grid, '\\')
            self.print_char(x, y + 1, grid, ' ')
            self.print_char(x + 1, y + 1, grid, '/')
            # Print the actual cell.
            self.print_cell(x, y, field, cell)
        
        # Print the "cursor".
        x, y = self.cursor
        self.print_char(fx(x, y) - 1, fy(x, y), 'cursor-l')
        self.print_char(fx(x, y) + 1, fy(x, y), 'cursor-r')
    

    def mouse_travel_hex(self, x, y, field):
        '''
        TODO: What does this return?
        '''
        x += self.window_start[0]
        y += self.window_start[1]
        if x % 4 == 2 and y % 4 == 0 or x % 4 == 0 and y % 4 == 2:
            y += 1              # Right above the target
        if x % 4 == 2 and y % 4 == 2 or x % 4 == 0 and y % 4 == 0:
            y -= 1              # Right below the target
        if not y % 2:
            return False        # Misc place on horizontal border
        if y % 4 == 3:          # Unpush the pushed rows.
            x -= 2
        if not x % 4:           # Vertical border
            return False
        y = y // 2
        x = x // 4
        # Travel
        if 0 <= x < field.dimensions[0] and 0 <= y < field.dimensions[1]:
            self.cursor = (x, y)
            return True
        else:
            return False


# ### FUNCTION DEFINITIONS ###
# Except `cheat` which is defined above the class definitions.

def clear_screen():
    '''Run clear(1) or cls'''
    if sys_platform.startswith('win'):
        os.system('cls')
    else:
        os.system('clear')

def output(stream, content):
    '''
    Due to a bug syscalls may fail with EINTR after leaving curses mode.
    
    Write `content` to `stream` and flush() without crashing.
    
    Example:
    output(sys.stdout, 'Hello world!\n')
    
    The bug
    =======
    
        sys.stdin.readline() in `ask` dies with IOError and
        errno=EINTR when the terminal gets resized after curses has
        been de-initialized.
        1: SIGWINCH is sent by the terminal when the screen has been
            resized.
        2: curses forgot to restore the signal handling of SIGWINCH
            to the default of ignoring the signal.
            NOTE: signal.getsignal(signal.SIGWINCH) incorrectly
                returns signal.SIG_DFL. (Default is to be ignored.)
        3: Python fails to handle EINTR when reading from stdin.
    REFERENCES:
        Issue 3949: https://bugs.python.org/issue3949
        PEP 0457: https://www.python.org/dev/peps/pep-0475/

    FIXME 2024:
        - Can't I just call signal.signal(signal.SIGWINCH, signal.SIG_IGN)?
        - Is this really a problem for output functions?  The odds of SIGWINCH
          arriving right when we're writing ought to be low.
    '''
    def write():
        stream.write(content)
    def flush():
        stream.flush()
    for function in (write, flush):
        while True:
            try:
                function()
            except InterruptedError:
                continue
            except IOError as e:
                if e.errno == errno.EINTR:
                    continue
                else:
                    raise
            break


def convert_param(paramtype, s):
    '''Convert user input (potentially incorrect text) to the proper type.
    
    Convert the string `s` to the proper type.
    Raises ValueError if `s` cannot be converted.
    
    `paramtype` MUST be one of the recognised values:
    
        'str':          `s` is returned.
        
        'yesno':        "Yes" is True and "no" is False.
        
        'dimension':    An integer >= 4
        
        'minecount':    Two modes (automatic selection):
                            An integer >= 1 returned as an integer.
                            Or a percentage `str(float)+'%'` in
                            ]0%, 100%[ returned as a float:
        
        'gametype':     Mapping with case-insensitive keys and
                        lower-case values:
                            'a', 'neumann' and '4' to 'neumann'
                            'b', 'hex', 'hexagonal' and '6' to 'hex'
                            'c', 'moore' and '8' to 'moore'
        
        'reverse-minecount':    `s` is an integer or a float and the
                                returned value is a string that can be
                                converted back to `s` with 'minecount'.
    '''
    if paramtype == 'str':
        return s
    elif paramtype == 'int':
        return int(s)
    elif paramtype == 'yesno':
        if s.lower() in ('y', 'yes'):
            return True
        elif s.lower() in ('n', 'no'):
            return False
        else:
            output(sys.stderr, '"Yes" or "no" please. (Without quotes.)\n')
            raise ValueError
    elif paramtype == 'dimension':
        try:
            value = int(s)
        except ValueError:
            # Easter egg.
            #
            # ~85.6% of English words contain 'a', 'c', 'm' or 'p'.
            # All numbers under one thousand belongs to the ~14.4%.
            #
            # 194 of the numbers between 0 and 200 contain one or more of
            # the letters 'n', 'f' and 'h'.
            #
            # But zero, two, six and twelve aren't included.
            # So check for X and startswith('TW').
            #
            # Some special words may appear too, so let's remove them.
            s = s.lower()
            for word in ['and', 'percent', 'point', 'comma', 'decimal']:
                s = s.replace(word, '')
            S = s.upper()
            if (
                'A' not in S and 'C' not in S and 'M' not in S and
                'P' not in S and ('N' in S or 'F' in S or 'H' in S
                or 'X' in S or S.startswith('TW'))
            ):
                output(sys.stderr, "Use digits.\n")
            else:
                output(sys.stderr,
                    'Invalid width or height; '
                    '"{}" is not an integer.\n'.format(s)
                )
            raise ValueError
        if value < 4:
            output(sys.stderr, 'Lowest allowed width or height is 4.\n')
            raise ValueError
        return value
    elif paramtype == 'minecount':
        assert len(s) >= 1, 'No (empty) amount of mines specified.'
        if s[-1] == '%':
            try:
                value = float(s[:-1])/100
            except ValueError:
                output(sys.stderr,
                    "You can't have {0} percent of the cells to be mines; "
                    "{0} is not a number.\n".format(s)
                )
                raise ValueError
            if value >= 1.0 or value <= 0.0:
                output(sys.stderr,
                    'Percentage of the cells that will be mines '
                    'must be in ]0%, 100%[.\n'
                )
                raise ValueError
        else:
            try:
                value = int(s)
            except ValueError:
                output(sys.stderr,
                    "You can't have {0} mines; "
                    "{0} is not an integer\n".format(s)
                )
                raise ValueError
            if value <= 0:
                output(sys.stderr, 'You must have at least ONE mine.\n')
                raise ValueError
        return value
    elif paramtype == 'gametype':
        if s.upper() in ('A', 'NEUMANN', '4'):
            return 'neumann'
        elif s.upper() in ('B', 'HEX', 'HEXAGONAL', '6'):
            return 'hex'
        elif s.upper() in ('C', 'MOORE', '8'):
            return 'moore'
        else:
            output(sys.stderr,'Invalid gametype.\n')
            raise ValueError
    elif paramtype == 'reverse-minecount':
        if isinstance(s, float):
            return '{}%'.format(100 * s)
        else:
            return str(s)
    else:
        assert False, "Invalid paramtype"


def ask(question, paramtype, default):
    '''Ask the user a question in line mode. (Not curses mode.)
    
    Ask the user a question (line mode; not curses mode) and return the
    answer after it has been converted.
    
    It will invoke `convert_param` to convert the string into the
    proper type and check that the user didn't say something stupid.
    
    `default` is what will be sent to `convert_param` if the user
    hits enter.
    
    `paramtype` will be sent to `convert_param`.  See the doc-string
    for `convert_param` to know what values of `paramtype` are permitted.
    
    NOTICE: This function will cause the program to exit if a
    KeyboardInterrupt is raised.
    '''
    while True:
        output(sys.stdout, '{} [{}]: '.format(question, default))
        try:
            # Due to a bug, syscalls may fail with EINTR after leaving
            # curses mode. See the docstring of `output` for info about
            # the bug.
            while True:
                try:
                    answer = sys.stdin.readline()
                except InterruptedError:
                    continue
                except IOError as e:
                    if e.errno == errno.EINTR:
                        continue
                    else:
                        raise
                break
            if not answer:
                # Handle EOF the same way as SIGINT later in this function.
                raise KeyboardInterrupt
            answer = answer.strip()
            if not answer:
                answer = default
            value = convert_param(paramtype, answer)
        except ValueError:
            continue
        except KeyboardInterrupt:
            output(sys.stdout,'\n')
            sys.exit(0)
        return value


def highscores_add_entry(title, prompt):
    '''
    Input callback for `game_engine.hiscores.add_entry`.
    '''
    output(sys.stdout, title + '\n')
    while True:
        try:
            return ask(prompt, 'str', '')
        except UnicodeDecodeError:
            output(sys.stderr, 'Decoding error.\n')


def highscores_display(title, headers, rows, cfgfile):
    '''
    Output formatter function for `game_engine.hiscores.display`.
    '''
    # Settings:
    cfg = game_engine.load_cfg(cfgfile, 'cursescfg', ['curses'])
    hs_conf = cfg['highscores']
    # Create all rows to be displayed.
    header_underline = ['='*len(col) for col in headers]
    header_blankline = ['' for col in headers]
    all_rows = [headers] + [header_underline] + [header_blankline] + rows
    # Calculate column widths.
    column_width = []
    for column in zip(*all_rows):
        #column_width.append(max(list(map(len, column))) + 1)
        column_width.append(max(list(map(len, column))))
    # Print
    # BUG: This message is only valid for less(1)
    text = 'Arrow keys to scroll, "q" when done viewing highscores.\n'
    text += '\n' + '_'*len(title) + '\n'
    text += title + '\n\n'
    for row in all_rows:
        for index, width in enumerate(column_width):
            spacing = -(width) % hs_conf['tabsize']
            if spacing < hs_conf['min_tabspace']:
                spacing += hs_conf['tabsize']
            text += row[index]
            text += ' ' * (width - len(row[index]))
            text += ' ' * spacing
        text += '\n'
    # Encode
    utext = text        # Needed for Python 3 if |less fails.
    text = text.encode(sys.stdout.encoding, errors='xmlcharrefreplace')

    # --- Pipe to the pager ---
    # Set the LESSSECURE environment variable to restrict less(1)
    os.environ['LESSSECURE'] = '1'
    pager_commands = [
        ['less', '-S', '-#', str(hs_conf['less_step'])],    # Nudelman's less
        'less',                                             # SerenityOS, Redox
        'windows-beta\\pager.bat',                          # Notepad
        'more',                                             # fallback
    ]
    # TODO: Should I respect the PAGER environment variable if set?

    for command in pager_commands:
        # Clean up after failed commands
        clear_screen()
        # Skip Windows specific commands
        if 'windows' in command and not sys_platform.startswith('win'):
            continue
        try:
            pager = subprocess.Popen(command, stdin=subprocess.PIPE)
            pager.communicate(text)
            if pager.wait() == 0:
                break
        except Exception:
            pass
    else:
        # TODO: Is this ever needed?
        if sys.version_info[0] == 3:
            output(sys.stdout, utext)
        else:
            output(sys.stdout, text)


def play_game(parameters):
    '''Play a custom game of minesweeper.
    
    When called with all required parameters,
    one game of minesweeper will be played.
    
    NOTICE: This function does not expect incorrect parameters!
    
    WARNING: If anything, except a KeyboardInterrupt, happens during an
        actual game, this function will raise an exception without
        leaving curses mode.
    
    `parameters` is a dictionary which MUST contain all these keys:
        'width'         Integer >= 4
        'height'        Integer >= 4
        'mines'         Integer >= 1 or float in ]0.0, 1.0[
        'gametype'      'moore', 'hex' or 'neumann'
        'guessless'     A boolean (no guessing required)
        'insult'        A boolean (!polite mode)
        'enginecfg'     The path to the configuration file for the
                        game engine.
        'cursescfg'     The path to the configuration file for key
                        bindings and textics customisation.

    '''
    if isinstance(parameters['mines'], float):
        area = parameters['width'] * parameters['height']
        mines = int(parameters['mines'] * area + 0.5)
        parameters['mines'] = mines
    # WORKAROUND for a special bug.
    if parameters['mines'] == 0:      # Test: 42
        parameters['mines'] == 1      # Test: 0
    # Don't blame the player when it's not the players fault.
    if not parameters['guessless']:
        parameters['insult'] = False

    try:
        engine = game_engine.game_engine(parameters['enginecfg'], **parameters)
    except game_engine.security_alert as e:
        output(sys.stderr, str(e) + '\n')
        return
    interface = curses_game(
        parameters['cursescfg'],
        parameters['gametype']
    )

    # Do this, or the terminal gets fucked up after the first game.
    # Still needed as of 2024-01-07
    if sys_platform.startswith('netbsd'):
        curses.reset_shell_mode()
        curses.reset_prog_mode()
        interface._init_mouse()     # I thought mouse didn't work on NetBSD
        interface.output(engine)
        interface.window.redrawwin()
    
    leave = False
    try:
        win, highscores = engine.play_game(interface)
    except KeyboardInterrupt:
        global allow_curses_getch_error
        allow_curses_getch_error = True
        leave = True
    except game_engine.security_alert as e:
        interface.leave()
        output(sys.stderr, str(e) + '\n')
        return
    interface.leave()

    if not sys_platform.startswith('win'):
        os.system('clear')
    if leave:
        return
    
    if parameters['insult']:
        if win:
            output(sys.stdout,
                   '\n\n"Congratulations", you won the unlosable game.\n')
        else:
            output(sys.stdout,'\n\nYou moron, you lost the unlosable game!\n')
    ask('Press enter to continue...', 'str', '')
    highscores.add_entry(highscores_add_entry)
    title, headers, rows = highscores.display()
    highscores_display(title, headers, rows, parameters['cursescfg'])


def user_input(default, cursescfg_path):
    '''Retrieve game parameters from the user.
    
    `cursescfg_path` is the path to the configuration file that happens
    to contain the key bindings and their documentation, which might be
    displayed by this function.
    
    `default` is a dictionary that MUST contain these keys:
    'width', 'height', 'mines', 'gametype', 'guessless' and 'insult'.
    Their types are specified in the doc-string for `play_game`.
    
    `user_input` will return dictionary containing the same keys.
    '''
    parameters = {}
    booldefault = {True: 'Yes', False: 'No'}


    output(sys.stdout,
           'Game types:\n'
           '  A: Neumann (no diagonals)\n'
           '  B: Hexagonal\n'
           '  C: Moore (normal)\n'
    )
    parameters['gametype'] = ask(
        'Choose game type', 'gametype', default['gametype']
    )

    output(sys.stdout, '\nDifficulties:\n')
    difficulties = default_difficulties[parameters['gametype']]
    default_choice = 1
    for index, values in enumerate(difficulties):
        name = values[0]
        if name == 'Default':
            default_choice = index+1
        output(sys.stdout, '  {}: {}\n'.format(index+1, name))
    output(sys.stdout, '  {}: Custom\n'.format(len(difficulties) + 1))
    choice = ask('Choose difficulty', 'int', default_choice)

    if choice > len(difficulties) or choice < 1:
        custom = True
    else:
        custom = False
        _, mines, width, height = difficulties[choice - 1]
        parameters['width'] = width
        parameters['height'] = height
        parameters['mines'] = mines
        parameters['guessless'] = True

    if custom:
        parameters['width'] = ask(
            'Width of the playing field',
            'dimension',
            default['width']
        )
        parameters['height'] = ask(
            'Height of the playing field',
            'dimension',
            default['height']
        )
        # MUST ask for dimensions before for the # of mines.
        parameters['mines'] = ask(
            'Mines: number or percent%',
            'minecount',
            convert_param('reverse-minecount', default['mines'])
        )
        parameters['guessless'] = ask(
            '100% solvable field (no guessing required)',
            'yesno',
            booldefault[default['guessless']]
        )

    # MUST ask for guessless mode before polite mode.
    if parameters['guessless']:
        parameters['insult'] = not ask(
            'Polite mode?',
            'yesno',
            booldefault[not default['guessless']]
        )
    # Ask if the user wants to know the key bindings.
    if ask('Show key bindings?', 'yesno', 'No'):
        cursescfg = game_engine.load_cfg(cursescfg_path, 'cursescfg',
                                         ['curses'])
        output(sys.stdout,cursescfg['pre-doc'])
        if parameters['gametype'] == 'hex':
            output(sys.stdout, cursescfg['doc-hex'])
        else:
            output(sys.stdout, cursescfg['doc-square'])
        output(sys.stdout,
            "\nPressing an unrecognised key will refresh the screen.\n\n"
        )
        ask('Press enter to continue...', 'str', '')
    return parameters


def arg_input(default):
    '''Get configuration filepaths and game parameters from `sys.argv`.
    
    user_input_required, params = arg_input(default)
    
    This function will retrieve the game parameters, and paths to the
    configuration files, from `sys.argv`.  If `sys.argv` contains no
    game parameters, `user_input` will be True.
    
    `default` is a dictionary that MUST contain these keys:
    'width', 'height', 'mines', 'gametype', 'guessless' and 'insult'.
    Their types are specified in the doc-string for `play_game`.
    
    `params` is a dictionary that contains either all of the keys that
    are required for `default`, or none of them.  It may also contain
    'enginecfg' and/or 'cursescfg' for specifying configuration files
    that are not in the ordinary search path.
    
    The program will exit if bogus parameters are given.
    
    NOTICE: If argparse couldn't be imported, this function will exit
    the program if there are any command line arguments.  If there
    aren't, it will return `True, {}`.
    '''
    default_s = {
        True: '(Default)',
        False: '(Not default)'
    }

    # Add blank lines in --help output:
    # https://stackoverflow.com/a/29485128/6950051
    class Formatter(argparse.RawTextHelpFormatter):
        def _split_lines(self, text, width):
            return super(Formatter, self)._split_lines(text, width) + ['']

    # Get the arguments sent on the command line.
    parser = argparse.ArgumentParser(
        formatter_class=Formatter,
        description='''
Anonymine is a curses mode minesweeper that checks if the field can be solved
without guessing and supports three different game types:
    Traditional (Moore grids): 8 neighbors {}
    Hexagonal: 6 neighbors {}
    Von Neumann grids: 4 neighbors {}
{}
        '''.format(
          default_s[default['gametype'] == 'moore'],
          default_s[default['gametype'] == 'hex'],
          default_s[default['gametype'] == 'neumann'],
          {
            True:   'By default, it will insult you for both winning and\n'
                    'losing a game that has been proven to be 100% winnable.',
            False:  'By default, it will not insult you for either winning '
                    'or losing.',
          }[default['insult']],
        )
    )
    # Configuration files.
    # TODO: Feels bad hardcoding the paths like this.
    parser.add_argument(
        '-c', '--cursescfg', dest='cursescfg',
        help=(
            'The path to the configuration file for the key bindings\n'
            'and textics directives.\n'
            'Default is the first of:\n'      # Dodgy English
                '  "~/.anonymine/cursescfg",\n'
                '  "MAKEFILE_CFGDIR/cursescfg",\n'
                '  "/etc/anonymine/cursescfg",\n'
                '  "windows-beta/cursescfg" (Only on Windows).\n'
            'WARNING: This file allows code execution'
        )
    )
    parser.add_argument(
        '-e', '--enginecfg', dest='enginecfg',
        help=(
            'The path to the configuration file for field\n'
            'initialization and misc. game engine functions.\n'
            'Default is the first of:\n'      # Dodgy English
                '  "~/.anonymine/enginecfg",\n'
                '  "MAKEFILE_CFGDIR/enginecfg",\n'
                '  "/etc/anonymine/enginecfg",\n'
                '  "windows-beta/enginecfg" (Only on Windows).\n'
            'WARNING: This file allows code execution'
        )
    )
    # Dimensions and minecount.
    parser.add_argument(
        '-s', '--size', dest='size',
        help=(
            "The size of the field width + 'x' + height.  Ex. 30x16\n"
            "Default is {}x{}.".format(
                default['width'], default['height']
            )
        )
    )
    parser.add_argument(
        '-m', '--mines', dest='mines',
        help=(
            "The number of mines OR the percentage. Default is {}.\n"
            "Creating solvable fields becomes exponentially slower\n"
            "somewhere around 20 to 25 percent.".format(
                convert_param(
                    'reverse-minecount',
                    default['mines']
                ).replace('%', '%%')
            )
        )
    )
    # --version
    parser.add_argument(
        '-V', '--version', action='store_true', dest='version',
        help='Show version and exit'
    )
    # Gametype
    gametype = parser.add_mutually_exclusive_group()
    gametype.add_argument(
        '-4', '--neumann',
        action='store_const', dest='gametype', const='neumann',
        help=(
            "Use von Neumann grids.\n" +
            "(Each cell has 4 neighbors, corners don't count)\n" +
            default_s[
                default['gametype'] == 'neumann'
            ]
        )
    )
    gametype.add_argument(
        '-6', '--hex', '--hexagonal',
        action='store_const', dest='gametype', const='hex',
        help=(
            "Use a hexagonal field. (Each cell has 6 neighbors)\n" +
            default_s[
                default['gametype'] == 'hex'
            ]
        )
    )
    gametype.add_argument(
        '-8', '--moore', '--traditional',
        action='store_const', dest='gametype', const='moore',
        help=(
            "Traditional minesweeper; Moore grids.\n" +
            "(Each cell has 8 neighbors) " +
            default_s[
                default['gametype'] == 'moore'
            ]
        )
    )

    # Bools.
    guessless = parser.add_mutually_exclusive_group()
    guessless.add_argument(
        '-g', '--guessless', dest='guessless', action='store_true',
        help=(
            "Play a minesweeper that can be solved without guessing.\n" +
            default_s[
                default['guessless']
            ]
        )
    )
    guessless.add_argument(
        '-G', '--no-guessless', dest='noguessless', action='store_true',
        help=(
            "Play with the risk of having to guess.\n" +
            "Large fields will be initialized much faster.\n" + default_s[
                not default['guessless']
            ]
        )
    )
    insult = parser.add_mutually_exclusive_group()
    insult.add_argument(
        '-r', '--rude', dest='insult', action='store_true',
        help=default_s[default['insult']]
    )
    insult.add_argument(
        '-n', '--nice', dest='noinsult', action='store_true',
        help=(
            "(more polite setting) " + default_s[
                not default['insult']
            ]
        )
    )
    # Sanitize args if over SSH connection
    if os.getenv('SSH_CLIENT'):
        msg = (
            "WARNING: Cleared command line arguments.\n"
            "`anonymine -c <arg>` is not allowed over SSH connections.\n"
            "See https://gitlab.com/oskog97/anonymine/-/issues/33\n"
            "\n"
        )
        if len(sys.argv) == 3:
            if sys.argv[1] == '-c':
                output(sys.stderr, msg)
                sys.argv = ['anonymine']
    #
    # Parse the args and store the params.
    args = parser.parse_args()
    params = {}
    user_input_required = True
    error = False
    # Test for --version first.
    if args.version:
        output(sys.stdout, ANONYMINE_VERSION)
        sys.exit(0)
    # Size, mines and gametype.
    if args.size:
        user_input_required = False
        try:
            params['width'], params['height'] = map(
                lambda x: convert_param('dimension', x),
                args.size.split('x')
            )
        except ValueError:
            error = True
            output(sys.stderr,
                'Error with "--size": Explanation above.\n'
            )
        except:
            error = True
            output(sys.stderr,'Error with "--size": UNKNOWN\n')
            raise
    if args.mines:
        user_input_required = False
        try:
            params['mines'] = convert_param('minecount', args.mines)
        except ValueError:
            error = True
            output(sys.stderr,
                'Error with "--mines": Explanation above.\n'
            )
        except:
            error = True
            output(sys.stderr,'Error with "--mines": UNKNOWN\n')
            raise
    if args.gametype:
        user_input_required = False
        assert args.gametype in ('moore', 'hex', 'neumann')
        params['gametype'] = args.gametype
    # guessless, insult
    if args.guessless:
        user_input_required = False
        params['guessless'] = True
    if args.noguessless:
        user_input_required = False
        params['guessless'] = False
    if args.insult:
        user_input_required = False
        params['insult'] = True
    if args.noinsult:
        user_input_required = False
        params['insult'] = False
    # Configuration
    if args.cursescfg:
        params['cursescfg'] = args.cursescfg
    if args.enginecfg:
        params['enginecfg'] = args.enginecfg
    # Deal with error and user_input_required.
    if error:
        sys.exit(1)
    if not user_input_required:
        for key in default:
            if key not in params:
                params[key] = default[key]
    return user_input_required, params


def main_inner():
    '''Invoke `arg_input` to find configuration files with non-standard
    paths.
    
    Find the remaining configuration files.
    NOTICE: It does not check if theconfiguration file is valid,
    only if it exists.
    
    Invoke `user_input` if `arg_input` didn't return game parameters.
    
    Invoke `play_game`.
    
    Invoke `ask` in a while loop to ask the player if [s]he wants to
    continue play again. And while True, invoke `play_game`.
    
    
    WARNING: MAY raise an exception while in curses mode.
    '''

    # python -O will disable asserts
    def forced_assert(test):
        if not test:
            raise game_engine.security_alert("Assertion failure")

    default = {
        'width': 20,
        'height': 20,
        'mines': .2,
        'gametype': 'moore',
        'guessless': True,
        'insult': True,
    }
    
    interactive, parameters = arg_input(default)
    
    # Handle the configuration filepaths.
    cfgfiles = {
        'enginecfg': None,
        'cursescfg': None,
    }
    error = False
    for cfgfile in cfgfiles:
        if cfgfile in parameters:
            cfgfiles[cfgfile] = parameters[cfgfile]
        else:
            # NOTE: This list is duplicated twice in arg_input
            locations = [
                os.path.expanduser('~/.anonymine/' + cfgfile),
                'MAKEFILE_CFGDIR/' + cfgfile,
                '/etc/anonymine/' + cfgfile,
            ]
            if sys_platform.startswith('win'):
                locations.append('windows-beta/' + cfgfile)
            for location in locations:
                # Check if file exists
                try:
                    stat = os.stat(location)
                except OSError:
                    continue
                # Check if file is usable
                try:
                    game_engine.load_cfg(location, cfgfile, ['curses'])
                    cfgfiles[cfgfile] = location
                    break
                except game_engine.cfg_error as err:
                    # forced_assert failed
                    output(
                        sys.stderr,
                        '{}\nWill use a different file instead.\n\n'.format(err)
                    )
            else:
                output(sys.stderr, '{} not found.\n'.format(cfgfile))
                error = True
    if error:
        # Don't use `exit` here, use `sys.exit`. Otherwise `ask` will hang.
        sys.exit(1)
    
    if interactive:
        output(sys.stdout, "\x1b]2;Anonymine\x07")
        output(sys.stdout, '\r' + ANONYMINE_VERSION + '\n')
        output(sys.stdout,
            'You can quit the game with the interrupt signal. (Ctrl + c)\n\n'
        )
        output(sys.stdout,
            'How do you want your game to be set up? Write in the values'
            ' and press Enter.\nLeave blank to use the default.\n\n'
        )
        parameters = user_input(default, cfgfiles['cursescfg'])
    
    parameters.update(cfgfiles)
    play_game(parameters)
    
    while interactive and ask('Play again?', 'yesno', 'Yes'):
        parameters = user_input(default, cfgfiles['cursescfg'])
        parameters.update(cfgfiles)
        play_game(parameters)


def main():
    '''Invoke `main_inner` and handle the odd exceptions it may raise.'''
    
    # I should probably use curses.wrapper instead, but this mess is
    # here for some sort of reason.
    if 'SIGTSTP' in dir(signal):
        signal.signal(signal.SIGTSTP, signal.SIG_IGN)

    # Anonymine needs TERM=linux on the terminal on macOS and SerenityOS
    # for numpad to work.
    # FIXME: Find a more robust way of checking for the terminals.
    buggy_term = [
        ('serenityos', 'xterm'),
        ('darwin', 'xterm-256color'),
    ]
    if (sys_platform, os.getenv('TERM')) in buggy_term:
        if os.getenv('SSH_CLIENT') is None:
            os.environ['TERM'] = 'linux'

    def reset():
        try:
            curses.endwin()
        except:
            pass
        try:
            clear_screen()
        except:
            pass
        # Jython needs something to be printed to stdout before
        # printing to stderr works
        if platform.python_implementation() == 'Jython':
            output(sys.stdout, '\n')

    try:
        main_inner()
    except KeyboardInterrupt:
        # Haven't seen this happen as it usually gets caught much earlier.
        # Reset the terminal just in case.
        reset()
    except SystemExit as e:
        if isinstance(e.code, int):
            return e.code
        else:
            reset()
            output(sys.stderr, e.code + '\n')
            return 1
    except game_engine.cfg_error as e:
        reset()
        # The error message already explains itself well enough
        output(sys.stderr, str(e) + '\n')
        return 1
    except:
        # Get the traceback without fucking up the terminal.
        exception = sys.exc_info()
        reset()
        output(sys.stdout, "Bug found!!!\n")
        traceback.print_exception(*exception)
        return 1
    return 0


if __name__ == '__main__':
    status = main()
    if status:
        ask("Press enter to quit", 'str', '')
    exit(status)
