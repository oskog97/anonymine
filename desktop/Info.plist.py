#!/usr/bin/python
# Build Info.plist for Mac OS X.
# ${srcdir}/desktop/Info.plist.py outfile version

import sys

outfile = sys.argv[1]
version_in = sys.argv[2]

# Apple wants the major number to be greater than zero.
try:
    assert False # But it works without this conversion too.
    if version_in.startswith('pre-') or version_in.startswith('test-'):
        alpha = True
        version_in = version_in.split('-')[1]
    else:
        alpha = False
    major, minor, micro = tuple(map(int, version_in.split('.')))
    apple_version = '{}.{}{}'.format(
        100*major + minor,
        micro,
        'd'*alpha
    )
except:
    apple_version=version_in
    
f = open(outfile, 'w')
f.write('''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>CFBundleName</key>                 <string>Anonymine</string>
    <key>CFBundleIdentifier</key>           <string>com.oskog97.anonymine</string>
    <key>CFBundleDisplayName</key>
    <string>Anonymine - Minesweeper without guessing</string>
    <key>CFBundleVersion</key>              <string>{0}</string>
    <key>CFBundleShortVersionString</key>   <string>{0}</string>

    <!--
    https://cocoadev.github.io/CFBundleSignature/
    <key>CFBundleSignature</key>        <string>boom</string>
    -->

    <key>CFBundlePackageType</key>      <string>APPL</string>
    <key>CFBundleExecutable</key>       <string>macosx1</string>
    <key>CFBundleIconFile</key>         <string>icon.icns</string>
    <key>LSApplicationCategoryType</key><string>public.app-category.puzzle-games</string>
    <!-- -->
    <key>NSHumanReadableCopyright</key>
    <string>
        Copyright (c) Oskar Skog
        This software is released under the FreeBSD license (2-clause BSD).
    </string>
</dict>
</plist>
'''.format(apple_version))
f.close()
