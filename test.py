#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
`runmoore`, `runhex` and `runneumann` are demos to be played in a
80x24 terminal.  The `_output` function sets the output frequency.
    runmoore(x=78, y=18, m=225)
    runhex(x=39, y=18, m=112)
    runneumann(x=78, y=18, m=225)
'''

import hashlib
import multiprocessing
import os
import platform
import random
import signal
import sys
import time
import traceback


try:
    from getpass import getuser
except:
    def getuser():
        raise NotImplementedError

try:
    WindowsError
except NameError:
    class WindowsError(Exception):
        pass


import anonymine_fields
import anonymine_solver
import anonymine_engine

# anonymine_engine has already unshadowed the system name on Jython.
os_name = anonymine_engine.os_name
sys_platform = anonymine_engine.sys_platform


class Timeout(Exception):
    pass


class TestResult():
    def __init__(self, reason=None):
        self.reason = reason
    def __eq__(self, other):
        return isinstance(self, other)

class TestOK(TestResult):
    def __bool__(self):
        return True
    def __str__(self):
        return 'OK'

class TestUntestable(TestResult):
    def __bool__(self):
        return True
    def __str__(self):
        return 'Untestable: {}'.format(self.reason)

class TestFailed(TestResult):
    def __bool__(self):
        return False
    def __str__(self):
        return 'Failed: {}'.format(self.reason)


def _output(field, argument):
    print(field)
    time.sleep(.04)     # 25 Hz


def _temp_dir():
    # IronPython 2.7.12 on Mono on Linux has broken os.stat and no os.getuid
    impl = platform.python_implementation()
    if impl=='IronPython' and sys.version_info[0]==2 and os_name=='posix':
        temp_dir = '/tmp/anonymine-test-ironpython'
        sys.stderr.write('Warning: Assuming {} is a good temp dir\n'
                         .format(temp_dir))

    elif os_name == 'posix':
        # Basically does `temp_dir = "/tmp/anonymine-test-{username}"`
        try:
            username = getuser()
        except:
            username = str(os.getuid())
        assert '..' not in username
        assert '\0' not in username
        assert '/' not in username
        assert '~' not in username          # Shouldn't be necessary here?
        temp_dir = os.path.join('/tmp/', 'anonymine-test-' + username)
        stat = os.stat('/tmp')
        if not sys_platform in ('haiku1', 'cygwin'):
            assert stat.st_uid == 0
            assert stat.st_mode == 0o41777      # drwxrwxrwt

    elif os_name == 'nt':
        temp_base = os.getenv('TEMP')
        # Check that %TEMP% is somewhere in the home directory
        # Can't use `temp_base.startswith(os.path.expanduser('~'))`
        # due to 8.3 filenames.
        if 'samefile' in dir(os.path):
            temp_env_ok = False
            home_dir = os.path.expanduser('~')
            parent, basename = os.path.split(temp_base)
            while basename:
                if os.path.samefile(parent, home_dir):
                    temp_env_ok = True
                    break
                parent, basename = os.path.split(parent)
            if not temp_env_ok:
                raise RuntimeError("%TEMP% outside home directory")
        else:
            sys.stderr.write('Warning: Cannot check if %TEMP% is sane\n')
        #
        temp_dir = os.path.join(temp_base, 'anonymine-test')

    else:
        raise RuntimeError("Unknown operating system")

    return temp_dir


def create_cfg(overrides):
    '''
    Create testcfg temp file.  Not all fields need to be populated.
    Returns the path to the configuation file.  Make sure to remove it
    later.
    '''

    default = {
        'init-field': {
            'sec-maxtime':          600,
            'sec-maxarea':          40000,
            'options': {
                'nice':             3,
                'stuck-check':      2,
                'rule9bf-max':      13,
                'rule9bf-action':   'deny',
            },
        }
    }

    temp_dir = _temp_dir()

    try:
        os.mkdir(temp_dir, 0o755)
    except OSError:
        pass

    filename = os.path.join(temp_dir, 'enginecfg-{}'.format(os.getpid()))

    def merge_dict(a, b):
        dest = {}
        for key in a:
            if key in b and isinstance(a[key], dict):
                dest[key] = merge_dict(a[key], b[key])
            elif key in b:
                dest[key] = b[key]
            else:
                dest[key] = a[key]
        for key in b:
            if key not in a:
                dest[key] = b[key]
        return dest

    cfg = merge_dict(default, overrides)
    old_umask = os.umask(0o022)
    f = open(filename, 'w')
    f.write(repr(cfg))
    f.close()
    os.umask(old_umask)

    return filename


def runmoore(x=78, y=18, m=225):
    field = anonymine_fields.generic_field([x, y])
    field.set_callback('input', _output, None)
    print(field)
    mines = field.all_cells()
    random.shuffle(mines)
    field.fill(mines[:m])

    for mine in mines[m:]:
        for neighbour in field.get_neighbours(mine):
            if neighbour in mines[:m]:
                break
        else:
            field.reveal(mine)
            break

    solver = anonymine_solver.solver()
    solver.field = field

    print(solver.solve())


def runneumann(x=78, y=18, m=225):
    field = anonymine_fields.generic_field([x, y], False)
    field.set_callback('input', _output, None)

    mines = field.all_cells()
    random.shuffle(mines)
    field.fill(mines[:m])

    for mine in mines[m:]:
        for neighbour in field.get_neighbours(mine):
            if neighbour in mines[:m]:
                break
        else:
            field.reveal(mine)
            break

    solver = anonymine_solver.solver()
    solver.field = field
    print(solver.solve())


def runhex(x=39, y=18, m=112):
    field = anonymine_fields.hexagonal_field(x, y)
    field.set_callback('input', _output, None)

    mines = field.all_cells()
    random.shuffle(mines)
    field.fill(mines[:m])

    for mine in mines[m:]:
        for neighbour in field.get_neighbours(mine):
            if neighbour in mines[:m]:
                break
        else:
            field.reveal(mine)
            break

    solver = anonymine_solver.solver()
    solver.field = field
    print(solver.solve())


def test_new_initializer():
    params = {
        'width': 10,        # 20 to test SerenityOS bug
        'height': 10,       # 20
        'mines': 40,        # 80
        'gametype': 'moore',
        'guessless': True,
    }
    start_point = (0, 0)

    cfgfile = create_cfg({
        'init-field': {
            'options': {
                'debug':        True,
                'force-procs':  1,
            },
        },
    })
    pid = os.getpid()

    times = []
    timeouts = 0
    try:
        while True:
            assert os.getpid () == pid
            engine = anonymine_engine.game_engine(cfgfile, **params)
            # Can't use os.times().children_user and os.times().children_sys
            # because the child may be dead when the stop time is measured
            try:
                t_start = time.time()
                engine.init_field(start_point)
                t_stop = time.time()
                times.append(t_stop - t_start)
                print(len(times), times[-1])
            except anonymine_engine.security_alert:
                timeouts += 1
    except KeyboardInterrupt:
        pass
    except:
        exception = sys.exc_info()
        msg = traceback.format_exception(*exception)
        if os.getpid() == pid:
            sys.stdout.write('Exception in parent:\n{}\n'.format(msg))
        else:
            sys.stdout.write('Exception in child:\n{}\n'.format(msg))

    os.remove(cfgfile)

    if os.getpid() != pid:
        sys.stdout.write('Kill child\n')
        sys.stdout.flush()
        os._exit(0)

    n = len(times)
    mean = sum(times) / n
    stddev = (sum([(x-mean)**2 for x in times]) / n)**.5
    times.sort()

    print('')
    print('Raw:')
    print(repr(times))
    print('')

    print('Samples: {}'.format(len(times)))
    print('Timeouts: {}'.format(timeouts))
    print('Min: {}'.format(times[0]))
    print('Median: {}'.format(times[n//2]))
    print('Mean: {}'.format(mean))
    print('Max: {}'.format(times[-1]))
    print('Standard deviation: {}'.format(stddev))


def _progressbar(progress, start_time):
    time_taken = time.time() - start_time
    if progress > 0:
        eta = start_time + time_taken/progress
        eta = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(eta))
    else:
        eta = ''
    sys.stderr.write('\r{:.2f}%\tETA {}'.format(100. * progress, eta))
    sys.stderr.flush()


def _clear_progressbar():
    sys.stderr.write('\r\x1b[K')
    sys.stderr.flush()


def performance(size=20, samples=10000, nice=3, procs=1):
    params = {
        'width': size,
        'height': size,
        'mines': int(0.2 * size**2),
        'gametype': 'moore',
        'guessless': True,
    }
    start_point = (size//2, size//2)
    cfgfile = create_cfg({
        'init-field': {
            'options': {
                'force-procs':  procs,
                'nice':         nice,
                'debug':        False,
                'stuck-check':  2,
            }
        },
    })

    pid = os.getpid()
    total_start_t = time.time()
    times = []
    timeouts = 0
    for i in range(samples):
        _progressbar(float(i)/samples, total_start_t)
        engine = anonymine_engine.game_engine(cfgfile, **params)
        # Can't use os.times().children_user and os.times().children_sys
        # because the child may be dead when the stop time is measured
        try:
            t_start = time.time()
            engine.init_field(start_point)
            t_stop = time.time()
            times.append(t_stop - t_start)
        except anonymine_engine.security_alert:
            timeouts += 1

    _clear_progressbar()
    os.remove(cfgfile)

    n = len(times)
    mean = sum(times) / n
    stddev = (sum([(x-mean)**2 for x in times]) / n)**.5
    times.sort()

    print('Samples:   {}'.format(n))
    print('Timeouts:  {}'.format(timeouts))
    print('Mean:    {:7.3f} s'.format(mean))
    print('Std dev: {:7.3f} s'.format(stddev))
    print('10%:     {:7.3f} s'.format(times[int(n*.1)]))
    print('Median:  {:7.3f} s'.format(times[n//2]))
    print('90%:     {:7.3f} s'.format(times[int(n*.9)]))
    if samples >= 1000:
        print('99%:     {:7.3f} s'.format(times[int(n*.99)]))


def parallel_seed(automate=None, verbose=False):
    '''
    Verify that random.Random() always has a unique seed, even when many
    instances are created at the same time.
    '''
    from pickle import PicklingError

    def child(queue):
        rng = random.Random()
        queue.put(rng.random())

    fork_mode = False
    try:
        multiprocessing.set_start_method('fork')
        fork_mode = True
    except AttributeError:
        # set_start_method was introduced in 3.4
        assert sys.version_info[:2] < (3, 4)
    except RuntimeError as err:
        # Because anonymine_engine already has done this
        assert str(err) == "context has already been set"
    except ValueError:
        if verbose:
            print("Couldn't set start method to 'fork'")

    # 250 procs eats all memory on SerenityOS.
    # Why hasn't this been observed on Cygwin?
    # 250 procs causes Python to open too many files on macOS
    if sys_platform in ('darwin', 'serenityos'):
        n_proc = 10
    else:
        n_proc = 100

    procs = []
    queue = multiprocessing.Queue()
    for i in range(n_proc):
        procs.append(multiprocessing.Process(target=child, args=(queue,)))

    for i, proc in enumerate(procs):
        try:
            proc.start()
        except NotImplementedError:
            # Pööp
            assert i == 0
            return TestUntestable(
                'multiprocessing.Process.start() raised NotImplementedError'
            )
        except (PicklingError, AttributeError) as err:
            # Windows
            assert fork_mode == False
            return TestUntestable("Failed to pickle code? " + repr(err))

    results = set()
    for proc in procs:
        results.add(queue.get())

    if len(results) < len(procs):
        return TestFailed('Same seed used for multiple processes')
    else:
        return TestOK()


def _game_hash(cfg_override, params, start_point):
    cfgfile = create_cfg(cfg_override)

    engine = anonymine_engine.game_engine(cfgfile, **params)
    engine.init_field(start_point)

    # Get string representing the whole minefield
    solver = anonymine_solver.solver()  # the WHOLE field, not just the start
    solver.field = engine.field
    solver.solve()
    field_string = str(solver.field)

    try:
        os.remove(cfgfile)
    except WindowsError as err:
        sys.stderr.write('BUG: WindowsError caught in _game_hash: ')
        sys.stderr.write(repr(err) + '\n')

    assert 'X' not in field_string

    if sys.version_info[0] > 2:
        field_string = field_string.encode('ascii')
    return hashlib.md5(field_string).hexdigest()


def timeout(automate=False, verbose=True):
    whoami = '{} {}.{}'.format(
        platform.python_implementation(),
        sys.version_info[0], sys.version_info[1]
    )

    # Interpreters that die if they receive SIGINT:
    known_untestable = [
        'Jython 2.7',                       # signal.alarm works
        'GraalVM 3.8',
    ]
    # GraalVM 3.11 doesn't die on Debian 8, but still fails
    if whoami in known_untestable:
        return TestUntestable('Interpreter would die if timeout is tested')

    while platform.system() == 'Windows' and automate:
        # Jython return TestUntestable earlier, because it always dies
        # IronPython 3 is doesn't even detect the timeout
        # PyPy 2.7 works
        # Everything else dies on Windows with signal.SIGINT
        impl = platform.python_implementation()
        if impl == 'IronPython':
            if sys.version_info[0] == 3:
                break
        if impl == 'PyPy':
            if sys.version_info[0] == 2:
                break
        if impl not in ('CPython', 'PyPy', 'IronPython'):
            return TestUntestable('Interpreter MIGHT die')
        return TestUntestable('Interpreter and calling process WILL die')

    if anonymine_engine.use_SimpleQueue:
        sys.stderr.write('{}: Skip timeout due to SimpleQueue\n'.format(whoami))
        return TestOK()

    # max_time needs to be at least 1 s for Pööp multiprocessing-forking.py
    # to work.  epsilon need to be at least 1 s for PyPy 2.7 on Windows, and
    # more than 5 s for SerenityOS
    max_time = 1.1
    epsilon = 10

    cfg = {
        'init-field': {
            'sec-maxtime': max_time,
        }
    }
    params = {
        'width': 24,
        'height': 18,
        'mines': 128,
        'gametype': 'moore',
        'guessless': True,
    }
    start = time.time()
    try:
        _game_hash(cfg, params, (0, 0))
    except anonymine_engine.security_alert:
        if time.time() - start > max_time + epsilon:
            return TestFailed("Timeout detected too late")
        else:
            return TestOK()
    except Exception as err:
        return TestFailed('Raised ' + repr(err))

    if time.time() - start > max_time:
        return TestFailed("Timeout condition not detected at all")
    else:
        return TestUntestable('Successfully initialized field too quickly')


def test_ultra_difficulty():
    '''
    Test that ultra difficulty isn't too difficult to generate.
    Target computer: Raspberry Pi 4
    Completion time: max 15 hours
    '''
    params_list = [
        {
            'gametype': 'moore',
            'width': 27,
            'height': 19,
            'mines': 205,
        },
        {
            'gametype': 'hex',
            'width': 25,
            'height': 24,
            'mines': 205,
        },
        {
            'gametype': 'neumann',
            'width': 38,
            'height': 21,
            'mines': 205,
        },
    ]
    cfg = {
        'init-field': {
            'sec-maxtime': 1800,        # Default for the game
            'nice': 3,                  # Default for the game
            'max-procs': 4,             # Raspberry Pi 4
        },
    }
    target = 10
    # Previous results included
    success_rates = {
        # 36, 36, 26 / 40:  205@27x15, 205@25x24, 205@37x21
        # 55, 55, 54 / 60:  205@27x15, 205@25x24, 205@38x21
        'target': target + 60,
        'moore': 55,
        'hex': 55,
        'neumann': 54,
    }
    for params in params_list:
        params['guessless'] = True
        gametype = params['gametype']
        print(gametype)

        for i in range(target):
            try:
                _game_hash(cfg, params, (0, 0))
                success_rates[gametype] += 1
                print('ok')
            except anonymine_engine.security_alert:
                print('timeout')
    return success_rates


def timeout_test():
    '''
    Continuosly test `timeout`.
    Does not return.
    '''
    import gc
    success = 0
    total = 0
    try:
        while True:
            success += timeout() == TestOK
            total += 1
            sys.stdout.write('*** {}/{} ***\n'.format(success, total))
            sys.stdout.flush()
            gc.collect()
    except OSError:
        # Too many open files, what are they?
        for fd in range(3, 20):
            try:
                print(repr(os.fstat(fd)))
            except:
                pass


def test_multiprocessing(automate=False, verbose=True):
    def worker(queue):
        queue.put("Working")
        exit(0)

    def caller():
        queue = multiprocessing.Queue()
        process = multiprocessing.Process(target=worker, args=(queue,))
        try:
            process.start()
        except Exception as e:
            # This happens on Windows anyway
            if verbose:
                sys.stderr.write(
                    'DEGRADED: multiprocessing.Process().start() failed: '
                    '{}\n'.format(e)
                )
            return
        assert queue.get() == "Working"

    def alarm_handler(*args):
        raise Timeout('multiprocessing.Queue() seems to hang')

    try:
        signal.signal(signal.SIGALRM, alarm_handler)
        signal.alarm(10)
    except Exception as e:
        if verbose:
            sys.stderr.write('Warning: Failed to set alarm: {}\n'.format(e))

    try:
        caller()
        try:
            signal.alarm(0)
        except Exception as e:
            if verbose:
                sys.stderr.write('Warning: Failed to unset alarm: {}\n'
                                 .format(e))
        return TestOK()
    except Exception as err:
        traceback.print_exception(*sys.exc_info())
        return TestFailed('Raised ' + repr(err))


def load_cursescfg(automate=False, verbose=True):
    if automate and platform.python_implementation() == 'RustPython':
        return TestUntestable('RustPython will panic')

    try:
        import curses
        del curses
    except:
        return TestUntestable('Failed to import curses')

    if platform.python_implementation() == 'IronPython':
        standard_paths = [
            'ipycfg/cursescfg',
            'cursescfg',
        ]
    else:
        standard_paths = (
            '/etc/anonymine/cursescfg',
            '/usr/local/etc/anonymine/cursescfg',
            'windows-beta\\cursescfg',
            '/boot/system/non-packaged/data/anonymine/cursescfg',
        )
    for path in standard_paths:
        try:
            os.stat(path)
            cfgfile = path
            break
        except:
            pass
    else:
        return TestUntestable("Can't find cursescfg")

    try:
        anonymine_engine.load_cfg(cfgfile, 'cursescfg', ['curses'])
    except Exception as err:
        exception = sys.exc_info()
        traceback.print_exception(*exception)
        return TestFailed(repr(err))

    return TestOK()


def test_test(automate=False, verbose=True):
    return TestFailed('TEST')


def hiscores_0_6_42(automate=False, verbose=True):
    '''
    Test that the new _read_nick_field and _write_nick_field
    work after the modifications in 0.6.42 and still preserve
    no-longer used extensions (seed).
    '''
    cfg = {
        'file': '/dev/null',
        'entries': None,
        'maxsize': None,
        'use-nick': None,
        'nick-maxlen': None,
    }
    hs_obj = anonymine_engine.hiscores(cfg, "invalid", 0.0, 0, False)

    reader = lambda s: hs_obj._read_nick_field(s)
    writer = lambda n, e: hs_obj._write_nick_field(n, e)

    cheater = {'cheater': None}

    # Seeds should still be tested to (in theory) support downgrading
    # Anonymine
    reader_patterns = [
        # Old format conversions
        ('[Pretending to be immortal] Silly', ('[IMMORTAL] Silly', {})),
        ('[IMMORTAL] A cheater', ('A cheater', cheater)),
        ('Old style nickname', ('Old style nickname', {})),
        # New format conversions
        ('[] New style', ('New style', {})),
        ('[cheater] ...', ('...', cheater)),
        ("[seed-66@{2'2},cheater] test",
            ('test', {'cheater': None, "seed-66@{2'2}": None})),
        # Based on old [Pretending...] highscore entry
        ('[] [IMMORTAL]  wannabe', ('[IMMORTAL]  wannabe', {})),
    ]
    writer_patterns = [
        (('Foo', cheater), '[cheater] Foo'),
        (('Bar', {}), '[] Bar'),
        (('Baz', {'seed-5-(5.5)': None}), '[seed-5-(5.5)] Baz'),
    ]
    # For write tests with multiple extensions.  The order of extensions
    # is not important.
    # And might not be deterministic depending on Python version?
    round_trip_patterns = [
        ('test', {'cheater': None, "seed-66@{2'2}": None}),
    ]

    for reader_input, expected_output in reader_patterns:
        output = reader(reader_input)
        if output != expected_output:
            return TestFailed("reader: " + reader_input)

    for writer_input, expected_result in writer_patterns:
        result = writer(*writer_input)
        if result != expected_result:
            return TestFailed("writer: " + repr(writer_input))

    for pattern in round_trip_patterns:
        str_form = writer(*pattern)
        final = reader(str_form)
        if final != pattern:
            return TestFailed("round-trip: " + repr(pattern))

    return TestOK()


def run_all(debug=False):
    tests = [
        load_cursescfg,
        parallel_seed,
        test_multiprocessing,
        timeout,
        #test_test,
        hiscores_0_6_42,
    ]

    impl = platform.python_implementation()
    major, minor, micro, releaselvel, series = sys.version_info
    version_string = '{}.{}.{}'.format(major, minor, micro)
    if releaselvel != 'final':
        version_string += '{}{}'.format(releaselvel, series)
    if impl != 'CPython' and '\n' in sys.version:
        second_line = sys.version.split('\n')[1]
    else:
        second_line = ''
    version = '{} {} {} {}'.format(
        impl, version_string, second_line, sys.executable
    )

    results = []
    for test in tests:
        try:
            if debug:
                print('\nRunning ' + test.__name__)
            result = test(automate=True, verbose=debug)
        except Exception as err:
            exception = sys.exc_info()
            traceback.print_exception(*exception)
            result = TestFailed('Raised ' + repr(err))
        results.append((test, result))

    print('%%##\nResults for {}'.format(version))
    valid_statuses = [TestOK, TestUntestable, TestFailed]
    for status in valid_statuses:
        for test, result in results:
            #if sys.version_info[0] == 3:
            assert result in valid_statuses
            if result == status:
                print('{}: {}'.format(test.__name__, result))
    print('')

    return all(map(lambda x: bool(x[1]), results))


def pre_release():
    exit(not run_all())


def poeoep_probe():
    import curses
    import math
    import multiprocessing
    import subprocess
    import threading

    boolmap = ('original', 'Pööp')

    curses_version = curses.version
    math_version = boolmap['Minix' in repr(math.pi)]
    multiprocessing_version = boolmap[not 'Pipe' in dir(multiprocessing)]
    subprocess_version = boolmap[not 'call' in dir(subprocess)]
    threading_version = boolmap[threading.active_count() == 2]

    print('curses version:          {}'.format(curses_version))
    if curses.version in ('Pööp', 'Pp'):
        print('curses.input_mode:       {}'.format(curses.input_mode))
        print('curses.dumb_term:        {}'.format(curses.dumb_term))
        print('curses.disable_color:    {}'.format(curses.disable_color))
    print('math version:            {}'.format(math_version))
    print('multiprocessing version: {}'.format(multiprocessing_version))
    print('subprocess version:      {}'.format(subprocess_version))
    print('threading version:       {}'.format(threading_version))
    print('platform.version():      {}'.format(platform.version()))
