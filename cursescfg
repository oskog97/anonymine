#!/usr/bin/python
#@not-modified@
# Remove the above line if this file has been modified.
# If you don't, this file will be overwritten.


# ***WARNING***  This file allows code execution, don't trust configuration
# files from strangers.


# Copyright (c) Oskar Skog, 2016-2024
# Released under the 2-Clause (Simplified) BSD License.

# NOTICE:
# The keys (the format) may change when you update the game.  If that
# happens, the configuration files need to be replaced for the game to
# function.  Any modification would need to be remade, so keep a
# backup.
#
# BUG: The user's own configuration have a higher priority and will be loaded
# if available.  If they are out of date, the game will crash.


{
    # Various options to deal with platform specific issues.
    # These can be set to 'on', 'off' or 'auto'. 'auto' is assumed
    # for any option that is missing.
    'options': {
        'autorefresh':      'auto',
        'disable-mouse':    'auto',
    },
    # curses-mouse-input must be None on systems with no support for
    # mouse in curses.
    'curses-mouse-input': eval({True:
    """{
        # BUTTON1 = Left
        # BUTTON3 = Right
        'interval':     0,  # Maximum time between PRESSED and RELEASED in
                            # order to count the two as a CLICK.
                            # A high value requires waiting between presses
                            # and releases, so set it to zero if you don't
                            # intend to use BUTTONn_CLICKED.  Default: 200
        # First select, then confirm.  This prevents unintended explosions
        # if the field moves.
        'flag-select':      [curses.BUTTON3_PRESSED,   ],
        'flag-confirm':     [curses.BUTTON3_RELEASED,  ],
        'reveal-select':    [curses.BUTTON1_PRESSED,   ],
        'reveal-confirm':   [curses.BUTTON1_RELEASED,  ],
        # Also supported: 'flag', 'reveal'
    }""",
    False: "None"}['getmouse' in dir(curses)]),
    'curses-input': {
        # Key bindings:
        # Only ASCII characters are supported in the key bindings.
        # (3 = ^C and 459 = numpad enter on Windows)
        'quit':                 ['Q', 3,                     ],
        'flag':                 ['f', 'r',  't',  'g', '\t', ],
        'reveal':               [' ', '\n', '\r', 459,       ],
        'toggle-attention':     ['!', '?',                   ],
        # Hexagonal direction numbers:
        #  5 0
        # 4   1
        #  3 2
        'hex0':     ['u', '9', 'e', ],
        'hex1':     ['l', '6', 'd', ],
        'hex2':     ['n', '3', 'x', ],
        'hex3':     ['b', '1', 'z', ],
        'hex4':     ['h', '4', 'a', ],
        'hex5':     ['y', '7', 'w', ],
        # Neumann and Moore grid directions:
        # NW    up      NE
        # left          right
        # SW    down    SE
        'up':    ['k', 'w', '8', curses.KEY_UP,    ],
        'right': ['l', 'd', '6', curses.KEY_RIGHT, ],
        'down':  ['j', 's', '2', curses.KEY_DOWN,  ],
        'left':  ['h', 'a', '4', curses.KEY_LEFT,  ],
        'NE':    ['u',      '9', ],
        'SE':    ['n',      '3', ],
        'SW':    ['b',      '1', ],
        'NW':    ['y',      '7', ],
    },
    # 'pre-doc':        In-game documentation on the key bindings
    #                   for flagging and revealing.
    # 'doc-hex':        In-game documentation on the key bindings
    #                   for the hexagonal gametype.
    # 'doc-square':     In-game documentation on the key bindings
    #                   for the other gametypes.
    'pre-doc': '''
    - Press f (or r, t or g) or tab to place or remove a flag on a cell.
    - Press space or enter to reveal (click on) a cell.
    - Type ? to find difficult to find cells. Press again to
      deactivate attention mode.
    - Shift-Q or Control-c to quit.
    ''',
    'doc-square': '''
    In the traditional and von Neumann modes, you can steer with the arrow
    keys, wasd, the numpad or hjklyubn.
    
    You can also (probably) use the mouse, but it's really finicky.
    ''',
    'doc-hex': '''
    You can also (probably) use the mouse.  You can scroll the wheel while
    hovering over a cell to move the "cell pointer" there, this is best done
    at the edges.
    
    This mode lacks an obvious up or down.
     w e        y u        7 9
    a   d      h   l      4   6
     z x        b n        1 3
    ''',
    'curses-output': {
        # Textics
        # =======
        #
        # 'key': ('C', 'foreground', 'background', attributes),
        
        # Keys
        # ----
        #
        # These keys consist of two parts:
        #       * the property (mandatory)
        #       * the mode (optional)
        # 'property'
        # 'property:mode'
        # The mode-part is used to give different gametypes different
        # values for a property.
        # Recognised properties are:
        #       'hex'           Only for hexagonal fields.
        #       'moore'         Only for traditional fields.
        #       'neumann'       Only for fields with von Neumann
        #                       neighbourhoods.
        #       'square'        'moore' + 'neumann'
        #       no mode         Used when the preferred mode is
        #                       unavailable.
        # Search order:
        # von Neumann fields:   ':neumann',     ':square',      ''
        # hexagonal fields:     ':hex',         ''
        # traditional fields:   ':moore',       ':square',      ''
        #
        # Properties:
        #       '1', '2', '3', '4', '5', '6', '7', '8'
        #       'flag'
        #       'free'          (unclicked)
        #       'attention'     free cell when in attention mode or a number
        #                       with too many surrounding flags (the number
        #                       will override the character.)
        #       'zero'          (no mines around this cell)
        #       'mine'          (game over; don't click on these)
        #       'grid'          The space between the characters of each cell
        #       'grid2'         (grid2 to grid4 are only used in hexagonal:
        #       'grid3'         grid is the closest to the "cursor" and grid4
        #       'grid4'         is the furthest away.)
        #       'cursor-l'      (Mark the position of the selected cell)
        #       'cursor-r'      (Mark the position of the selected cell)
        #       'text'          (All messages that appear in curses mode
        #                       and the flags left text)
        #       'background'    (To differ from the grid.)
        #
        
        # Values
        # ------
        #
        # The values are tuples of:
        # [0]:  character (Required by all keys except 'number', 'grid',
        #       'text' and 'background'. For these three, the character
        #       will be ignored. ('background' is ignored due to a bug.))
        #       * Non-ASCII characters are supported in Python 3, but not in
        #       Python 2.  See http://bugs.python.org/issue18118
        #       * Only one column wide characters are supported.  Characters
        #       that take up multiple columns are not supported.
        # [1]:  foreground color; valid colors are: "BLACK", "BLUE",
        #       "GREEN", "CYAN", "RED", "MAGENTA", "YELLOW" and
        #       "WHITE".
        # [2]:  background color; (same valid colors as for foreground)
        # [3]:  attributes; this is curses attributes or'ed together,
        #       see link below for a table of common attributes:
        # https://docs.python.org/3/howto/curses.html#attributes-and-color
        #       To combine blinking and bright/bold text use:
        #               curses.A_BLINK | curses.A_BOLD
        #       as attribute.  The pipe character is the or operator.
        #       NOTICE: The effects of the attributes will vary between
        #       different terminals:
        #               - curses.A_BOLD may or may not make the background
        #               color brighter.  And as the name suggests, some
        #               terminals may use a bolder font instead.
        #               - curses.A_BLINK is not always supported and on
        #               the Linux virtual consoles (tty1 to tty6) it does
        #               something weird with the color.

        # curses.A_INVIS is useless, I've tried using it for 'free' to
        # prevent visible dots on some color terminals but without success.
        #   SerenityOS terminal: Not supported, which makes the terminal slower
        #   Solaris console: No difference
        # I can't recall any other terminals being affected.
        # See doc/latest-tested on the 0.6.37 branch in git for details.
        
       #                         foreground           attributes
       #key                character        background
        'text':           (None, "BLACK",   "WHITE",  curses.A_NORMAL),
        'flag':           ('F',  "GREEN",   "BLACK",  curses.A_REVERSE
                                                      |curses.A_DIM),
        'free':           ('.',  "BLACK",   "BLACK",  curses.A_NORMAL),
        'attention':      ('.',  "WHITE",   "BLACK",  curses.A_REVERSE
                                                      |curses.A_BOLD
                                                      |curses.A_BLINK),
        '1':              ('1',  "YELLOW",  "BLACK",  curses.A_BOLD),
        '2':              ('2',  "RED",     "BLACK",  curses.A_NORMAL),
        '3':              ('3',  "CYAN",    "BLACK",  curses.A_BOLD),
        '4':              ('4',  "WHITE",   "BLACK",  curses.A_NORMAL),
        '5':              ('5',  "WHITE",   "BLACK",  curses.A_BOLD),
        '6':              ('6',  "GREEN",   "BLACK",  curses.A_NORMAL),
        '7':              ('7',  "MAGENTA", "BLACK",  curses.A_BOLD),
        '8':              ('8',  "WHITE",   "BLUE",   curses.A_BLINK),
        'zero':           ('-',  "WHITE",   "BLACK",  curses.A_DIM),
        'mine':           ('X',  "RED",     "BLACK",  curses.A_REVERSE
                                                      |curses.A_BOLD),
        'cursor-l':       ('>',  "WHITE",   "BLACK",  curses.A_BOLD),
        'cursor-r':       ('<',  "WHITE",   "BLACK",  curses.A_BOLD),
        'grid':           (' ',  "BLACK",   "BLACK",  curses.A_NORMAL),
        # FIXME: Attributes on 'background' affect everything.
        'background':     (None, "BLACK",   "BLUE",   curses.A_NORMAL),
       # Hexagonal:
        'zero:hex':       ('-',  "GREEN",   "BLACK",   curses.A_BOLD),
        'cursor-l:hex':   ('(',  "WHITE",   "BLACK",   curses.A_BOLD),
        'cursor-r:hex':   (')',  "WHITE",   "BLACK",   curses.A_BOLD),
        'grid:hex':       (None, "GREEN",   "BLACK",   curses.A_BOLD),
        'grid2:hex':      (None, "BLUE",    "BLACK",   curses.A_BOLD),
        'grid3:hex':      (None, "BLUE",    "BLACK",   curses.A_DIM),
        'grid4:hex':      (None, "BLUE",    "BLACK",   curses.A_DIM),
        'background:hex': (None, "BLUE",    "BLACK",   curses.A_NORMAL),
    },
    'highscores': {
        'tabsize':      3,      # Tab size for the highscores
        'min_tabspace': 2,      # Minimum amount of spaces in a tab.
        'less_step':    3,      # Horizontal step size for less(1).
        # Real tabs would be: tabsize=8; min_tabspace=1
        # To separate the columns more clearly: increase `min_tabspace`
        # High values of `tabsize` can be excessive.
        # For aesthetic reasons `tabsize` should be divisible by `less_step`.
    },
}
