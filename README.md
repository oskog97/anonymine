Anonymine is a curses mode (terminal) minesweeper that checks if the fields
actually are solvable without guessing and has a few other features.

Apart from being solvable without guessing, Anonymine has a few other features:
- Three different field types
    - von Neumann (diagonals don't count)
    - hexagonal
    - traditional (normal)
- There's a highscores table for the losers too.

You can also try it over SSH as `play@anonymine-demo.oskog97.com` port 22.
Password is: `play`

You can find screenshots and more at https://oskog97.com/projects/anonymine/

# See also
- doc/INSTALL.txt for installation instructions
- doc/RELASE-NOTES for important information about updates
- doc/NEWS


# Software & hardware requirements

## Required software
- Python 2.7 or >= 3.3 (both CPython and PyPy supported)
- The `curses` module

## Recommended software
- unix-like operating system or compatibility layer. `fork` is required for
  utilizing multiple CPU cores.
- PyPy, but not on Windows. See more in doc/INSTALL.txt


## Hardware requirements
- Faster CPU is better.  Having more than 8 CPU threads won't help much at
  the default difficulty.

## Installation instructions
See doc/INSTALL.txt


# Tested platforms

## Python version
Python 2.7 and 3.x are both being actively tested, there should not be any
issues whatsoever.

It works with both CPython and PyPy.

## Operating systems

### unix-like
Should work flawlessly on most GNU/Linux distributions and OpenBSD*.

Some versions have been tested to work on various GNU/Linux distributions,
macOS, FreeBSD, OpenBSD, NetBSD, DragonflyBSD, OpenIndiana, Cygwin, GNU/Hurd,
Haiku and SerenityOS.

### Windows
See doc\INSTALL.Windows.txt or
https://gitlab.com/oskog97/anonymine/-/wikis/Windows
