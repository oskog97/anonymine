This is only relevant for staged installs.  If you do not intend to use
DESTDIR, you can ignore this document entirely.

You are expected to have read 'INSTALL.advanced'.  It may be helpful to also
read the source code in installtools/*.


Staged installs (*DESTDIR)
==========================

    NOTICE: This feature is in alpha stage, expect bugs and frequent
    breaking changes!

    The Makefile does NOT use "install command categories" (PRE_INSTALL,
    NORMAL_INSTALL, POST_INSTALL, PRE_UNINSTALL, NORMAL_UNINSTALL,
    POST_UNINSTALL)!
    It is not really designed for creating "binary packages".

    Staged installs are done through Makefile targets.  Pre-install
    and post-install are prevented through the use of two additional
    variables: CHECKCFG_DESTDIR and INSTALLED_DESTDIR.

    CHECKCFG_DESTDIR and INSTALLED_DESTDIR both default to $(DESTDIR)
    which is not useful for staged installs, but will contain any
    oopsies in the staged location instead of screwing things up elsewhere.

    'make install' will skip 'install-check-cfg' if
    DESTDIR != CHECKCFG_DESTDIR and will skip 'postinstall' if
    DESTDIR != INSTALLED_DESTDIR.

    DESTDIR affects:
        install
        overwrite-cfg

    CHECKCFG_DESTDIR affects:       pre-install
        check-install-cfg               WILL ALSO WRITE TO THE FILES!

    INSTALLED_DESTDIR affects:      post-install
        postinstall
        uninstall
        postuninstall

    CHECKCFG_DESTDIR=DESTDIR            -> Force overwriting cfg files
    CHECKCFG_DESTDIR=INSTALLED_DESTDIR  -> check-install-cfg || fail

    Not implemented:
        - Merging and removing old highscores files.  This is only available
          in non-staged installations.

    You may directly use the scripts in 'installtools/target/' instead of
    using Makefile.  They should be (at least somewhat) stable.


    Example 1 (Always overwrite configuration files)
    ================================================

        git clone https://gitlab.com/oskog97/anonymine.git src
        src/configure \
            'DESTDIR=stagedir'                  \
            'INSTALLED_DESTDIR=/opt/anonymine'  \
            'CHECKCFG_DESTDIR=$(DESTDIR)'
        make
        make install

        # Postinstall and postuninstall scripts:
        mkdir scripts
        cp symlinks src/installtools/target/post*install config-vars scripts

        # Possibly on another computer or as another user:
        cp -r stagedir /opt/anonymine
        cd scripts && ./postinstall


    Example 2 (Check configuration files pre-install)
    =================================================

        # NOTE: 'make check-install-cfg' depends on a few files.
        # Might be easier to transfer the entire build directory.

        cd anonymine
        ./configure                         \
            'DESTDIR=foo'                   \
            'INSTALLED_DESTDIR=bar'         \
            'CHECKCFG_DESTDIR=$(INSTALLED_DESTDIR)'
        make
        make install        # Does not create config files

        # Move to target (bar)

        # Try to install configuration files on the system
        make check-install-cfg || { echo "abort" >&2; exit 1;}
        # Copy foo/* to bar
        make postinstall


    'check-install-cfg' needs '$(srcdir)cursescfg' and
    '$(builddir)enginecfg'.  It *will* *install* these files to
    $(CHECKCFG_DESTDIR)$(sysconfdir)/anonymine unless there already are
    modified *and* incompatible files there.

    'postinstall' is only used for desktop stuff on freedesktop.org
    desktops and Cygwin (Windows desktop).

    'postuninstall' undoes 'postinstall' and also runs the 'symlinks'
    script to remove symlinks to modules and any bytecode files.
    NOTE: 'postuninstall' may be changed to 'preuninstall'.


Copy-pasted from INSTALL.advanced.
==================================

    srcdir, builddir, *DESTDIR
    ==========================

        srcdir          Directory extracted from the tarball.

        builddir        Temporary storage;
                        directory for files from the build process.

        *DESTDIR        There are three different, see section
                        "Staged installs".

        $(srcdir) is the directory that was extracted from the tarball.
        $(builddir) is the directory where Makefile will be created and
        where make will create "temporary" files when building
        (make [all]) the program.

        builddir defaults to the current working directory.
        srcdir defaults to the directory containing configure.py.

        Sending builddir=foo to ./configure will create the Makefile
        inside $builddir.  You can cd to $builddir and run
        'make && sudo make install'.

        NOTICE:  Makefile will ignore $builddir and create all temporary
            files in the current working directory.  You MUST cd to
            builddir before you run make.   !!!!!!!!!!

        NOTICE:  If you set any *DESTDIR to a relative path, keep in mind that
            it's 'make' that uses it (from within $builddir rather than
            wherever you happen to be at the moment.)

        *DESTDIR only affects make install, make uninstall, etc.  It has
        zero effect on ./configure.


    Desktop shortcuts
    =================

        freedesktop = 'true' for X11/Wayland:
            Files are installed to DESTDIR by 'target/install'.
            `xdg-desktop-menu install` and such commands will be run with
            paths containing INSTALLED_DESTDIR by 'target/postinstall'

        macosx = 'true' for macOS:
            Files are installed to DESTDIR, no cache-updating commands are
            run.  Staged installs should work.

        cygwin = 'true' for Cygwin (Windows desktop, not for X11):
            Set 'windows-isadmin' to 'true' if you are admin and want
            shortcuts to be installed for all users.
            The options 'windows-desktop' and 'windows-startmenu' can be
            set to 'false' to stop shortcuts from being created.
            Shortcuts will be installed on desktop and in start menu
            regardless of DESTDIR or INSTALLED_DESTDIR.

        haiku = 'true' for Haiku (maybe also BeOS??)
            Files are installed to DESTDIR, no cache-updating commands are
            run.  Staged installs should work but are not recommended as
            the destination path is directly to the user's desktop.

