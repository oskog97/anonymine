def expand(variable_name, all_variables, stacktrace=None):
    '''Do Makefile variable macro-expansion.
    
    `all_variables` is a dictionary of unexpanded Makefile variables,
    where `variable_name` is the key.
    
    `stacktrace` is a list of the variable names we've been asked to expand.
    If the current variable is already there, we know that there is an
    infinite loop.
    '''
    # Prevent infinite recursion:
    if stacktrace is None:
        stacktrace = [variable_name]
    else:
        if variable_name in stacktrace:
            raise ValueError(
                'Infinite recursion found: "{}" repeated'.format(variable_name)
            )
        else:
            stacktrace.append(variable_name)
    
    # Prohibit references to variables with hyphens in the name to prevent
    # Makefile2shell from converting 'test=$(foo-bar)' to 'test="${foo-bar}"'
    # expanding a variable 'foo-bar' IS allowed though.
    # This is done because it would be too hard to do in either Makefile2shell
    # or check_variables.
    if len(stacktrace) > 1:
        if '-' in variable_name:
            raise ValueError(
                'Variables may not use hyphens in references to other '
                'variables. Please use underscores instead.'
            )

    # Evaluate variable:
    chunks = []
    if variable_name not in all_variables:
        raise ValueError(
            'Reference to undefined variable "{}" found.'.format(variable_name)
        )
    var = all_variables[variable_name]
    while var:
        if '$' not in var:
            chunks.append(var)
            break
        # A dollar sign has been encountered.
        pre_dollar, post_dollar = var.split('$', 1)
        chunks.append(pre_dollar)
        if len(post_dollar) == 0:
            raise ValueError('Trailing dollar sign.')
        elif post_dollar[0] == '$':
            # Escaped dollar sign.
            chunks.append('$')
            var = post_dollar[1:]
            continue
        elif post_dollar[0] == '(':
            # Variable insertion.
            variable_name, post_variable = post_dollar[1:].split(')', 1)
            chunks.append(expand(variable_name, all_variables, stacktrace))
            var = post_variable
            continue
        else:
            raise ValueError('Unrecognised character after dollar sign.')
    del stacktrace[-1]
    return ''.join(chunks)


def check_variables(Makefile, flags):
    '''
    Check user specified variables. No infinite loops allowed, etc.
    
    Returns True if any variable has a bad value.  Otherwise False.
    '''
    error = False
    import string
    # '-' is not allowed in shell scripts but will be converted to '_'
    # in `Makefile2shell`.
    safe = string.ascii_letters + string.digits + '_-'
    # Value will lazily be put in "" in config-vars, so prohibit anything
    # that can escape "" or '' in shell scripts.
    # `Makefile2shell` will blindly replace () with {}
    unsafe = '"\'\\$`()'

    # Keep track of potential name collisions
    shell_converted_names = {}

    # Check variables:
    for variable in Makefile:
        # Empty name:
        if not variable:
            error = True
            stderr("Empty variable name.\n")
        # Duplicate (in shell) name:
        converted_name = variable.replace('-', '_')
        if converted_name in shell_converted_names:
            error = True
            stderr('Variable "{}" will collide with "{}".\n'.format(
                variable, shell_converted_names[converted_name]
            ))
        else:
            shell_converted_names[converted_name] = variable
        # Name:
        for ch in variable:
            if ch not in safe:
                error = True
                stderr(
                    'Variable name "{}" contains unsafe characters.\n'
                    ''.format(variable)
                )
                break
        # Value:
        try:
            value = expand(variable, Makefile)
            for ch in unsafe:
                if ch in value:
                    error = True
                    stderr(
                        'Variable "{}" contains unsafe character "{}".\n'
                        ''.format(variable, ch)
                    )
        except ValueError:
            # Infinite loops or incorrect syntax for variable expansion:
            error = True
            msg = str(sys.exc_info()[1])
            stderr(
                'Error in variable "{}": {}\n'.format(variable, msg)
            )
    # Check $USERPROFILE on Cygwin:
    userprofile = os.getenv('USERPROFILE', '')
    for ch in unsafe.replace('\\', ''):
        if ch in userprofile:
            error = True
            stderr('There is a "{}" in your Windows %USERPROFILE%.\n'.format(
                ch
            ))
    if error:
        if flags['f']:
            stderr('config-vars will likely broken!\n')
        stderr('./configure may crash at any moment!\n\n')
    return error


def Makefile2shell(outfile, Makefile_dict):
    '''
    Export a dictionary of Makefile variables as variables in a
    Shell scipt.

    '-' will be converted to '_' in variable names.
    Other bad characters are strictly forbidden

    Example input
    ```
    {
        'prefix': '/usr/local',
        'bindir': '$(prefix)/bin',
        'test-var': 'foo',
    }
    ```

    `outfile` will be created an contain:
    ```
    prefix="/usr/local"
    test_var=foo
    bindir="${prefix}/bindir"
    ```
    '''
    Makefile_dict = Makefile_dict.copy()
    f = open(outfile, 'w')
    # Keep track of variables the shell knows
    shell_dict = {}
    while Makefile_dict:
        # Find variables that can be evaluated at this time:
        batch = {}
        for key in Makefile_dict:
            try:
                shell_dict[key] = Makefile_dict[key]    # MUST add temporarily
                expand(key, shell_dict)
                # `expand` raises ValueError if `key` can't be evaluated from
                # the known variables in `shell_dict`.
                batch[key] = Makefile_dict[key]
            except ValueError:
                pass
            # Remove variable from shell_dict:
            #   - If it couldn't be evaluated, it MUST be removed
            #   - If it could be evaluated by `expand`, the output order
            #     may look ugly.
            del shell_dict[key]
        # Print those variables in order:
        for key in sorted(batch):
            shell_dict[key] = batch[key]
            del Makefile_dict[key]
            # Output
            value = shell_dict[key].replace('(', '{').replace(')', '}')
            key = key.replace('-', '_')
            f.write('{}="{}"\n'.format(key, value))
    f.close()
