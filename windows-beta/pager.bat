@echo off
:: https://superuser.com/a/424215/651050
setlocal
set file=%temp%\Anonymine-highscores-display.temp-%random%%random%.txt
more >"%file%"

:: start /wait notepad "%f%"

start notepad "%file%"
ping 127.0.0.1 -w 1 -n 2 >NUL

del "%file%"
