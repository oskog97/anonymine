#!/bin/sh
'''
' 2>&-
exec python "$0" "$@" || exec python3 "$0" "$@"
# Haiku has no /usr/bin/env
'''

# Copyright (c) Oskar Skog, 2022
# Released under the FreeBSD (2-Clause) license.


import os
import sys

# Inhibit exit(1) if imported as a module
if __name__ != '__main__':
    def exit(x): raise ValueError

# PIL/Pillow
try:
    from PIL import Image
except ImportError:
    sys.stderr.write(
        "Can't import PIL\n"
        "Try running 'python{} -m pip install Pillow'\n"
        .format('3' if sys.version_info[0] == 3 else '')
    )
    exit(1)

# Local file palette.py generated from Palette.h from Haiku
try:
    from palette import palette
except ImportError:
    sys.stderr.write(
        "Can't import palette (local file palette.py)\n"
        "Try running 'make' in beos-icon-tools\n"
    )
    exit(1)


if sys.version_info[0] == 2:
    int_to_byte = chr
else:
    int_to_byte = lambda i: bytes([i])


def usage():
    sys.stderr.write(
        'Usage: ./convert.py srcfile destfile\n'
        'Converts image to BeOS icon attribute value\n'
        'srcfile can be of any image format supported by PIL\n'
        'srcfile MUST already use the BeOS palette, use palette.gpl in GIMP\n'
        'srcfile MUST be either 16x16 or 32x32\n'
    )
    exit(1)


def main(srcfile, dstfile):
    img = Image.open(srcfile).convert("RGBA")
    if (img.width, img.height) not in [(16, 16), (32, 32)]:
        usage()

    outfile = open(dstfile, "wb")
    size = img.width
    for y in range(size):
        for x in range(size):
            r, g, b, a = img.getpixel((x, y))
            # Ensure transparent pixels have the correct value.
            if a == 0:
                r = g = b = 255
            # Convert RGBA to 8-bit paletted pixel
            for index, value in enumerate(palette):
                if value == (r, g, b, a):
                    outfile.write(int_to_byte(index))
                    break
            else:
                sys.stderr.write(
                    "Unsupported color {} found at ({}, {})\n"
                    "Run 'make' in beos-icon-tools to generate "
                    "'palette.gpl' which can be imported\n"
                    "as a palette in GIMP\n"
                    .format((r, g, b, a), x, y)
                )
                os.remove(dstfile)
                exit(1)
    outfile.close()


if __name__ == '__main__':
    if len(sys.argv) != 3:
        usage()
    main(sys.argv[1], sys.argv[2])
