#!/usr/bin/env python

import glob
import os
import shutil

pkgdir = os.path.abspath(os.getenv('pkgdir'))
srcdir = os.path.abspath(os.getenv('srcdir'))

# README is symlink to README.md, don't need duplicates
# attic/, doc/cheat-codes/ and doc/latest-tested are not useful for anyone else
# desktop/icon-GIMP/ is kept out of tarballs due to size
# installtools/autotest requires git
distignore = {
    '': ['.git', 'attic', 'README.md'],
    'doc': ['cheat-codes', 'latest-tested', 'old-ChangeLog'],
    'desktop': ['icon-GIMP', 'README.md'],
    'installtools': ['autotest', 'manualtest'],
    'foo/bar': [],      # Example
    'GLOBAL': [],
}

# Load most of the ignore lists from .gitignore
gitignore = list(filter(
    None,
    open(os.path.join(srcdir, '.gitignore')).read().split('\n')
))
for entry in gitignore:
    if entry.startswith('/'):
        # Strip leading slash for dirpath
        entry = entry.split('/')
        dirpath = '/'.join(entry[1:-1])
        if '*' in dirpath:
            sys.stderr.write("Unsupported wildcard in .gitignore\n")
            exit(1)
        if dirpath not in distignore:
            distignore[dirpath] = []
        distignore[dirpath].append(entry[-1])
    else:
        distignore['GLOBAL'].append(entry)

# Copy files
for path, dirs, files in os.walk(srcdir):
    # Calculate current relative path (no leading slash)
    rel_path = path[len(srcdir)+1:]
    dest_path = os.path.join(pkgdir, rel_path)
    # chdir needed for glob.   NOTE: Must use abolute paths with os.walk!
    os.chdir(path)
    # Find all files to ignore
    ignore_list = []
    if rel_path in distignore:
        for entry in distignore[rel_path]:
            ignore_list += glob.glob(entry)
    for entry in distignore['GLOBAL']:
        ignore_list += glob.glob(entry)
    # Ignore the files
    for ignore in ignore_list:
        if ignore in dirs:
            dirs.remove(ignore)
        if ignore in files:
            files.remove(ignore)
    # Avoid infinite recursion:
    if os.path.basename(pkgdir) in dirs:
        dirs.remove(os.path.basename(pkgdir))
    # Create directories and copy files:
    for directory in dirs:
        os.mkdir(os.path.join(dest_path, directory))
    for filename in files:
        # Follows symlinks.  The tarballs don't have any symlinks.
        shutil.copy(
            os.path.join(path, filename),
            os.path.join(dest_path, filename)
        )
