/*
 * gen-palette.c:
 * Copyright (c) Oskar Skog, 2022
 * Released under the FreeBSD (2-Clause) license.
 *
 * Palette.h:
 * Copyright 2005, Haiku, Inc. All Rights Reserved.
 * Distributed under the terms of the MIT License.
 */

#include <stdint.h>
#include <stdio.h>

// rgb_color is a datatype used in Palette.h
typedef struct {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
} rgb_color;

#include "Palette.h"
// rgb_color kSystemPalette[256] = {...};

int main()
{
    FILE *gimp_palette = fopen("palette.gpl", "w");
    FILE *python_palette = fopen("palette.py", "w");
    if (!gimp_palette || !python_palette)
        return 1;
    // No matter how I try GIMP refuses to recognise the name.
    fprintf(gimp_palette, "GIMP Palette\n"
                          "Name: \"BeOS icon palette\"\n"
                          "Columns: 16\n"
                          "#\n");
    fprintf(python_palette, "palette = [\n");
    for (int i = 0; i < 256; i++) {
        rgb_color c = kSystemPalette[i];
        // Alpha is either 255 or 0
        // Don't add transparent colors to the GIMP palette.
        if (c.a == 255) {
            fprintf(gimp_palette, "%3d %3d %3d\n", c.r, c.g, c.b);
        }
        fprintf(python_palette, "(%3d, %3d, %3d, %3d),\n", c.r, c.g, c.b, c.a);
    }
    fprintf(python_palette, "]\n");
    fclose(gimp_palette);
    fclose(python_palette);
    return 0;
}
