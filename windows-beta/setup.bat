@echo off

:: Check if called as windows-beta\setup
if exist anonymine.py (
    cd windows-beta
)

:: Check that we are where we're supposed to be
if not exist pager.bat (
    echo Working directory is wrong
    pause
    exit 1
)

:: Create highscores.txt, but don't nuke it
if not exist highscores.txt (
    type NUL>highscores.txt
)

:: Why not do some normal installation stuff as well
echo Making sure pip and windows-curses is installed...
python -m ensurepip
python -m pip install windows-curses
