#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) Oskar Skog, 2016-2024
# Released under the FreeBSD license (2-Clause)

import errno
import os
import sys
import traceback


# If stdout/stderr is closed then sys.stdout/sys.stderr will be None.
# https://docs.python.org/3/library/sys.html#sys.__stderr__
if sys.stdout is None:
    stdout = lambda s: None
else:
    stdout = sys.stdout.write

if sys.stderr is None:
    stderr = lambda s: None
else:
    stderr = sys.stderr.write


def shellescape(s):
    'Returns `s` in single quotes (and escaped) suitable for shell.'
    # Everything in single quotes appear to be interpreted literally, even \'
    return "'" + s.replace("'", "\'\"\'\"\'") + "'"


def expect_errno(exception, *errno_values):
    '''
    Print a warning if `exception` is not an OSError with errno set to
    one of the expected values.

    Use this function when replacing bare except statements or
    `except OSError` without checking errno value.  This will catch
    unexpected exceptions without altering existing behaviour.

    Added 2024-08-06 for version 0.6.37

    os.listdir
        ENOENT
        ENOTDIR     Jython (Linux)

    eg. From
        try:
            os.stat('/might/not/exist')
        except:
            <handle it>
    to
        try:
            os.stat('/might/not/exist')
        except Exception as e:
            expect_errno(e, errno.ENOENT)

    '''
    # FileNotFoundError is based on OSError and has errno set appropriately
    # when caused by os.* functions, hence checking for OSError and errno
    # is sufficient.
    exc_info = sys.exc_info()
    unexpected = not isinstance(exception, OSError)
    if not unexpected:
        try:
            if exception.errno not in errno_values:
                unexpected = True
        except AttributeError:
            stderr('OSError has no attribute "errno"\n')
            unexpected = True
    if unexpected:
        stderr('\n\n\nWARNING: Unexpected exception:\n')
        traceback.print_exception(*exc_info)
        stderr('\nCurrent traceback (from exception *handler*):\n')
        traceback.print_stack()
        stderr('\nEND of traceback.  Please report this issue.\n\n\n')


def is_command(command):
    '''
    Returns True if `command` is anywhere on $PATH, otherwise False
    '''
    for directory in os.getenv('PATH').split(':'):
        try:
            if command in os.listdir(directory):
                return True
        except OSError as e:
            # Non-existent directory in $PATH
            expect_errno(e, errno.ENOENT)
    return False


def expand(variable_name, all_variables, stacktrace=None):
    '''Do Makefile variable macro-expansion.
    
    `all_variables` is a dictionary of unexpanded Makefile variables,
    where `variable_name` is the key.
    
    `stacktrace` is a list of the variable names we've been asked to expand.
    If the current variable is already there, we know that there is an
    infinite loop.
    '''
    # Prevent infinite recursion:
    if stacktrace is None:
        stacktrace = [variable_name]
    else:
        if variable_name in stacktrace:
            raise ValueError(
                'Infinite recursion found: "{}" repeated'.format(variable_name)
            )
        else:
            stacktrace.append(variable_name)

    # Evaluate variable:
    chunks = []
    if variable_name not in all_variables:
        raise ValueError(
            'Reference to undefined variable "{}" found.'.format(variable_name)
        )
    var = all_variables[variable_name]
    while var:
        if '$' not in var:
            chunks.append(var)
            break
        # A dollar sign has been encountered.
        pre_dollar, post_dollar = var.split('$', 1)
        chunks.append(pre_dollar)
        if len(post_dollar) == 0:
            raise ValueError('Trailing dollar sign.')
        elif post_dollar[0] == '$':
            # Escaped dollar sign.
            chunks.append('$')
            var = post_dollar[1:]
            continue
        elif post_dollar[0] == '(':
            # Variable insertion.
            if ')' not in post_dollar:
                raise ValueError('Missing closing parenthesis')
            variable_name, post_variable = post_dollar[1:].split(')', 1)
            chunks.append(expand(variable_name, all_variables, stacktrace))
            var = post_variable
            continue
        else:
            raise ValueError('Unrecognised character after dollar sign.')
    del stacktrace[-1]
    return ''.join(chunks)


def getargs(flag_chars):
    '''Parse arguments.
    
    Makefile, flags = getargs(flag_chars)
    
    `flags_chars` is a string of short option (flag) characters.  The
    short options do not take any arguments.  Command line syntax:
        -F
    
    `Makefile` is a dictionary of the specified variables (long
    options) and some default variables.  The command line syntaxes are:
        key value
        key=value
        --key value
        --key=value
    
    `flags` is a dictionary where each character of `flag_chars` is a
    key. The value is True if the flag was specified and False if not.
    '''
    # Find all variables
    accept_flags = True
    flags = {}
    for c in flag_chars:
        flags[c] = False
    Makefile = {}
    try:
        while len(sys.argv) > 1:
            arg = sys.argv.pop(1)
            # Accept flags.
            if arg == '--':
                accept_flags = False
                continue
            if accept_flags:
                if arg[0] == '-' and arg[1] != '-':
                    for c in arg[1:]:
                        if c in flags:
                            flags[c] = True
                        else:
                            raise Exception('Bad flag: ' + c)
                    continue
            # * Having the value on the next argument is only allowed
            # with long options.
            # * We don't want leading dashes in the variable names.
            allow_separation = False
            if arg.startswith('--'):
                # --name[=value]
                arg = arg[2:]
                allow_separation = True
            if arg.startswith('-'):
                raise Exception('Only zero or two leading dashes are allowed.')
            # Check if the value is immediately assigned, or if it is
            # allowed to be assigned in the next
            if '=' in arg:
                # [--]name=value
                varname, value = arg.lstrip('-').split('=', 1)
                Makefile[varname] = value
            elif allow_separation:
                try:
                    value = sys.argv.pop(1)
                except IndexError:
                    raise Exception('Missing argument.')
                Makefile[arg] = value
            else:
                raise Exception('Missing argument.')
    except Exception:
        stderr(
            'Cannot parse your arguments.\nAt "{}":\n{}\n'
            'There may be more errors.\n'.format(arg, sys.exc_info()[1])          
        )
        sys.exit(1)
    
    return Makefile, flags


def check_variables(Makefile, flags, second_run=False):
    '''
    Check user specified variables. No infinite loops allowed, etc.
    
    Returns True if any variable has a bad value.  Otherwise False.
    '''
    error = False
    import string
    # '-' is not allowed in shell scripts but will be converted to '_'
    # in `Makefile2shell`.
    safe = string.ascii_letters + string.digits + '_-'
    unsafe = '"\'\\$`() #&'

    # Keep track of potential name collisions
    shell_converted_names = {}

    # Check variables:
    for variable in Makefile:
        # Empty name:
        if not variable:
            error = True
            stderr("Empty variable name.\n")
        # Duplicate (in shell) name:
        converted_name = variable.replace('-', '_')
        if converted_name in shell_converted_names:
            error = True
            stderr('Variable "{}" will collide with "{}".\n'.format(
                variable, shell_converted_names[converted_name]
            ))
        else:
            shell_converted_names[converted_name] = variable
        # Name:
        if variable[0] in string.digits:
            error = True
            stderr(
                'Error: Variable name "{}" begins with a digit.\n'
                ''.format(variable)
            )
        for ch in variable:
            if ch not in safe:
                error = True
                stderr(
                    'Error: Variable name "{}" contains unsafe character {}.\n'
                    ''.format(variable, repr(ch))
                )
                break
        # Value:
        try:
            value = expand(variable, Makefile)
            for ch in unsafe:
                if ch in value:
                    if ch == ' ':
                        # Need to allow spaces as they are not too uncommon on
                        # Cygwin.
                        stderr(
                            'Warning: Variable "{}" contains spaces.\n'
                            ''.format(variable)
                        )
                    else:
                        error = True
                        stderr(
                            'Error: Variable "{}" contains potentially unsafe '
                            'character {}\n'.format(variable, repr(ch))
                        )
        except ValueError:
            # Infinite loops or incorrect syntax for variable expansion:
            error = True
            msg = str(sys.exc_info()[1])
            stderr(
                'Error in variable "{}": {}\n'.format(variable, msg)
            )
    # Check $USERPROFILE on Cygwin:
    userprofile = os.getenv('USERPROFILE', '')
    for ch in unsafe.replace('\\', '').replace(' ',''):
        if ch in userprofile:
            error = True
            stderr('There is a "{}" in your Windows %USERPROFILE%.\n'.format(
                ch
            ))
    if error:
        if flags['f']:
            stderr('config-vars will likely broken!\n')
        if second_run:
            stderr("'make' and 'make install' may break!\n\n")
        else:
            stderr('./configure may crash at any moment!\n\n')
    return error


def chk_deps(Makefile, flags):
    ''' Check dependencies.  Return True if there was an error '''
    error = False
    modules = {
        'curses':
            "It's a standard module on unix-like systems, but some systems\n"
            "keep the curses module in a separate package from Python.\n"
            "Eg. py38-curses if your python is python38 (3.8)\n"
            "\n"
            "Some Python implementations such as Jython lack the curses\n"
            "module regardless of the underlying operating system.\n"
            "If you're not on a unix-like system, you're not exepected to\n"
            "run this configure script at all.\n",
        'math':
            "Anonymine versions 0.2.30 to 0.5.17 may still work without the\n"
            "`multiprocessing` module.\n",
        'multiprocessing':
            "Anonymine version 0.5.17 and below may still work without the\n"
            "`multiprocessing` module.\n",
        'subprocess':
            "Anonymine version 0.2.30 to 0.5.17 may still work without the\n"
            "`subprocess` module.\n",
    }
    for module in modules:
        try:
            if flags['v']:
                stderr('Testing module {}...\n'.format(module))
            exec("import " + module)
            # Test to see if the module is working:
            filename = "test-" + module + ".py"
            dirname = os.path.join(Makefile['srcdir'], 'installtools')
            # Do we have a test for this module:
            try:
                if filename not in os.listdir(dirname):
                    continue
            except Exception as e:
                stderr("Failed to run test for {}: {}\n\n".format(module, e))
                continue
            # Run test:
            command = "{} {}".format(
                shellescape(Makefile['python']),
                shellescape(os.path.join(dirname, filename))
            )
            assert os.system(command) == 0
        except Exception:
            error = True
            stderr('Failed to import or use module "{}":\n'.format(module))
            stderr(modules[module])
            stderr('\n')
    # IronPython
    try:
        eval("\n1")
    except SyntaxError:
        error = True
        stderr("This Python implementation is incompatible:\n"
               "eval() doesn't like multi-line expressions.\n\n")
    if flags['v']:
        stderr('Done testing modules.\n\n')
    return error


def find_prefix(Makefile, flags):
    '''
    All functions beginning with "find_" are Makefile variable generator
    function.
    It takes two arguments: Makefile and flags.
    
    `Makefile` is a dictionary of all Makefile variables to be prepended
    to the Makefile.
    
    `flags` is a dictionary where each key is a flag and its value is a
    boolean representing if the flag was set or not.
    
    Set Makefile['prefix'] if needed to.
    This is probably the first variable that needs to be set.
    
    Returns True if there was any error.
    '''
    # Note: prefix can change depending on the Python interpreter used.
    if 'prefix' in Makefile:
        Makefile['prefix-manual'] = 'true'
        return False
    Makefile['prefix-manual'] = 'false'

    trywith = [
        '/usr/local',
        '/usr',
        '/boot/system/non-packaged',    # Haiku
        '/opt/homebrew',                # Homebrew on Apple Silicon macOS
        '/home/linuxbrew/.linuxbrew',   # Homebrew on Linux
        sys.prefix,     # Fallback if nothing else works
    ]
    for path in trywith:
        # Test that prefix is good for $PATH in shell:
        os_path_ok = False
        for element in os.getenv('PATH').split(':'):
            if element.startswith(path):
                os_path_ok = True
                break
        if os_path_ok:
            Makefile['prefix'] = path
            return False
    else:
        stderr('Cannot find $(prefix).\n\n')
        return True
    return False


def find_EXECUTABLES(Makefile, flags):
    '''
    See the doc-string for find_prefix as well.
    
    Set Makefile['EXECUTABLES'] if needed to.
    Depends (directly) on $(gamesdir) and $(bindir).
    Depends (indirectly) on $(prefix).
    '''
    if 'EXECUTABLES' not in Makefile:
        acceptable = os.getenv('PATH').split(':')
        for exec_dir in ('gamesdir', 'bindir'):
            if expand(exec_dir, Makefile) in acceptable:
                Makefile['EXECUTABLES'] = '$('+exec_dir+')'
                return False
        else:
            stderr("Can't find a place to store programs.\n\n")
            return True
    else:
        return False


def find_sysconfdir(Makefile, flags):
    '''
    See the doc-string for find_prefix as well.
    
    Set Makefile['sysconfdir'] if needed to.
    Depends on $(prefix)
    '''
    if 'sysconfdir' not in Makefile:
        if sys.platform.startswith('haiku'):
            # $(prefix)/etc/anonymine -> $(prefix)/data/anonymine
            Makefile['sysconfdir'] = '$(prefix)/data'
            return False
        try:
            os.listdir(expand('prefix', Makefile) + '/etc')
            Makefile['sysconfdir'] = '$(prefix)/etc'
        except Exception as e:
            expect_errno(e, errno.ENOENT, errno.ENOTDIR)
            Makefile['sysconfdir'] = '/etc'
    return False


def find_vargamesdir(Makefile, flags):
    '''
    See the doc-string for find_prefix as well.
    
    Set Makefile['localstatedir'] if needed to.
    Set Makefile['vargamesdir'] if needed to.
    Depends on $(prefix).
    '''
    def inner(Makefile, flags):
        # $(localstatedir) = $(prefix)/var or /var
        if 'localstatedir' not in Makefile:
            try:
                os.listdir(expand('prefix', Makefile) + '/var')
                Makefile['localstatedir'] = '$(prefix)/var'
            except Exception as e:
                expect_errno(e, errno.ENOENT, errno.ENOTDIR)
                try:
                    os.listdir('/var')
                    Makefile['localstatedir'] = '/var'
                except Exception as e:
                    expect_errno(e, errno.ENOENT, errno.ENOTDIR)
                    return True
        # $(vargamesdir) = $(localstatedir)/games or $(localstatedir)
        # eg. /var/games
        if 'oldvargamesd' in Makefile and 'vargamesdir' not in Makefile:
            stderr(
                "ERROR: $(oldvargamesd) will be overridden by ./configure\n"
                "unless $(vargamesdir) is also manually defined.\n\n"
            )
            return True
        if 'vargamesdir' in Makefile:
            if 'oldvargamesd' not in Makefile:
                Makefile['oldvargamesd'] = ''
            return False
        else:
            # Changed in 0.5.31 on Haiku
            if sys.platform.startswith('haiku'):
                Makefile['oldvargamesd'] = '$(prefix)/data/anonymine/highscores'
                Makefile['vargamesdir'] = '/boot/home/config/var'
                try:
                    os.stat(expand('oldvargamesd', Makefile) + '/anonymine')
                except Exception as e:
                    expect_errno(e, errno.ENOENT)
                    Makefile['oldvargamesd'] = ''
                return False
            # new $(vargamesdir)
            for tail in ('/games', ''):
                try:
                    os.listdir(expand('localstatedir', Makefile) + tail)
                    Makefile['vargamesdir'] = '$(localstatedir)' + tail
                    break
                except Exception as e:
                    expect_errno(e, errno.ENOENT, errno.ENOTDIR)
            else:
                return True
            # Find oldvargamesd (not Haiku)
            # Blank means none
            Makefile['oldvargamesd'] = ''
            # /var -> $(prefix)/var due to configure.py update
            # Look for reverse changes too.
            prefix = expand('prefix', Makefile)
            known_locations = [
                '/var/anonymine',
                '/var/games/anonymine',
                prefix + '/var/anonymine',
                prefix + '/var/games/anonymine',
            ]
            for old in known_locations:
                dirname = old.replace('/anonymine', '')
                if dirname == expand('vargamesdir', Makefile):
                    continue
                try:
                    os.stat(old)
                    stderr(
                        'NOTICE: Found old highscores file '
                        '"{}".\n'.format(old)
                    )
                    Makefile['oldvargamesd'] = dirname
                except Exception as e:
                    expect_errno(e, errno.ENOENT)
                    continue
            return False

    if inner(Makefile, flags):
        stderr("Can't find a place for the highscores file\n\n")
        return True
    else:
        return False


def find_MODULES(Makefile, flags):
    '''
    See the doc-string for find_prefix as well.
    
    Set Makefile['MODULES'] if needed to.
    Depends on $(libdir). $(libdir64) and $(prefix).
    
    $(MODULES) is where Python modules should be installed.
    '''
    if 'MODULES' in Makefile:
        return False

    suffixes = ['/site-packages', '/dist-packages']

    # Find matches
    n_matches = 0
    last_match = None
    for suffix in suffixes:
        for modules_dir in sys.path:
            if modules_dir.endswith(suffix):
                last_match = modules_dir
                n_matches += 1

    # Zero
    if n_matches == 0:
        stderr("No site-packages or dist-packages found anywhere.\n\n")
        return True

    # One
    if n_matches == 1:
        Makefile['MODULES'] = os.path.realpath(last_match)
        return False

    # Many
    preferred_prefixes = [
        expand('prefix', Makefile), # Only option configurable by the user
        '/Library/',                # macOS
        sys.prefix,                 # System python
        '/opt/',                    # Reasonable
    ]
    for prefix in preferred_prefixes:
        for suffix in suffixes:
            criteria = lambda s: s.startswith(prefix) and s.endswith(suffix)
            results = sorted(filter(criteria, sys.path), key=len)
            if results:
                Makefile['MODULES'] = os.path.realpath(results[0])
                if not flags['v']:
                    stdout("Multiple choices for $(MODULES), chose:\n")
                    stdout(Makefile["MODULES"] + "\n\n")
                return False

    # Uh-oh
    stderr("Unable to find suitable location for Python modules.\n\n")
    return True


def find_INSTALL(Makefile, flags):
    '''
    See the doc-string for find_prefix as well.
    
    Sets Makefile['INSTALL'] if needed.
    
    $(INSTALL) is normally "install", but on OpenIndiana (Solaris) it needs to
    be "/usr/ucb/install" or "/usr/gnu/bin/install".
    '''
    if 'INSTALL' not in Makefile:
        trywith = [
            '/usr/ucb/install',
            '/usr/gnu/bin/install',
        ]
        for install in trywith:
            try:
                os.stat(install)
            except Exception as e:
                expect_errno(e, errno.ENOENT)
                continue
            Makefile['INSTALL'] = install
            break
        else:
            Makefile['INSTALL'] = 'install'
    return False


def detect_desktop(Makefile, flags):
    '''
    Checks for the existence of various desktop environments.

    See the doc-string for find_prefix as well.
    
    Sets Makefile['freedesktop'] if needed.
    Sets Makefile['macosx'] if needed.
    Sets Makefile['cygwin'] if needed.
    Sets Makefile['haiku'] if needed.
    Sets Makefile['serenity'] if needed.

    The value for these variables are either 'true' or 'false'.

    If Makefile['cygwin'] gets set to 'true', the following variables
    are also forced to exist:
        'windows-isadmin'
        'windows-startmenu'
        'windows-desktop'

    If Makefile['serenity'] gets set to 'true', the following variables
    are also forced to exist:
        'serenity-launcher'
    '''
    err = False
    
    ### freedesktop.org, "modern" X11 and Wayland desktops.
    if 'freedesktop' not in Makefile:
        freedesktop_paths = [
            '/usr/share/applications',
            expand('prefix', Makefile) + '/share/applications',
            sys.prefix + '/share/applications',
        ]
        for path in freedesktop_paths:
            try:
                os.listdir(path)
                Makefile['freedesktop'] = 'true'
                break
            except OSError as e:
                expect_errno(e, errno.ENOENT, errno.ENOTDIR)
        else:
            Makefile['freedesktop'] = 'false'
    
    ### macOS
    if 'macosx' not in Makefile:
        Makefile['macosx'] = 'false'
        if sys.platform == 'darwin':
            try:
                os.listdir('/Applications')
                Makefile['macosx'] = 'true'
            except OSError as e:
                expect_errno(e, errno.ENOENT, errno.ENOTDIR)
    
    ### Cygwin (desktop and menu shortcuts for Windows, not X11)
    if 'cygwin' not in Makefile:
        Makefile['cygwin'] = 'false'
        if sys.platform == 'cygwin':
            try:
                if is_command('mkshortcut.exe') or is_command('mkshortcut'):
                    Makefile['cygwin'] = 'true'
                else:
                    stderr("Cygwin detected but 'mkshortcut' is missing.\n\n")
            except OSError:
                exception = sys.exc_info()
                stderr('WARNING: Unexpected OSError in detect_desktop\n')
                traceback.print_exception(*exception)
    if Makefile['cygwin'] == 'true':
        default = {
            'windows-desktop':   'true',
            'windows-startmenu': 'true',
            'windows-isadmin':   'false',
        }
        for key in default:
            if key not in Makefile:
                Makefile[key] = default[key]
            elif Makefile[key] in ('true', 'false'):
                pass
            else:
                err = True
                stderr(
                    "Unkown value {} for {}. "
                    "Valid: 'true'/'false'\n\n".format(
                        repr(Makefile[key]), key
                ))
                Makefile[key] = default[key]
        # DESTDIR is completely ignored by mkshortcut(1)
        installed_destdir = expand('INSTALLED_DESTDIR', Makefile)
        if installed_destdir and Makefile['skip-postinstall'] != 'true':
            for shortcut in ('windows-desktop', 'windows-startmenu'):
                if Makefile[shortcut] == 'true':
                    err = True
                    stderr(
                        "Using INSTALLED_DESTDIR will mess things up with "
                        "Windows shortcuts.  Refusing.\n"
                        "Keep DESTDIR != INSTALLED_DESTDIR or set "
                        "skip-postinstall=true to ./configure\n\n"
                    )
                    break

    ### Haiku (open source BeOS clone)
    if 'haiku' not in Makefile:
        # Will overwrite /boot/home/Desktop/Anonymine, but only if it is
        # what we think it is.
        acceptable_content = [
            # Just `Terminal -t Anonymine anonymine` doesn't work
            "#!/bin/sh\nTerminal -t Anonymine sh -c anonymine\n",
            # 0.6.6 - 0.6.36:
            "#!/bin/sh\nTerminal -t Anonymine sh -c 'TERM=screen anonymine'\n",
        ]
        try:
            desktop_files = os.listdir('/boot/home/Desktop')
            # goto except if fail to listdir
            if 'Anonymine' in desktop_files:
                # /boot/home/Desktop/Anonymine already exists, make sure
                # it is just the launch script.
                try:
                    content = open('/boot/home/Desktop/Anonymine').read()
                    assert content in acceptable_content
                except Exception:
                    stderr(
                        "Don't know what /boot/home/Desktop/Anonymine is.\n"
                        "Setting haiku=false to prevent overwriting it.\n\n"
                    )
                    raise
            # /boot/home/Desktop exists and it's safe to create Anonymine there
            if not is_command('Terminal'):
                stderr(
                    "This looks like Haiku/BeOS but there is no 'Terminal'\n"
                    "program. Setting haiku=false\n\n"
                )
                assert False
            if not is_command('addattr'):
                stderr(
                    "This looks like Haiku/BeOS but there is no 'addattr'\n"
                    "command. Setting haiku=false\n\n"
                )
                assert False
            Makefile['haiku'] = 'true'
        except Exception as e:
            # EACCES on SerenityOS
            expect_errno(e, errno.ENOENT, errno.EACCES, errno.ENOTDIR)
            Makefile['haiku'] = 'false'

    ### SerenityOS
    if 'serenity' not in Makefile:
        if sys.platform == 'serenityos':
            Makefile['serenity'] = 'true'
        else:
            Makefile['serenity'] = 'false'
    # Compile a launcher program with icon?
    if 'serenity-launcher' not in Makefile:
        if Makefile['serenity'] == 'true':
            Makefile['serenity-launcher'] = 'true'
            if 'CC' not in Makefile:
                for compiler in ('cc', 'clang', 'gcc'):
                    if is_command(compiler):
                        Makefile['CC'] = compiler
                        break
                else:
                    Makefile['serenity-launcher'] = 'false'
                    stderr('Note: No C compiler found.\n')
            if not is_command('objcopy'):
                Makefile['serenity-launcher'] = 'false'
                stderr('Note: objcopy not found.\n')
            if Makefile['serenity-launcher'] == 'false':
                stderr('Note: No launcher will be created.\n\n')
        else:
            Makefile['serenity-launcher'] = 'false'

    return err


def treat_DESTDIR(Makefile, flags):
    '''
    Ensure $(DESTDIR), $(CHECKCFG_DESTDIR), $(INSTALLED_DESTDIR),
    $(skip-check-install-cfg) and $(skip-postinstall) are set.

    Ensure DESTDIRs are absolute paths.
    '''
    makebool = {True: 'true', False: 'false'}
    destdirs = [
        ('DESTDIR', ''),
        ('CHECKCFG_DESTDIR', '$(DESTDIR)'),
        ('INSTALLED_DESTDIR', '$(DESTDIR)'),
    ]
    error = False
    for destdir, default in destdirs:
        if destdir not in Makefile:
            Makefile[destdir] = default
        destdir_val = expand(destdir, Makefile)
        if destdir_val and destdir_val != os.path.abspath(destdir_val):
            stderr("Warning: $({0}) is not absolute\n"
                   "{0}={1}\n\n".format(destdir, destdir_val))

    # Inhibit check-install-cfg
    destdir = expand('DESTDIR', Makefile)
    checkcfg_destdir = expand('CHECKCFG_DESTDIR', Makefile)
    skip_check_install_cfg = makebool[checkcfg_destdir != destdir]
    if 'skip-check-install-cfg' not in Makefile:
        Makefile['skip-check-install-cfg'] = skip_check_install_cfg

    # Inhinit postinstall:
    installed_destdir = expand('INSTALLED_DESTDIR', Makefile)
    skip_postinstall = makebool[installed_destdir != destdir]
    if 'skip-postinstall' not in Makefile:
        Makefile['skip-postinstall'] = skip_postinstall

    return error


def Makefile2shell(outfile, Makefile_dict):
    '''
    Export a dictionary of Makefile variables as variables in a
    Shell scipt.

    '-' will be converted to '_' in variable names.
    Other bad characters are strictly forbidden

    Example input
    ```
    {
        'prefix': '/usr/local',
        'bindir': '$(prefix)/bin',
        'test-var': 'foo',
    }
    ```

    `outfile` will be created an contain:
    ```
    bindir='$/usr/local/bindir'
    prefix='/usr/local'
    test_var='foo'
    ```
    '''
    f = open(outfile, 'w')
    f.write(
        '# To tweak variables, run configure again with parameters\n'
        '# or edit ./reconfigure and run ./reconfigure.\n'
    )
    for key in sorted(Makefile_dict):
        value = expand(key, Makefile_dict)
        key = key.replace('-', '_')
        f.write('{}={}\n'.format(key, shellescape(value)))
    f.close()


def main():
    '''
    Parse arguments
    Generate Makefile variables (will later convert to config-vars)
    Print messages in verbose mode
    Create config-vars and Makefile
    '''
    def v(s):
        'print(s) if verbose'
        if flags['v']:
            stdout(s + '\n')
    
    # NOTICE: `getargs` pops items from sys.argv
    reconfigure_argv = sys.argv[1:]
    # Get srcdir, note that dirname can return '' for current working directory.
    srcdir = os.path.dirname(sys.argv[0])
    if srcdir == '':
        srcdir = '.'
    srcdir += '/'
    
    # These are all statically defined, they have no finder
    Makefile = {
        'srcdir': srcdir,
        'builddir': './',
        'python': sys.executable,
        'gamesdir': '$(prefix)/games',
        'bindir': '$(prefix)/bin',
        'libdir': '$(prefix)/lib',
        'libdir64': '$(prefix)/lib64',
        'exec_prefix': '$(prefix)',         # In case someone tries using it.
    }
    # Flags (relative_builddir, force, relative_srcdir, verbose)
    # and override variables:
    forced_vars, flags = getargs('bfsv')
    Makefile.update(forced_vars)

    # Check that we're at least on a unix system
    error = False
    if os.name != 'posix':
        # Jython (IIRC)
        if 'getshadow' in dir(os.name):
            if os.name.getshadow() != 'posix':
                error = True
        else:
            error = True
    if error:
        stderr(
            'The configure[.py] script is only meant to be used on unix-like '
            'operating systems.\n\n'
        )
        if not flags['f']:
            exit(1)
    
    # Need to sanity check variables early (they can cause crashes)
    # But it will fail if $(prefix) is not defined
    error |= find_prefix(Makefile, flags)
    error |= check_variables(Makefile, flags)
    
    error |= chk_deps(Makefile, flags)          # Check if we have curses, etc

    # Directories:
    error |= find_EXECUTABLES(Makefile, flags)
    error |= find_MODULES(Makefile, flags)
    error |= find_sysconfdir(Makefile, flags)
    error |= find_vargamesdir(Makefile, flags)

    error |= find_INSTALL(Makefile, flags)      # install(1) command

    # Special treatment for $(*DESTDIR), want this before detect_desktop.
    error |= treat_DESTDIR(Makefile, flags)
    error |= detect_desktop(Makefile, flags)    # "Linux" vs macOS vs no GUI

    # Just in case we pick up something strange from the environment
    error |= check_variables(Makefile, flags, second_run=True)

    # Special treatment for $(verbose)
    makebool = {True: 'true', False: 'false'}
    Makefile['verbose'] = makebool[flags['v']]

    # Special treatment for $(srcdir) and $(builddir)
    if flags['s'] or flags['b']:
        stderr(
            'Flags -s and -b are no longer supported.\n'
            'They appear to no longer be needed.\n\n'
        )
    for variable in ['srcdir', 'builddir']:
        if Makefile[variable] != './':
            Makefile[variable] = os.path.abspath(Makefile[variable]) + '/'

    # Add version
    try:
        Makefile['version'] = open(Makefile['srcdir']+'version').read().strip()
    except Exception as e:
        Makefile['version'] = 'ERROR'
        error = True
        stderr("Failed to read 'version' from srcdir: " + repr(e) + "\n\n")

    # ### Output status (verbose info and notices)
    
    # Verbose output lists some Makefile variables:
    of_interest = [
        'prefix',
        'python',
        'EXECUTABLES',
        'MODULES',
        'INSTALL',
        'sysconfdir',
        'vargamesdir',
        'oldvargamesd',
        'freedesktop',
        'macosx',
        'haiku',
        'serenity',         # This one second last
        'cygwin',           # This one last
    ]
    if 'serenity' in Makefile and Makefile['serenity'] == 'true':
        of_interest.extend([
            'serenity-launcher',
            'CC',
        ])
    if 'cygwin' in Makefile and Makefile['cygwin'] == 'true':
        of_interest.extend([
            'windows-isadmin',
            'windows-desktop',
            'windows-startmenu',
        ])
    for variable in of_interest:
        try:
            if Makefile[variable] == 'false':
                continue
            v('{} = {}'.format(variable, Makefile[variable]))
            if '$' in Makefile[variable]:
                v('(expand) "{}"'.format(expand(variable, Makefile)))
        except KeyError:
            v('Missing variable: "{}"'.format(variable))
        v('')

    if Makefile['oldvargamesd']:
        stderr(
            'NOTICE: Will merge old highscores "{}"\n'
            'into new location "{}".\nSee "doc/INSTALL.txt".\n\n'.format(
                Makefile['oldvargamesd'] + '/anonymine',
                Makefile['vargamesdir'] + '/anonymine'
        ))
    
    # ### Write output files ###
    
    builddir = Makefile['builddir']
    srcdir = Makefile['srcdir']
    Makefile_input_name = srcdir + 'installtools/makefile.in'
    Makefile_output_name = builddir + 'Makefile'
    config_vars_output_name = builddir + 'config-vars'
    reconfigure_name = Makefile['builddir'] + 'reconfigure'
    
    error_flag = False
    test_files = {
        'srcdir': Makefile_input_name,
        'builddir': builddir,
    }
    for directory in test_files:
        try:
            os.stat(test_files[directory])
        except OSError as error:
            stderr("\n$({}) appears wrong\n".format(directory))
            stderr("{}={}\n".format(directory, Makefile[directory]))
            stderr(str(error) + '\n')
            error_flag = True
    if not os.access(builddir, os.W_OK):
        stderr("\n$(builddir) is not writable\n")
        stderr("builddir={}\n".format(builddir))
        error_flag = True
    if error_flag:
        sys.exit(1)

    # The error variable used for all the find_* functions and such
    if error:
        if flags['f']:
            stderr("There were errors, but you don't seem to care.\n")
        else:
            stderr("There were errors; no Makefile will be written.\n")
            sys.exit(1)
    
    v('Create "{}"'.format(config_vars_output_name))
    Makefile2shell(config_vars_output_name, Makefile)

    v('Create "{}"'.format(Makefile_output_name))
    f = open(Makefile_output_name, 'w')
    f.write('srcdir = {}\n'.format(srcdir))
    for variable in Makefile:
        if variable.startswith('skip-'):
            f.write('{} = {}\n'.format(variable, Makefile[variable]))
    f.write(open(Makefile_input_name).read())
    f.close()

    v('Create "{}"'.format(reconfigure_name))
    reconfigure_f = open(reconfigure_name, 'w')
    reconfigure_f.write('#!/bin/sh\ncd {} && {} -B {}configure.py'.format(
        shellescape(os.getcwd()),
        shellescape(sys.executable),
        shellescape(srcdir)
    ))
    for arg in reconfigure_argv:
        reconfigure_f.write(' ' + shellescape(arg))
    reconfigure_f.write('\n')
    reconfigure_f.close()
    os.system('chmod +x ' + shellescape(reconfigure_name))


if __name__ == '__main__':
    major, minor = sys.version_info[:2]
    if major > 3:
        stderr("Never tested on Python 4.\n")
    if major < 2 or (major == 2 and minor < 7) or (major == 3 and minor < 3):
        stderr('Python 2.7 or Python >= 3.3 required. Aborted.\n')
        sys.exit(1)
    main()
