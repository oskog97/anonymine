P_executable="$EXECUTABLES/anonymine"
P_highscores="$vargamesdir/anonymine"
P_cfgdir="${sysconfdir}/anonymine"

P_cygwin_icondir="$prefix/share/icons/windows"
P_cygwin_icon="$P_cygwin_icondir/anonymine.ico"

P_serenity_af="/res/apps/Anonymine.af"
P_serenity_icondir="$prefix/share/icon"
P_serenity_icon="$P_serenity_icondir/anonymine.ico"
P_serenity_launcher="$EXECUTABLES/Anonymine"        # 'anonymine' vs 'Anonymine'
P_serenity_desktop_link="/home/anon/Desktop/Anonymine"
